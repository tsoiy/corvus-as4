package hk.hku.cecid.edi.as4.pkg;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.junit.Test;

import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;

public class AS4ErrorMessageTest {

	@Test
	public void testAS4ErrorMessage() throws AS4Exception {
		AS4ErrorMessage a;
		try {
			a = new AS4ErrorMessage();
			a.addError(new AS4Error(AS4ErrorCode.CONNECTION_FAILURE));
			a.addError(new AS4Error(AS4ErrorCode.INVALID_HEADER));

		System.out.println("Request SOAP Message = ");
			a.getMsg().writeTo(System.out);
			System.out.println();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println();
	}
	
	@Test
	public void testParseMsg() throws IOException, SOAPException, AS4Exception {
		String msg = "<S12:Envelope xmlns:S12=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:eb3=\"http://docs.oasis-open.org/ebxml-soapMessage/ebMS/v3.0/ns/core/200704/\"><S12:Header><eb3:Messaging S12:mustUnderstand=\"true\"><eb3:SignalMessage><eb3:MessageInfo><eb3:TimeStamp/><eb3:MessageId/></eb3:MessageInfo><eb3:Error/><eb3:MessageInfo><eb3:TimeStamp/><eb3:MessageId/></eb3:MessageInfo></eb3:SignalMessage><eb3:SignalMessage><eb3:PullRequest/></eb3:SignalMessage></eb3:Messaging></S12:Header><S12:Body/></S12:Envelope>";
		InputStream is = new ByteArrayInputStream(msg.getBytes());
		
		SOAPMessage request = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null, is);
		//AS4ErrorMessage a = new AS4ErrorMessage(request);
	}

}
