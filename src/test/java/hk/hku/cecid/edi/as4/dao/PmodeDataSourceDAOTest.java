package hk.hku.cecid.edi.as4.dao;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.pkg.AS4MessageConstant;
import hk.hku.cecid.piazza.commons.dao.DAOException;
import hk.hku.cecid.piazza.commons.spa.Plugin;
import hk.hku.cecid.piazza.commons.spa.PluginException;

public class PmodeDataSourceDAOTest {

	private PmodeDAO dao;

	@Before
	public void setup() throws DAOException, PluginException {
		Plugin plugin = mock(Plugin.class);
		AS4Processor as4processor = new AS4Processor();
		Properties prop = new Properties();
		prop.setProperty("module-group-descriptor", "src/test/resources/as4.module-group.xml");
		when(plugin.getParameters()).thenReturn(prop);
		as4processor.processActivation(plugin);
		dao = (PmodeDAO) AS4Processor.core.dao.createDAO(PmodeDAO.class);
	}

	@Test
	public void test() throws DAOException {
		for (int i = 1; i < 11; i++) {
			PmodeDVO dvo = (PmodeDVO) dao.createDVO();
			dvo.setPmodeId("pmode_id_" + i);
			dvo.setAction("action_" + i);
			dvo.setService("service_" + i);
			dvo.setServiceType("servicetype_" + i);
			dvo.setAddress("http://target/");
			dvo.setRespPartyId("respPartyId_" + i);
			dvo.setRespPartyRole("respPartyRole_" + i);
			dvo.setIsTokenRequired(true);
			dvo.setRespUsername("respUser_" + i);
			dvo.setRespPassword("respPassword_" + i);
			dvo.setInitPartyId("initPartyId_" + i);
			dvo.setInitPartyRole("initPartyRole_" + i);
			dvo.setInitUsername("initUser_" + i);
			dvo.setInitPassword("initPassword_" + i);
			dvo.setReceiptReplyPattern(AS4MessageConstant.EB3_RECEIPT_REPLY_PATTERN_RESPONSE);
			dvo.setReportReceiverErrorTo("report_error_to_" + i);
			dvo.setTokenUsername("token_user_" + i);
			dvo.setTokenPassword("token_password_" + i);
			switch(i)
			{
				case 5:
					dvo.setMpc(AS4MessageConstant.EB3_DEFAULT_MPC);
					break;
				case 7:
					break;
				case 3:
					dvo.setMpc("");
					break;
				default:
					dvo.setMpc("mpc_" + i);
			}
			dvo.setRetries(10);
			dvo.setRetryInterval(1000);
			dvo.setSignAlgorithm(AS4MessageConstant.EB3_SECURITY_SIGN_ALGO_RSA_SHA256);
			dvo.setEncryptAlgorithm(AS4MessageConstant.EB3_SECURITY_ENCRYPT_ALGO_3DES);
			dvo.setSignHashFunc(AS4MessageConstant.EB3_SECURITY_SIGN_HASH_FUNC_SHA256);
			if (i % 2 == 0) {
				dvo.setPullOrPush(AS4MessageConstant.EB3_MEP_BINDING_PUSH_URL);
			} else {
				dvo.setPullOrPush(AS4MessageConstant.EB3_MEP_BINDING_PULL_URL);
			}
			dvo.setAgreement("agreement_" + i);
			dvo.setSignCert("sign_cert_"+i);
//			dvo.setSignCertPassword("sign_password_"+i);
			dvo.setEncryptCert("enc_cert_" + i);
			dvo.setIsDisabled(false);
			dvo.setIsTokenDigestRequired(false);
			dvo.setIsTokenCreatedRequired(false);
			dvo.setIsPmodeAuthRequired(false);
			dvo.setIsRetryRequired(false);
			dvo.setIsEncryptRequired(false);
			dvo.setIsSignRequired(false);
			dvo.setIsReceiptRequired(false);
			dvo.setIsReportDeliveryFailure(false);
			dvo.setIsReportErrorAsResponse(false);
			dvo.setIsReportProcessError(false);
			dao.create(dvo);

		}

	}

	@Test
	public void testFindBySericeActionAgreement() throws DAOException {
		PmodeDVO dvo = (PmodeDVO) dao.createDVO();
		dvo.setAction("action_4");
		dvo.setService("service_4");
		dvo.setAgreement("agreement_4");
		assertTrue(dao.findByServiceActionAgreement(dvo));
	}
	
	@Test
	public void testFindByMpc() throws DAOException {
		List<PmodeDVO> l = dao.findPmodeForPullByMpc("mpc_4");
		assertEquals(l.size(), 0);
		l = dao.findPmodeForPullByMpc("mpc_3");
		assertEquals(l.size(), 1);
		assertEquals(l.get(0).getMpc(), "mpc_3");
		assertEquals(l.get(0).getPullOrPush(), AS4MessageConstant.EB3_MEP_BINDING_PULL_URL);
		
	}

	@Test
	public void testFindByDefaultMpc() throws DAOException {
		List<PmodeDVO> l = dao.findPmodeForPullByDefaultMpc();
		assertEquals(l.size(), 3);
		assertEquals(l.get(0).getPullOrPush(), AS4MessageConstant.EB3_MEP_BINDING_PULL_URL);
		assertEquals(l.get(1).getPullOrPush(), AS4MessageConstant.EB3_MEP_BINDING_PULL_URL);
		assertEquals(l.get(2).getPullOrPush(), AS4MessageConstant.EB3_MEP_BINDING_PULL_URL);
		
	}
	
}
