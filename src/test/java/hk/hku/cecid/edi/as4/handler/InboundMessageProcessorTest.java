package hk.hku.cecid.edi.as4.handler;

import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;

import org.junit.Before;
import org.junit.Test;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.PmodeDAO;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4UserMessage;
import hk.hku.cecid.piazza.commons.dao.DAOException;
import hk.hku.cecid.piazza.commons.spa.Plugin;
import hk.hku.cecid.piazza.commons.spa.PluginException;

public class InboundMessageProcessorTest {

	private PmodeDAO dao;

	@Before
	public void setup() throws PluginException, DAOException {
		Plugin plugin = mock(Plugin.class);
		AS4Processor as4processor = new AS4Processor();
		Properties prop = new Properties();
		prop.setProperty("module-group-descriptor", "src/test/resources/as4.module-group.xml");
		when(plugin.getParameters()).thenReturn(prop);
		as4processor.processActivation(plugin);
		dao = (PmodeDAO) AS4Processor.core.dao.createDAO(PmodeDAO.class);
	}

	@Test
	public void test() throws SOAPException, IOException, AS4Exception  {
		InputStream fin = this.getClass().getClassLoader().getResourceAsStream("bundle.txt");
		SOAPMessage s = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null, fin);
		InboundMessageProcessor i = InboundMessageProcessor.getInstance();
		i.processRequest(s);

	}

	@Test
	public void testSignedMessage() throws SOAPException, DAOException, AS4Exception {
		AS4UserMessage u = new AS4UserMessage();
		PmodeDVO pmode = dao.findByPmodeId("ex-pm-push");
		u.setPmodeId("ex-pm-push");
		if (!AS4Processor.getSecurityProcessor().addSecurityProcess(u, true, pmode))
			throw new RuntimeException();
		System.out.println(u.toString());
		SOAPHeaderElement e = u.getSecurityHeader(null);
		Iterator it = e.getChildElements(new QName("http://www.w3.org/2000/09/xmldsig#", "Signature", "ds"));
		if (it.hasNext()) {
			SOAPElement e1 = (SOAPElement) it.next();
			it = e1.getChildElements(new QName("http://www.w3.org/2000/09/xmldsig#", "SignatureValue", "ds"));
			if (it.hasNext()) {
				SOAPElement e2 = (SOAPElement) it.next();
				System.out.println("Signature value = " + e2.getTextContent());
				e2.setTextContent(e2.getTextContent().toUpperCase());
				System.out.println("Signature value = " + e2.getTextContent());				
			}
		}
		System.out.println(u.toString());
		
		AS4Processor.getSecurityProcessor().verifyMessage(u, pmode, true);
		InboundMessageProcessor i = InboundMessageProcessor.getInstance();
		i.processRequest(u.getSOAP());

	}
	
	@Test
	public void testSignedEncrypted() throws AS4Exception, IOException, SOAPException, DAOException {
		InputStream fin = this.getClass().getClassLoader().getResourceAsStream("inbound_holodeck_signed_encrypted.txt");
		SOAPMessage msg = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null, fin);
//		AS4UserMessage usermsg = new AS4UserMessage(msg);
//		PmodeDVO pmode = dao.findByPmodeId("ex-pm-push");
		InboundMessageProcessor i = InboundMessageProcessor.getInstance();
		i.processRequest(msg);
	}
}
