package hk.hku.cecid.edi.as4.handler;

import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.xml.soap.SOAPException;

import org.junit.Before;
import org.junit.Test;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.MessageStoreConstant;
import hk.hku.cecid.edi.as4.dao.MessageStoreDAO;
import hk.hku.cecid.edi.as4.dao.RepositoryDVO;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4Message;
import hk.hku.cecid.edi.as4.pkg.AS4ReceiptMessage;
import hk.hku.cecid.edi.as4.pkg.AS4UserMessage;
import hk.hku.cecid.piazza.commons.dao.DAOException;
import hk.hku.cecid.piazza.commons.spa.Plugin;
import hk.hku.cecid.piazza.commons.spa.PluginException;
import hk.hku.cecid.piazza.commons.util.Generator;

public class ReceiptMessageProcessorTest {

	MessageStoreDAO dao;
	@Before
	public void setup() throws PluginException, DAOException  {
		Plugin plugin = mock(Plugin.class);
		AS4Processor as4processor = new AS4Processor();
		Properties prop = new Properties();
		prop.setProperty("module-group-descriptor", "src/test/resources/as4.module-group.xml");
		when(plugin.getParameters()).thenReturn(prop);
		as4processor.processActivation(plugin);
		dao = (MessageStoreDAO)AS4Processor.core.dao.createDAO(MessageStoreDAO.class);
	}

	
	@Test
	public void testReceipt() throws DAOException, SOAPException, AS4Exception, IOException {
		String messageId = Generator.generateMessageID();
		AS4UserMessage usermsg = new AS4UserMessage();
		usermsg.setPmodeId("pmode_id_4");
		usermsg.setMessageId(messageId);
		usermsg.setTimeStamp(new Date());
		usermsg.setConvId("TestReceipt");
		MessageDVO dvo = dao.createMessageDVO();
		RepositoryDVO repoDvo = dao.createRepositoryDVO();
		AS4DvoConvertor.toMessageDvo(usermsg, dvo);
		AS4DvoConvertor.toRepositoryDvo(usermsg, repoDvo);
		dvo.setMessageBox(MessageStoreConstant.OUTBOX);
		dvo.setStatus(MessageStoreConstant.STATUS_PENDING_DELIVERY);
		repoDvo.setMessageBox(MessageStoreConstant.OUTBOX);
		dao.storeMessage(dvo, repoDvo);
		
		AS4ReceiptMessage receiptMsg = new AS4ReceiptMessage();
		receiptMsg.setMessageId(Generator.generateMessageID());
		receiptMsg.setRefMsgId(messageId);
		receiptMsg.setTimeStamp(new Date());
		receiptMsg.addReceiptDetail(usermsg, false);
		ReceiptMessageProcessor r = new ReceiptMessageProcessor(receiptMsg);
		AS4Message reply = r.process();
		
	}

}
