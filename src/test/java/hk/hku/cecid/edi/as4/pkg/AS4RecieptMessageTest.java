package hk.hku.cecid.edi.as4.pkg;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.junit.Before;
import org.junit.Test;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.PmodeDataSourceDVO;
import hk.hku.cecid.edi.as4.handler.MessageSecurityProcessor;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.piazza.commons.spa.Plugin;
import hk.hku.cecid.piazza.commons.spa.PluginException;

public class AS4RecieptMessageTest {

	MessageSecurityProcessor s;
	PmodeDVO dvo;
	
	@Before
	public void setup() throws PluginException {
		Plugin plugin = mock(Plugin.class);
		AS4Processor as4processor = new AS4Processor();
		Properties prop = new Properties();
		prop.setProperty("module-group-descriptor", "src/test/resources/as4.module-group.xml");
		when(plugin.getParameters()).thenReturn(prop);
		as4processor.processActivation(plugin);
		s = (MessageSecurityProcessor) as4processor.getSystemModule().getComponent("security-processor");
			dvo = new PmodeDataSourceDVO();
			dvo.setPmodeId("holodeck");
			dvo.setSignCert("partya");
			dvo.setSignHashFunc("http://www.w3.org/2001/04/xmlenc#sha256");
			dvo.setSignAlgorithm("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
//			dvo.setSignCertPassword("ExampleA");
			dvo.setIsSignRequired(true);
			dvo.setIsEncryptRequired(true);
			dvo.setEncryptAlgorithm("http://www.w3.org/2009/xmlenc11#aes128-gcm");
			dvo.setEncryptCert("partya");
			dvo.setIsPmodeAuthRequired(true);
			dvo.setRespUsername("respUserName");
			dvo.setRespPassword("respPassword");
			dvo.setInitUsername("initUserName");
			dvo.setInitPassword("initPassword");
			dvo.setIsTokenRequired(true);
			dvo.setTokenUsername("tokenUserName");
			dvo.setTokenPassword("tokenPassword");
			dvo.setIsTokenDigestRequired(false);
			dvo.setIsTokenCreatedRequired(true);
	}
	
	
	@Test
	public void testCreateReceipt() throws SOAPException, IOException, AS4Exception {
		AS4UserMessage request = new AS4UserMessage();
		request.setPmodeId("pmode_id_4");
		request.setMessageId("RequestMessageId");
		System.out.println(request.toString());
		AS4ReceiptMessage receipt = new AS4ReceiptMessage();
		receipt.setMessageId("ReceiptMsgId");
		receipt.addReceiptDetail(request, false);
		System.out.println(receipt.toString());
		QName userMsgName = new QName(AS4MessageConstant.EB3_NS_URL, AS4MessageConstant.EB3_HEADER_USERMSG);
		Iterator usermsg = receipt.getReceiptNode().getChildElements(userMsgName);
		assertTrue(usermsg.hasNext());

	}
	
	@Test
	public void testCreateNRReceipt() throws SOAPException, IOException, AS4Exception {
		
		AS4UserMessage request = new AS4UserMessage();
		request.setPmodeId("pmode_id_4");
		request.setMessageId("RequestMessageId2");
		s.addSecurityProcess(request, false, dvo);
		System.out.println(request.toString());
		AS4ReceiptMessage receipt = new AS4ReceiptMessage();
		receipt.setMessageId("ReceiptMsgId2");
		receipt.addReceiptDetail(request, true);
		System.out.println(receipt.toString());
		QName nriName = new QName(AS4MessageConstant.EB3_EBBPSIG_URL, AS4MessageConstant.EB3_RECEIPT_EBBPSIG_NRI);
		Iterator nrinode = receipt.getReceiptNode().getChildElements(nriName);
		assertTrue(nrinode.hasNext());

	}	

}
