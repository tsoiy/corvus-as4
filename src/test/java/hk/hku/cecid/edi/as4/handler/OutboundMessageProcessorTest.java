package hk.hku.cecid.edi.as4.handler;

import static org.mockito.Mockito.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.soap.SOAPException;

import org.junit.Before;
import org.junit.Test;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.model.Attachment;
import hk.hku.cecid.edi.as4.model.MshMessage;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorCode;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorMessage;
import hk.hku.cecid.piazza.commons.dao.DAOException;
import hk.hku.cecid.piazza.commons.spa.Plugin;
import hk.hku.cecid.piazza.commons.spa.PluginException;

public class OutboundMessageProcessorTest {
	OutboundMessageProcessor o;

	@Before
	public void setup() throws PluginException  {
		Plugin plugin = mock(Plugin.class);
		AS4Processor as4processor = new AS4Processor();
		Properties prop = new Properties();
		prop.setProperty("module-group-descriptor", "src/test/resources/as4.module-group.xml");
		when(plugin.getParameters()).thenReturn(prop);
		as4processor.processActivation(plugin);
		o = OutboundMessageProcessor.getInstance();
	}
	
	@Test 
	public void testHolodeck() throws DAOException, IOException {
		sendUserMessage("holodeck");
	}
	
	@Test
	public void testUserMessage() throws DAOException, IOException {
		sendUserMessage("pmode_id_7");
		sendUserMessage("pmode_id_8");
	}
	
	@Test
	public void testHolodeckUserMessage() throws DAOException, IOException {
		sendUserMessage("ex-pm-pull2");
	}
	
	@Test
	public void testPullMessage() throws DAOException, IOException {
		sendPullMessage("pmode_id_7");
		sendPullMessage("pmode_id_8");
		sendPullMessage("pmode_id_9");
	}
	
	public void sendUserMessage(String pmode_id) throws DAOException, IOException {
		MshMessage request = new MshMessage();
		MshMessage response = new MshMessage();
		request.setType(MshMessage.RequestType.UserMessage);
		request.setConvId("ConvId");
		request.setPmodeId(pmode_id);
		Attachment a = new Attachment();
		a.setFilename("test1.txt");
		Path path = Paths.get("G:\\test1.txt");
		a.setContent( Files.readAllBytes(path));
		a.setContentType("text/plain");
		List<Attachment> l = new ArrayList<>();
		l.add(a);
		a = new Attachment();
		a.setFilename("test2.txt");
		path = Paths.get("G:\\test2.txt");
		a.setContent( Files.readAllBytes(path));
		a.setContentType("text/plain");
		l.add(a);
		request.setAttachments(l);
		o.processOutgoingRequest(request, response);
	}

	public void sendPullMessage(String pmode_id) throws DAOException, IOException {
		
		MshMessage request = new MshMessage();
		MshMessage response = new MshMessage();
		request.setType(MshMessage.RequestType.PullRequest);
		request.setPmodeId(pmode_id);
		o.processOutgoingRequest(request, response);
		System.out.println(response.getResult());
	}

	@Test
	public void sendErrorMessage() throws DAOException, IOException, SOAPException, AS4Exception {

		AS4ErrorMessage errorMessage = new AS4ErrorMessage();
		errorMessage.setRefMsgId("138a4386-806c-4439-8609-257b004f774e@Sanctuary");
		errorMessage.addError(new AS4Error(AS4ErrorCode.EMPTY_MPC));
		o.sendErrorMessage(errorMessage);
		System.out.println();
	}

}
