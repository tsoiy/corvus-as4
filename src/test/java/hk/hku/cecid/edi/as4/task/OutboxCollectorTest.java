package hk.hku.cecid.edi.as4.task;

import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.piazza.commons.spa.Plugin;
import hk.hku.cecid.piazza.commons.spa.PluginException;

public class OutboxCollectorTest {

	
	@Before
	public void setup() throws PluginException {
		Plugin plugin = mock(Plugin.class);
		AS4Processor as4processor = new AS4Processor();
		Properties prop = new Properties();
		prop.setProperty("module-group-descriptor", "src/test/resources/as4.module-group.xml");
		when(plugin.getParameters()).thenReturn(prop);
		as4processor.processActivation(plugin);
	}
	
	@Test
	public void test() throws Exception {
		OutboxCollector a = new OutboxCollector();
		int i =0;
		List<OutboxTask> l =a.getTaskList(); 
		for(OutboxTask m:l) {
			m.execute();
			i++;

		}
	}
}
