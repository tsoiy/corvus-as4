package hk.hku.cecid.edi.as4.pkg;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;

import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeader;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.junit.Test;

import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.piazza.commons.io.IOHandler;
import hk.hku.cecid.piazza.commons.net.ConnectionException;
import hk.hku.cecid.piazza.commons.soap.SOAPHttpConnector;
import hk.hku.cecid.piazza.commons.util.Generator;

public class AS4UserMessageTest {

	@Test
	public void testCreateAndParse() throws AS4Exception, SOAPException, IOException {
		AS4UserMessage msg = new AS4UserMessage();
		String action = Generator.generateUUID();
		String convid = Generator.generateUUID();
		String service = Generator.generateUUID();
		String serviceType = Generator.generateUUID();
		String fromPartyId = Generator.generateUUID();
		String fromPartyRole = Generator.generateUUID();
		String toPartyId = Generator.generateUUID();
		String toPartyRole = Generator.generateUUID();
		String agreeRef = Generator.generateUUID();
		String agreeType = Generator.generateUUID();
		String msgId = Generator.generateMessageID();
		String pmodeId = Generator.generateUUID();
		Date timestamp = new Date();
		
		msg.setMessageId(msgId);
		msg.setAction(action);
		msg.setService(service);
		msg.setServiceType(serviceType);
		msg.setFromPartyId(fromPartyId);
		msg.setFromPartyRole(fromPartyRole);
		msg.setToPartyId(toPartyId);
		msg.setToPartyRole(toPartyRole);
		msg.setConvId(convid);
		msg.setAgreeRef(agreeRef);
		msg.setTimeStamp(timestamp);
		msg.setPmodeId(pmodeId);
		msg.setAgreementType(agreeType);
//		msg.setMpc(null);
		
		String soapstr = msg.toString();
		// replace the namespace prefix with others
		String newPrefix = "adfdkflajd";
		soapstr = soapstr.replaceAll(AS4MessageConstant.EB3_PREFIX+":", newPrefix + ":");
		soapstr = soapstr.replaceAll(":"+AS4MessageConstant.EB3_PREFIX+"=", ":"+newPrefix+"=");
		soapstr = soapstr.replaceAll("env:", "S12:");
		soapstr = soapstr.replaceAll(":env=", ":S12=");

		System.out.println(soapstr);
		ByteArrayInputStream in = new ByteArrayInputStream(soapstr.getBytes());
		SOAPMessage s = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null, in);
		AS4UserMessage incmsg = new AS4UserMessage(msg.getSOAP());

		assertEquals(incmsg.getMessageId(), msgId);
		assertEquals(incmsg.getAction(), action);
		assertEquals(incmsg.getAgreeRef(), agreeRef);
		assertEquals(incmsg.getService(), service);
		assertEquals(incmsg.getServiceType(), serviceType);
		assertEquals(incmsg.getConvId(), convid);
		assertEquals(incmsg.getTimeStamp(), timestamp);
		assertEquals(incmsg.getFromPartyId(), fromPartyId);
		assertEquals(incmsg.getFromPartyRole(), fromPartyRole);
		assertEquals(incmsg.getToPartyId(), toPartyId);
		assertEquals(incmsg.getToPartyRole(), toPartyRole);
		assertEquals(incmsg.getPmodeId(), pmodeId);
		assertEquals(incmsg.getAgreementType(), agreeType);

		
	}

	
	@Test
	public void testAttachment() throws AS4Exception {
		try {
			AS4UserMessage a = new AS4UserMessage();
			a.setAction("Update");
			a.setAgreeRef("agreeref");
			a.setConvId("con-id");
			a.addMessageProperty("dept","HR");
			a.addMessageProperty("Attn","Mr Lee");
			InputStream in = this.getClass().getClassLoader().getResourceAsStream("usermessage_attachment.txt");
			a.addAttachment(IOHandler.readBytes(in), "usermessage_attachment.txt", "text/plain");
			System.out.println(a.toString());
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println();
	}	
	
	@Test
	public void testAS4Message() throws AS4Exception {
		try {
			AS4UserMessage a = new AS4UserMessage();
			a.setAction("Update");
			a.setAgreeRef("agreeref");
			a.setConvId("con-id");
			a.addMessageProperty("dept","HR");
			a.addMessageProperty("Attn","Mr Lee");
			InputStream in = this.getClass().getClassLoader().getResourceAsStream("usermessage_attachment.txt");
			a.addAttachment(IOHandler.readBytes(in), "usermessage_attachment.txt", "text/plain");
			System.out.println("Request SOAP Message = ");
			a.getMsg().writeTo(System.out);
			System.out.println("\n\nMime headers");
			Iterator<MimeHeader> it = a.getMsg().getMimeHeaders().getAllHeaders();
			while(it.hasNext()) {
				MimeHeader m = it.next();
				System.out.println("H: " + m.getName() + " " + m.getValue());
			}
//	        HttpURLConnection conn = null;
//	        SOAPMessage reply = null;
//
//            SOAPHttpConnector connector = new SOAPHttpConnector(new URL("http://localhost:9090/"));
//            try {
//				conn = connector.createConnection();
//	            conn.setDoOutput(true);
//	            reply = connector.send(a.getMsg(), conn);
//				
//			} catch (ConnectionException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			//AttachmentPart aa = (AttachmentPart)a.getMsg().getAttachments().next();
			//System.out.println("Content ID: " + aa.getContentId());
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println();
	}

}
