package hk.hku.cecid.edi.as4.handler;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;

import org.apache.wss4j.common.ext.WSSecurityException;
import org.apache.wss4j.common.util.XMLUtils;
import org.junit.Before;
import org.junit.Test;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.PmodeDataSourceDVO;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorCode;
import hk.hku.cecid.edi.as4.pkg.AS4UserMessage;
import hk.hku.cecid.piazza.commons.spa.Plugin;
import hk.hku.cecid.piazza.commons.spa.PluginException;
import hk.hku.cecid.piazza.commons.util.Generator;

public class MessageSecurityProcessorTest {

	private PmodeDVO dvo;
	private MessageSecurityProcessor s;

	public MessageSecurityProcessorTest() {
		dvo = new PmodeDataSourceDVO();
		dvo.setPmodeId("holodeck");
		dvo.setSignCert("partya");
		dvo.setSignHashFunc("http://www.w3.org/2001/04/xmlenc#sha256");
		dvo.setSignAlgorithm("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
//		dvo.setSignCertPassword("ExampleA");
		dvo.setIsSignRequired(true);
		dvo.setIsEncryptRequired(true);
		dvo.setEncryptAlgorithm("http://www.w3.org/2009/xmlenc11#aes128-gcm");
		dvo.setEncryptCert("partyc");
		dvo.setIsPmodeAuthRequired(true);
		dvo.setRespUsername("respUserName");
		dvo.setRespPassword("respPassword");
		dvo.setInitUsername("initUserName");
		dvo.setInitPassword("initPassword");
		dvo.setIsTokenRequired(true);
		dvo.setTokenUsername("tokenUserName");
		dvo.setTokenPassword("tokenPassword");
		dvo.setIsTokenDigestRequired(false);
		dvo.setIsTokenCreatedRequired(true);

	}

	@Before
	public void setup() throws PluginException {
		Plugin plugin = mock(Plugin.class);
		AS4Processor as4processor = new AS4Processor();
		Properties prop = new Properties();
		prop.setProperty("module-group-descriptor", "src/test/resources/as4.module-group.xml");
		when(plugin.getParameters()).thenReturn(prop);
		as4processor.processActivation(plugin);
		s = (MessageSecurityProcessor) as4processor.getSystemModule().getComponent("security-processor");

	}


	@Test
	public void testSignAndVerify()
			throws IOException, SOAPException, WSSecurityException, TransformerException, AS4Exception {
		InputStream fin = this.getClass().getClassLoader().getResourceAsStream("test_usermessage.txt");
		SOAPMessage usermsg = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null, fin);

		System.out.println(
				"Before:\n" + XMLUtils.prettyDocumentToString(usermsg.getSOAPPart().getEnvelope().getOwnerDocument()));
		s.signSOAP(usermsg, dvo);
		System.out.println(
				"After:\n" + XMLUtils.prettyDocumentToString(usermsg.getSOAPPart().getEnvelope().getOwnerDocument()));

		s.verifySignature(usermsg, dvo);
	}

	@Test
	public void encryptMessage() throws IOException, AS4Exception, SOAPException {
		InputStream fin = this.getClass().getClassLoader().getResourceAsStream("test_usermessage.txt");
		SOAPMessage usermsg = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null, fin);
		s.encryptSOAP(usermsg, dvo);

	}

	@Test
	public void encryptMessageWithAttachment() throws IOException, SOAPException, AS4Exception, TransformerException {
		HashMap<String, byte[]> attachments = new HashMap();
		InputStream fin = this.getClass().getClassLoader().getResourceAsStream("multi_part_usermessage.txt");
		MimeHeaders headers = new MimeHeaders();
		headers.addHeader("Content-Type",
				"multipart/related; boundary=\"MIMEBoundary_eb07cedb97676a6eb193273a16fa0d6c7fefe9394b6c633c\"; type=\"application/soap+xml\"; start=\"<0.fb07cedb97676a6eb193273a16fa0d6c7fefe9394b6c633c@apache.org>\"");
		SOAPMessage usermsg = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(headers, fin);
		Iterator it = usermsg.getAttachments();
		while (it.hasNext()) {
			AttachmentPart part = (AttachmentPart) it.next();
			attachments.put(part.getContentId(), part.getRawContentBytes());
		}
		s.encryptSOAP(usermsg, dvo);
		it = usermsg.getAttachments();
		while (it.hasNext()) {
			AttachmentPart part = (AttachmentPart) it.next();
			assertFalse(Arrays.equals(attachments.get(part.getContentId()), part.getRawContentBytes()));
		}
		System.out.println("After:\n" +  XMLUtils.prettyDocumentToString(usermsg.getSOAPPart().getEnvelope().getOwnerDocument()));
		s.decryptMessage(usermsg, dvo);
	}

	@Test
	public void signMessageWithAttachment() throws IOException, SOAPException, AS4Exception {
		InputStream fin = this.getClass().getClassLoader().getResourceAsStream("multi_part_usermessage.txt");
		MimeHeaders headers = new MimeHeaders();
		headers.addHeader("Content-Type",
				"multipart/related; boundary=\"MIMEBoundary_eb07cedb97676a6eb193273a16fa0d6c7fefe9394b6c633c\"; type=\"application/soap+xml\"; start=\"<0.fb07cedb97676a6eb193273a16fa0d6c7fefe9394b6c633c@apache.org>\"");
		SOAPMessage usermsg = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(headers, fin);
		s.signSOAP(usermsg, dvo);

	}

	@Test
	public void verifyMessage() throws AS4Exception, IOException, SOAPException {
		InputStream fin = this.getClass().getClassLoader().getResourceAsStream("test_verify_signature.txt");
		SOAPMessage msg = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null, fin);
		AS4UserMessage usermsg = new AS4UserMessage(msg); 
		dvo = new PmodeDataSourceDVO();
		dvo.setPmodeId("holodeck");
		dvo.setSignCert("partya");
		dvo.setSignHashFunc("http://www.w3.org/2001/04/xmlenc#sha256");
		dvo.setSignAlgorithm("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
//		dvo.setSignCertPassword("ExampleA");
		dvo.setIsSignRequired(true);
		s.verifyMessage(usermsg, dvo, true);
	}

	@Test
	public void verifyUser() throws IOException, SOAPException, WSSecurityException, AS4Exception {
		AS4UserMessage usermsg = new AS4UserMessage();
		usermsg.setMessageId(Generator.generateUUID());
		usermsg.setTimeStamp(new Date());
		usermsg.setBody("testing");
		s.addUserNameToken(usermsg.getSOAP(), dvo, true);
		System.out.println("Before:\n" + usermsg.toString());
		s.authEbmsUser(usermsg, dvo, true);
		System.out.println("After:\n" + usermsg.toString());
	}

	@Test
	public void verifyDecryption()
			throws IOException, SOAPException, WSSecurityException, AS4Exception, TransformerException {
		AS4UserMessage usermsg = new AS4UserMessage();
		usermsg.setMessageId(Generator.generateUUID());
		usermsg.setTimeStamp(new Date());
		usermsg.setBody("testing");
		s.addUserNameToken(usermsg.getSOAP(), dvo, true);
		s.signSOAP(usermsg.getSOAP(), dvo);
		s.encryptSOAP(usermsg.getSOAP(), dvo);
		System.out.println("Before:\n" + usermsg.toString());
		s.decryptMessage(usermsg.getSOAP(), dvo);
		System.out.println("After:\n" + usermsg.toString());
	}

	@Test
	public void verifySignature()
			throws IOException, SOAPException, WSSecurityException, AS4Exception, TransformerException {
		AS4UserMessage usermsg = new AS4UserMessage();
		usermsg.setMessageId(Generator.generateUUID());
		usermsg.setTimeStamp(new Date());
		usermsg.setBody("testing");
		s.addUserNameToken(usermsg.getSOAP(), dvo, true);
		s.signSOAP(usermsg.getSOAP(), dvo);
		// s.encryptSOAP(userMessage.getSOAP(), dvo);
		System.out.println("Before:\n" + usermsg.toString());
		s.verifySignature(usermsg.getSOAP(), dvo);
		System.out.println("After:\n" + usermsg.toString());
	}

	@Test
	public void verifyDecryptionMissing()
			throws IOException, SOAPException, WSSecurityException, AS4Exception, TransformerException {
		AS4UserMessage usermsg = new AS4UserMessage();
		usermsg.setMessageId(Generator.generateUUID());
		usermsg.setTimeStamp(new Date());
		usermsg.setBody("testing");
		try {
			s.decryptMessage(usermsg.getSOAP(), dvo);
		} catch (AS4Exception e) {
			assert (e.getError().getCode() == AS4ErrorCode.POLICY_NONCOMP);
			System.out.println(e.getMessage());
		}
		s.encryptSOAP(usermsg.getSOAP(), dvo);
		usermsg.addAttachment(new byte[] {0x40,  0x41, 0x42}, "test.txt", "text/plain");
		try {
			s.decryptMessage(usermsg.getSOAP(), dvo);
		} catch (AS4Exception e) {
			assert (e.getError().getCode() == AS4ErrorCode.POLICY_NONCOMP);
			System.out.println(e.getMessage());
		}
	}
	
//	@Test
//	public void verifyDecryptionMismatch()
//			throws IOException, SOAPException, WSSecurityException, AS4Exception, TransformerException {
//		AS4UserMessage usermsg = new AS4UserMessage();
//		usermsg.setMessageId(Generator.generateUUID());
//		usermsg.setTimeStamp(new Date());
//		usermsg.setBody("testing");
//		try {
//			s.decryptMessage(usermsg.getSOAP(), dvo);
//		} catch (AS4Exception e) {
//			assert (e.getError().getCode() == AS4ErrorCode.POLICY_NONCOMP);
//			System.out.println(e.getMessage());
//		}
//		s.encryptSOAP(usermsg.getSOAP(), dvo);
//		dvo.setIsEncryptRequired(false);
//		try {
//			s.decryptMessage(usermsg.getSOAP(), dvo);
//		} catch (AS4Exception e) {
//			assert (e.getError().getCode() == AS4ErrorCode.FAILED_DECRYPT);
//			System.out.println(e.getMessage());
//		}
//		dvo.setIsEncryptRequired(true);
//		String password = dvo.getEncryptCertPassword();
//		dvo.setEncryptCertPassword("123");
//		try {
//			s.decryptMessage(usermsg.getSOAP(), dvo);
//		} catch (AS4Exception e) {
//			assert (e.getError().getCode() == AS4ErrorCode.FAILED_DECRYPT);
//			System.out.println(e.getMessage());
//		}
//		dvo.setIsEncryptRequired(true);
//		dvo.setEncryptCertPassword(password);
//		dvo.setEncryptAlgorithm("dummy");
//		try {
//			s.decryptMessage(usermsg.getSOAP(), dvo);
//		} catch (AS4Exception e) {
//			assert (e.getError().getCode() == AS4ErrorCode.POLICY_NONCOMP);
//			System.out.println(e.getMessage());
//		}
//		System.out.println("After:\n" + usermsg.toString());
//	}

}
