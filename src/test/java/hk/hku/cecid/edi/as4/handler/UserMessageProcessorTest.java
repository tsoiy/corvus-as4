package hk.hku.cecid.edi.as4.handler;

import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.xml.soap.SOAPException;

import org.junit.Before;
import org.junit.Test;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.MessageStoreDAO;
import hk.hku.cecid.edi.as4.dao.PmodeDAO;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4Message;
import hk.hku.cecid.edi.as4.pkg.AS4UserMessage;
import hk.hku.cecid.piazza.commons.dao.DAOException;
import hk.hku.cecid.piazza.commons.spa.Plugin;
import hk.hku.cecid.piazza.commons.spa.PluginException;
import hk.hku.cecid.piazza.commons.util.Generator;

public class UserMessageProcessorTest {

	MessageStoreDAO dao;
	@Before
	public void setup() throws PluginException, DAOException  {
		Plugin plugin = mock(Plugin.class);
		AS4Processor as4processor = new AS4Processor();
		Properties prop = new Properties();
		prop.setProperty("module-group-descriptor", "src/test/resources/as4.module-group.xml");
		when(plugin.getParameters()).thenReturn(prop);
		as4processor.processActivation(plugin);
		dao = (MessageStoreDAO) AS4Processor.core.dao.createDAO(MessageStoreDAO.class);
	}

	@Test
	public void test() throws DAOException, IOException, SOAPException, AS4Exception {
		AS4UserMessage msg = new AS4UserMessage();
		msg.setPmodeId("pmode_id_4");
		msg.setMessageId(Generator.generateMessageID());
		msg.setService("UserMessageProcessor unit test service");
		msg.setAction("UserMessageProcessor unit test action");
		msg.setAgreeRef("UserMessageProcessor unit test agreeref");
		msg.setTimeStamp(new Date());
		UserMessageProcessor u = new UserMessageProcessor(msg);
		AS4Message r = u.process();
		if( r != null )
			System.out.println(r.toString());

//		u.getInsertMsgs().forEach(System.out::println);
//		u.getInsertDetails().forEach(System.out::println);
//		
	}	
	
	
}
