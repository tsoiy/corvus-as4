package hk.hku.cecid.edi.as4.pkg;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.junit.Test;

import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;

public class AS4BundledMessageTest {

	@Test
	public void testAS4BundledMessage() throws SOAPException, IOException, AS4Exception {
		AS4BundledMessage a = new AS4BundledMessage();
		AS4UserMessage u = new AS4UserMessage();
		AS4PullMessage p = new AS4PullMessage();
		AS4ErrorMessage e = new AS4ErrorMessage();
		AS4ReceiptMessage r = new AS4ReceiptMessage();
		r.setRefMsgId("REFID xc123");
		Path f = Paths.get("G:\\a.txt");
		u.addAttachment(Files.readAllBytes(f), f.getFileName().toString(), "text/plain");
		f = Paths.get("G:\\b.txt");
		u.addAttachment(Files.readAllBytes(f), f.getFileName().toString(), "text/plain");
		e.addError(new AS4Error(AS4ErrorCode.INVALID_HEADER));
		e.addError(new AS4Error(AS4ErrorCode.DECOMPRESSION_ERROR));
		a.addMessage(u);
		a.addMessage(p);
		a.addMessage(e);
		a.addMessage(r);
		System.out.println("Request SOAP Message = ");
		a.getMsg().writeTo(System.out);
		System.out.println("");
	}

	@Test
	public void testParsing() throws SOAPException, IOException, AS4Exception {
		AS4BundledMessage a = new AS4BundledMessage();
		AS4UserMessage u = new AS4UserMessage();
		AS4PullMessage p = new AS4PullMessage();
		AS4ErrorMessage e = new AS4ErrorMessage();
		AS4ReceiptMessage r = new AS4ReceiptMessage();
		r.setRefMsgId("REFID xc123");
		r.setRefMsgId("REFID xc123");
		Path f = Paths.get("G:\\a.txt");
		u.addAttachment(Files.readAllBytes(f), f.getFileName().toString(), "text/plain");
		f = Paths.get("G:\\b.txt");
		u.addAttachment(Files.readAllBytes(f), f.getFileName().toString(), "text/plain");
//		f = new File("G:\\a.jpg");
//		u.addAttachment(f);
		e.addError(new AS4Error(AS4ErrorCode.INVALID_HEADER));
		e.addError(new AS4Error(AS4ErrorCode.DECOMPRESSION_ERROR));
		a.addMessage(u);
		a.addMessage(p);
		a.addMessage(e);
		a.addMessage(r);
		System.out.println("Request SOAP Message = ");
		a.getMsg().writeTo(System.out);
		System.out.println("\n");
		AS4BundledMessage s = new AS4BundledMessage(a.getMsg());
		List<AS4Message> l = s.extractMessages();
		int i = 0;
		for (AS4Message m : l) {
			i++;
			System.out.println(i + ":" + m);
		}

	}

	@Test
	public void testContentId() throws IOException, SOAPException {
		String s = "--MIMEBoundary\r\nContent-Type: text/xml\r\n\r\n" + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
				+ "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/"
				+ "soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body/>" + "</SOAP-ENV:Envelope>\r\n--MIMEBoundary\r\n"
				+ "Content-Type: text/plain\r\nContent-Id: <ebxmlms>\r\n\r\n" + "ebxmlms\r\n--MIMEBoundary--\r\n";
		MimeHeaders headers = new MimeHeaders();
		headers.setHeader("Content-type", "multipart/related; type=\"text/xml\"; boundary=MIMEBoundary");
		ByteArrayInputStream bais = new ByteArrayInputStream(s.getBytes());
		SOAPMessage message = MessageFactory.newInstance().createMessage(headers, bais);
		String contentId = ((AttachmentPart) message.getAttachments().next()).getContentId();
		message.writeTo(System.out);
		System.out.println("\nContentID: " + contentId);
	}

	@Test
	public void TestSampleMessage() throws FileNotFoundException, IOException, SOAPException, AS4Exception {

		SOAPMessage s = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null,
				new FileInputStream("G:\\testsoap.txt"));
		AS4BundledMessage msg = new AS4BundledMessage(s);
		System.out.println("\n\nxxxx\n" + msg.toString());
		List<AS4Message> l = msg.extractMessages();
		int i = 0;
		for (AS4Message m : l) {
			i++;
			System.out.println("\n\n\n"+i + ":" + m);
			if( m instanceof AS4UserMessage) {
				AS4UserMessage u = (AS4UserMessage)m;
				System.out.println("partId: " + u.getFromPartyId()  + "," + u.getToPartyId());
				System.out.println("partyRole: "+ u.getFromPartyRole() + "," + u.getToPartyRole());
				System.out.println("ConvID: " + u.getConvId());
				System.out.println("Agree: " + u.getAgreeRef());
				System.out.println("Time: " + u.getTimeStamp());
				System.out.println("s&a: " + u.getService() + "," + u.getServiceType()+ "," + u.getAction());
				
			}
		}
	}
}
