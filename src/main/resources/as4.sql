CREATE TABLE message (
    message_id varchar(200),
    message_box varchar(200),
    message_type varchar(200),
    from_party_id varchar(200),
    from_party_role varchar(200),
    to_party_id varchar(200),
    to_party_role varchar(200),
    service varchar(200),
    service_type varchar(200),
    action varchar(200),
    conv_id varchar(200),
    agreement_ref varchar(200),
    ref_to_message_id varchar(200), 
    pmode_id varchar(200),
    is_ack_required varchar(200),
    is_acknowledged varchar(200),
    address varchar(200),
    mpc varchar(200),
    retries int,
    retry_interval int,
    status varchar(200),
    status_description varchar(200),
    time_stamp timestamp,
    timeout_time_stamp timestamp,
    modified_time_stamp timestamp,
    is_read boolean default false,
    PRIMARY KEY (message_id, message_box)
);

CREATE TABLE repository (
    message_id varchar(200),
    content_type varchar(200),
    content bytea,
    modified_time_stamp timestamp,
    message_box varchar(200),
    PRIMARY KEY (message_id, message_box)
);

CREATE TABLE public.pmode
(
  pmode_id character varying(200) NOT NULL,
  agreement character varying(200),
  service character varying(200),
  service_type character varying(200),
  action character varying(200),
  address character varying(200),
  mep_binding character varying(200),
  mpc character varying(200),
  
  init_party_id character varying(200),
  init_party_role character varying(200),
  resp_party_id character varying(200),
  resp_party_role character varying(200),
  
  is_pmode_auth_required boolean DEFAULT false,
  init_username character varying(200),
  init_password character varying(200),
  resp_username character varying(200),
  resp_password character varying(200),
  
  report_receiver_error_to character varying(200),
  is_report_error_as_response boolean DEFAULT false,
  is_report_process_error character varying(200),
  is_report_delivery_failure character varying(200),
  
  is_retry_required character varying(200),
  retries integer,
  retry_interval integer,
  
  is_sign_required character varying(200),
  sign_cert character varying(200),
  sign_hash_func character varying(200),
  sign_algorithm character varying(200),
  
  is_encrypt_required boolean,
  encrypt_cert character varying(200),
  encrypt_algorithm character varying(200),
  
  is_token_required character varying(200),
  token_username character varying(200),
  token_password character varying(200),
  is_token_created_required character varying(200),
  is_token_digest_required character varying(200),
  is_receipt_required character varying(200),
  receipt_reply_pattern character varying(200),
  
  is_disabled character varying(200),
  modified_time_stamp timestamp,
  CONSTRAINT pmode_pkey PRIMARY KEY (pmode_id)
);

