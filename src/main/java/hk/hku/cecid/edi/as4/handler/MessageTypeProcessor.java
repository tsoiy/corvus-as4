package hk.hku.cecid.edi.as4.handler;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.soap.SOAPException;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.dao.MessageDAO;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.MessageStoreDAO;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.RepositoryDAO;
import hk.hku.cecid.edi.as4.dao.RepositoryDVO;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorMessage;
import hk.hku.cecid.edi.as4.pkg.AS4Message;
import hk.hku.cecid.piazza.commons.dao.DAOException;
import lombok.Getter;

public abstract class MessageTypeProcessor {

	private List<MessageDVO> insertMsgs;
	private List<RepositoryDVO> insertDetails;
	private List<MessageDVO> updateMsgs;
	private List<AS4Error> errors;
	protected MessageStoreDAO dao;
	protected RepositoryDAO repoDao;
	protected MessageDAO msgDao;
	protected boolean isReply;
	private static int dbRetryCount = 10;
	static {
		int r = dbRetryCount;
		String retryString = AS4Processor.core.properties.getProperty("as4/db_retry_count");
		if (retryString != null) {
			try {
				r = Integer.parseInt(retryString);
				if (r < 0)
					r = dbRetryCount;
			} catch (NumberFormatException e) {
			}
			dbRetryCount = r;
		}
	}

	@Getter
	protected PmodeDVO pmode;

	protected void addError(AS4Error e) {
		errors.add(e);
	}

	protected void addInsert(MessageDVO message, RepositoryDVO detail) {
		insertMsgs.add(message);
		insertDetails.add(detail);
	}

	protected void addUpdate(MessageDVO message) {
		updateMsgs.add(message);
	}

	abstract void reset();

	abstract AS4Message process() throws AS4Exception;

	public AS4Message handle() throws DAOException {
		int retryCount = dbRetryCount;
		AS4Message reply = null;
		do {
			insertMsgs.clear();
			insertDetails.clear();
			updateMsgs.clear();
			errors.clear();
			try {
				reply = process();
				AS4Processor.core.log.debug("Message processed with no error");
			} catch (AS4Exception e) {
				addError(e.getError());
				AS4Processor.core.log.info("Message processed with error: " + e.getError().getShortDescription());
			}
			if (!errors.isEmpty()) {
				try {
					AS4ErrorMessage errorMessage = new AS4ErrorMessage();
					PmodeDVO pmode = null;
					for (AS4Error e : errors) {
						errorMessage.addError(e);
						// all errors should have the same pmode
						if (pmode == null && e.getPmode() != null)
							pmode = e.getPmode();
					}
					MessageDVO errorDvo = (MessageDVO) dao.createMessageDVO();
					RepositoryDVO errorDetails = (RepositoryDVO) dao.createRepositoryDVO();
					reply = errorMessage;
					if (!AS4DvoConvertor.convertOutboundErrorMessage((AS4ErrorMessage) errorMessage, pmode, errorDvo,
							errorDetails, isReply))
						reply = null;
					addInsert(errorDvo, errorDetails);
				} catch (SOAPException e) {
					AS4Processor.core.log.error("Error in constructing error message", e);
				}
			}
			try {
				dao.updateAndInsert(updateMsgs, insertMsgs, insertDetails);
				retryCount = 0;
			} catch (ConcurrentModificationException e2) {
				reset();
				retryCount--;
				String msgIds = updateMsgs.stream().map(u -> u.getMessageId()).collect(Collectors.joining(","));
				AS4Processor.core.log.info("Concurrent update detected when updating messages of ID (" + msgIds
						+ ") retry count: " + retryCount);
			}
		} while (retryCount > 0);
		return reply;
	}

	public MessageTypeProcessor(boolean b) throws DAOException {
		insertMsgs = new ArrayList<>();
		insertDetails = new ArrayList<>();
		updateMsgs = new ArrayList<>();
		errors = new ArrayList<>();
		isReply = b;

		dao = (MessageStoreDAO) AS4Processor.core.dao.createDAO(MessageStoreDAO.class);
		msgDao = (MessageDAO) AS4Processor.core.dao.createDAO(MessageDAO.class);
		repoDao = (RepositoryDAO) AS4Processor.core.dao.createDAO(RepositoryDAO.class);
	}
}
