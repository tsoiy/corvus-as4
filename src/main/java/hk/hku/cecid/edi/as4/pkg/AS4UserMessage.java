package hk.hku.cecid.edi.as4.pkg;

import static hk.hku.cecid.edi.as4.pkg.AS4MessageConstant.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.xml.namespace.QName;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.model.Attachment;
import hk.hku.cecid.piazza.commons.util.Generator;

public class AS4UserMessage extends AbstractAS4Message implements AS4Message {
	private SOAPElement userMsgNode;
	private SOAPElement partyInfo;
	private SOAPElement collabInfo;
	private SOAPElement messageProperties;

	private SOAPElement fromNode;
	private SOAPElement toNode;
	private SOAPElement agreementRef;
	private SOAPElement service;
	private SOAPElement action;
	private SOAPElement conversationId;
	private SOAPElement payloadInfo;

	private Set<String> attachmentIds;

	public AS4UserMessage(byte[] rawMessage) throws AS4Exception {
		super(rawMessage);
		fromSOAP();
	}

	public AS4UserMessage(SOAPMessage msg) throws AS4Exception {
		super(msg);
		fromSOAP();
	}

	private void fromSOAP() throws AS4Exception {
		attachmentIds = new HashSet<>();
		userMsgNode = findRequiredNode(EB3_NS_URL, msgNode, EB3_HEADER_USERMSG);
		partyInfo = findRequiredNode(EB3_NS_URL, userMsgNode, EB3_USERMSG_PARTYINFO);
		msgInfo = findRequiredNode(EB3_NS_URL, userMsgNode, EB3_HEADER_MSGINFO);
		collabInfo = findRequiredNode(EB3_NS_URL, userMsgNode, EB3_USERMSG_COLINFO);
		fromNode = findRequiredNode(EB3_NS_URL, partyInfo, EB3_USERMSG_FROM);
		toNode = findRequiredNode(EB3_NS_URL, partyInfo, EB3_USERMSG_TO);

		service = findRequiredNode(EB3_NS_URL, collabInfo, EB3_USERMSG_SERVICE);
		action = findRequiredNode(EB3_NS_URL, collabInfo, EB3_USERMSG_ACTION);
		conversationId = findRequiredNode(EB3_NS_URL, collabInfo, EB3_USERMSG_CONVID);
		agreementRef = findOptionalNode(EB3_NS_URL, collabInfo, EB3_USERMSG_AGREEREF, true);
		payloadInfo = findOptionalNode(EB3_NS_URL, userMsgNode, EB3_USERMSG_PAYLOADINFO, true);
		messageProperties = findOptionalNode(EB3_NS_URL, userMsgNode, EB3_USERMSG_MSGPROPERTIES, true);

		// verify the attachments
		if (payloadInfo != null) {
			QName nodeqname = new QName(EB3_NS_URL, EB3_USERMSG_PARTINFO);
			Iterator<SOAPElement> it = payloadInfo.getChildElements(nodeqname);
			while (it.hasNext()) {
				SOAPElement partInfo = it.next();
				String id = partInfo.getAttribute("href");
				if (id != null && !id.isEmpty()) {
					attachmentIds.add(id.replaceAll("cid:", ""));
				}
			}
			Iterator<AttachmentPart> parts = soapMessage.getAttachments();
			Set<String> includedIds = new HashSet<>();
			parts.forEachRemaining(p -> {
				includedIds.add(p.getContentId().replaceAll("<|>", ""));
			});

			if (!includedIds.containsAll(attachmentIds))
				throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER, "Missing attachment(s)"));
		}
	}

	public AS4UserMessage() throws SOAPException {
		attachmentIds = new HashSet<>();
		userMsgNode = msgNode.addChildElement(EB3_HEADER_USERMSG, EB3_PREFIX);
		addMessageInfoNode(userMsgNode);
		partyInfo = userMsgNode.addChildElement(EB3_USERMSG_PARTYINFO, EB3_PREFIX);
		fromNode = partyInfo.addChildElement(EB3_USERMSG_FROM, EB3_PREFIX);
		toNode = partyInfo.addChildElement(EB3_USERMSG_TO, EB3_PREFIX);

		collabInfo = userMsgNode.addChildElement(EB3_USERMSG_COLINFO, EB3_PREFIX);
		agreementRef = collabInfo.addChildElement(EB3_USERMSG_AGREEREF, EB3_PREFIX);
		service = collabInfo.addChildElement(EB3_USERMSG_SERVICE, EB3_PREFIX);
		service.setAttribute("type", "type:services");
		action = collabInfo.addChildElement(EB3_USERMSG_ACTION, EB3_PREFIX);
		conversationId = collabInfo.addChildElement(EB3_USERMSG_CONVID, EB3_PREFIX);

		payloadInfo = userMsgNode.addChildElement(EB3_USERMSG_PAYLOADINFO, EB3_PREFIX);

		messageProperties = userMsgNode.addChildElement(EB3_USERMSG_MSGPROPERTIES, EB3_PREFIX);
	}

	private void addAttachmentPart(AttachmentPart attachment) throws SOAPException {
		soapMessage.addAttachmentPart(attachment);
		SOAPElement part = payloadInfo.addChildElement(EB3_USERMSG_PARTINFO, EB3_PREFIX);
		String contentId = Generator.generateMessageID();
		attachment.setContentId("<" + contentId + ">");
		part.setAttribute("href", "cid:" + contentId);
		attachmentIds.add(contentId);
	}

	public void addAttachment(byte[] rawdata, String filename, String contentType) throws SOAPException {
		Attachment a = new Attachment();
		a.setContent(rawdata);
		a.setContentType(contentType);
		a.setFilename(filename);
		addAttachment(a);
	}
	
	public void addAttachment(Attachment a) throws SOAPException {
		AttachmentPart attachment = soapMessage.createAttachmentPart();
		attachment.setRawContentBytes(a.getContent(), 0, a.getContent().length, a.getContentType());
		if( a.getContentType() != null)
			attachment.setContentType(a.getContentType());
		if( a.getFilename() != null )
			attachment.setMimeHeader("Content-Disposition", a.constructDispositionHeader());
		addAttachmentPart(attachment);
	}

	void addMessageProperty(String name, String value) throws SOAPException {
		SOAPElement p = messageProperties.addChildElement(EB3_USERMSG_PROPERTY, EB3_PREFIX);
		p.setAttribute("name", name);
		p.setTextContent(value);
		// soapMessage.saveChanges();
	}

	public String getFromPartyId() {
		return getChildValue(EB3_NS_URL, fromNode, EB3_USERMSG_PARTYID);
	}

	public void setFromPartyId(String partyId) throws SOAPException {
		setChildValue(EB3_NS_URL, EB3_PREFIX, fromNode, EB3_USERMSG_PARTYID, partyId);
	}

	public String getFromPartyRole() {
		return getChildValue(EB3_NS_URL, fromNode, EB3_USERMSG_ROLE);
	}

	public void setFromPartyRole(String partyRole) throws SOAPException {
		setChildValue(EB3_NS_URL, EB3_PREFIX, fromNode, EB3_USERMSG_ROLE, partyRole);
	}

	public String getToPartyId() {
		return getChildValue(EB3_NS_URL, toNode, EB3_USERMSG_PARTYID);
	}

	public void setToPartyId(String partyId) throws SOAPException {
		setChildValue(EB3_NS_URL, EB3_PREFIX, toNode, EB3_USERMSG_PARTYID, partyId);
	}

	public String getToPartyRole() {
		return getChildValue(EB3_NS_URL, toNode, EB3_USERMSG_ROLE);
	}

	public void setToPartyRole(String partyRole) throws SOAPException {
		setChildValue(EB3_NS_URL, EB3_PREFIX, toNode, EB3_USERMSG_ROLE, partyRole);
	}

	public String getAction() {
		return action.getTextContent();
	}

	public void setAction(String act) {
		action.setTextContent(act);
	}

	public String getService() {
		return service.getTextContent();
	}

	public void setService(String serviceStr) {
		service.setTextContent(serviceStr);
	}

	public String getServiceType() {
		return service.getAttribute("type");
	}

	public void setServiceType(String serviceType) {
		service.setAttribute("type", serviceType);
	}

	public String getAgreeRef() {
		return (agreementRef == null ? null : agreementRef.getTextContent());
	}

	public void setAgreeRef(String ref) {
		agreementRef.setTextContent(ref);
	}

	public String getConvId() {
		return (conversationId == null ? null : conversationId.getTextContent());
	}

	public void setConvId(String id) {
		conversationId.setTextContent(id);
	}

	public String getPmodeId() {
		return (agreementRef == null ? null : agreementRef.getAttribute("pmode"));
	}

	public void setPmodeId(String pmodeId) {
		agreementRef.setAttribute("pmode", pmodeId);
	}

	public String getAgreementType() {
		return (agreementRef == null ? null : agreementRef.getAttribute("type"));
	}

	public void setAgreementType(String agreementType) {
		agreementRef.setAttribute("type", agreementType);
	}

	public SOAPElement getUserMessageNode() {
		return userMsgNode;
	}

	public String getMpc() {
		return userMsgNode.getAttribute("mpc");
	}

	public void setMpc(String mpc) {
		userMsgNode.setAttribute("mpc", mpc);
	}

	public void setBody(String bodytext) throws SOAPException {
		body.setTextContent(bodytext);
		body.addAttribute(new QName(AS4MessageConstant.EB3_WSU_URL, "Id", "wsu"), "id-body");
	}
	
	public Set<String> getAttachmentIds () {
		return attachmentIds;
	}
}