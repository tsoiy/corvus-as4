package hk.hku.cecid.edi.as4.model;

import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorCode;
import lombok.Getter;
import lombok.Setter;

public class AS4Error {
	private String extraDescription;

	@Setter
	@Getter
	private String refId;
	@Setter
	@Getter
	private PmodeDVO pmode;
	@Getter
	private final AS4ErrorCode code;

	public AS4Error(AS4ErrorCode code) {
		this(code, null, null, null);
	}
	
	public AS4Error(AS4ErrorCode code, String description) {
		this(code, description, null, null);
	}

	public AS4Error(AS4ErrorCode code, String description, String refId) {
		this(code, description, refId, null);
	}
	
	public AS4Error(AS4ErrorCode code, String description, String refId, PmodeDVO pmode) {
		this.code = code;
		this.extraDescription = description;
		this.refId = refId;
		this.pmode = pmode;
	}
	
	public String getShortDescription() {
		return code.getDescription();
	}
	
	public String getDetailDescription() {
		StringBuilder sb = new StringBuilder();
		sb.append(code.getDescription());
		if( extraDescription != null ) {
			sb.append(" (Detail: ").append(extraDescription).append(")");
		}
		if( refId != null ) {
			sb.append(" (RefId: ").append(refId).append(")");
		}
		return sb.toString();
	}

}
