package hk.hku.cecid.edi.as4.dao;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map;

import hk.hku.cecid.piazza.commons.dao.DAO;
import hk.hku.cecid.piazza.commons.dao.DAOException;

/**
 * 
 */
public interface MessageStoreDAO extends DAO {

	public void storeMessage(MessageDVO[] messageDVO, RepositoryDVO[] repositoryDVO) throws DAOException;

	public void storeMessage(MessageDVO messageDVO, RepositoryDVO repositoryDVO) throws DAOException;

	public MessageDVO createMessageDVO();

	public RepositoryDVO createRepositoryDVO();

	void updateAndInsert(List<MessageDVO> updateMsgs, List<MessageDVO> insertMsgs, List<RepositoryDVO> insertDetails)
			throws DAOException, ConcurrentModificationException;
	
}