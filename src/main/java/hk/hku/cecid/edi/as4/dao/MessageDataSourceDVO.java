package hk.hku.cecid.edi.as4.dao;

import java.util.Date;

import hk.hku.cecid.piazza.commons.dao.ds.DataSourceDVO;
import hk.hku.cecid.piazza.commons.util.Convertor;

/**
 *  
 */
public class MessageDataSourceDVO extends DataSourceDVO implements MessageDVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8389084480956449116L;

	public MessageDataSourceDVO() {
		super();
	}

	/**
	 * @return Returns the messageId.
	 */
	@Override
	public String getMessageId() {
		return super.getString("messageId");
	}

	/**
	 * @param messageId
	 *            The messageId to set.
	 */
	@Override
	public void setMessageId(String messageId) {
		super.setString("messageId", messageId);
	}

	/**
	 * @return Returns the messageBox.
	 */
	@Override
	public String getMessageBox() {
		return super.getString("messageBox");
	}

	/**
	 * @param messageBox
	 *            The messageBox to set.
	 */
	@Override
	public void setMessageBox(String messageBox) {
		super.setString("messageBox", messageBox);
	}

	/**
	 * @return Returns the timeStamp.
	 */
	@Override
	public Date getTimeStamp() {
		return super.getDate("timeStamp");
	}

	/**
	 * @param timeStamp
	 *            The timeStamp to set.
	 */
	@Override
	public void setTimeStamp(Date timeStamp) {
		super.put("timeStamp", Convertor.toTimestamp(timeStamp));
	}

	/**
	 * @return Returns the status.
	 */
	@Override
	public String getStatus() {
		return super.getString("status");
	}

	/**
	 * @param status
	 *            The service to set.
	 */
	@Override
	public void setStatus(String status) {
		super.setString("status", status);
	}


	/**
	 * setIsAcknowledged
	 * 
	 * @param isAcknowledged
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setIsAcknowledged(boolean)
	 */
	@Override
	public void setIsAcknowledged(boolean isAcknowledged) {
		super.setString("isAcknowledged", String.valueOf(isAcknowledged));
	}


	@Override
	public void setIsAckRequired(boolean isAckRequired) {
		super.setString("isAckRequired", String.valueOf(isAckRequired));
	}
	
	@Override
	public boolean isAckRequired() {
		return super.getBoolean("isAckRequired");
	}

	/**
	 * isAcknowledged
	 * 
	 * @return @see hk.hku.cecid.edi.as4.dao.MessageDVO#isAcknowledged()
	 */
	@Override
	public boolean isAcknowledged() {
		return super.getBoolean("isAcknowledged");
	}


	/**
	 * setStatusDescription
	 * 
	 * @param desc
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setStatusDescription(java.lang.String)
	 */
	@Override
	public void setStatusDescription(String desc) {
		super.setString("statusDesc", desc);
	}

	/**
	 * getStatusDescription
	 * 
	 * @return String
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getStatusDescription()
	 */
	@Override
	public String getStatusDescription() {
		return super.getString("statusDesc");
	}

	/**
	 * Return String
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getFromPartyId()
	 */
	@Override
	public String getFromPartyId() {
		return super.getString("fromPartyId");
	}

	/**
	 * Set the fromPartyId
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setFromPartyId(java.lang.String)
	 */
	@Override
	public void setFromPartyId(String fromPartyId) {
		super.setString("fromPartyId", fromPartyId);

	}

	/**
	 * Return the fromPartyRole
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getFromPartyRole()
	 */
	@Override
	public String getFromPartyRole() {
		return super.getString("fromPartyRole");
	}

	/**
	 * Set fromPartyRole
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setFromPartyRole(java.lang.String)
	 */
	@Override
	public void setFromPartyRole(String fromPartyRole) {
		super.setString("fromPartyRole", fromPartyRole);
	}

	/**
	 * Return to_party_id
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getToPartyId()
	 */
	@Override
	public String getToPartyId() {
		return super.getString("toPartyId");
	}

	/**
	 * Set toPartyId
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setToPartyId(java.lang.String)
	 */
	@Override
	public void setToPartyId(String toPartyId) {
		super.setString("toPartyId", toPartyId);

	}

	/**
	 * Return toPartyRole
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getToPartyRole()
	 */
	@Override
	public String getToPartyRole() {
		return super.getString("toPartyRole");
	}

	/**
	 * Set toPartyRole
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setToPartyRole(java.lang.String)
	 */
	@Override
	public void setToPartyRole(String toPartyRole) {
		super.setString("toPartyRole", toPartyRole);
	}

	/**
	 * Return refToMessageId
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getRefToMessageId()
	 */
	@Override
	public String getRefToMessageId() {
		return super.getString("refToMessageId");
	}

	/**
	 * Set refToMessageId
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setRefToMessageId(java.lang.String)
	 */
	@Override
	public void setRefToMessageId(String refToMessageId) {
		super.setString("refToMessageId", refToMessageId);
	}

	/**
	 * Return agreementRef
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getAgreementRef()
	 */
	@Override
	public String getAgreementRef() {
		return super.getString("agreementRef");
	}

	/**
	 * Set agreementRef
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setAgreementRef(java.lang.String)
	 */
	@Override
	public void setAgreementRef(String agreementRef) {
		super.setString("agreementRef", agreementRef);

	}

	/**
	 * Return service
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getServiceType()
	 */
	@Override
	public String getServiceType() {
		return super.getString("serviceType");
	}

	/**
	 * Set service
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setServiceType(java.lang.String)
	 */
	@Override
	public void setServiceType(String serviceType) {
		super.setString("serviceType", serviceType);

	}

	/**
	 * Return address
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getAddress()
	 */
	@Override
	public String getAddress() {
		return super.getString("address");
	}

	/**
	 * Set address
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setAddress(java.lang.String)
	 */
	@Override
	public void setAddress(String address) {
		super.setString("address", address);

	}
	
	/**
	 * Return action
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getAction()
	 */
	@Override
	public String getAction() {
		return super.getString("action");
	}

	/**
	 * Set action
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setAction(java.lang.String)
	 */
	@Override
	public void setAction(String action) {
		super.setString("action", action);
	}

	/**
	 * Return pmodeId
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getPartnerShipId()
	 */
	@Override
	public String getPmodeId() {
		return super.getString("pmodeId");
	}

	/**
	 * Set pmodeId
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setPartnerShipId(java.lang.String)
	 */
	@Override
	public void setPmodeId(String pmodeId) {
		super.setString("pmodeId", pmodeId);

	}

	/**
	 * Get convId
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#getConvId()
	 */
	@Override
	public String getConvId() {
		return super.getString("convId");
	}

	/**
	 * Set convId
	 * 
	 * @see hk.hku.cecid.edi.as4.dao.MessageDVO#setConvId(java.lang.String)
	 */
	@Override
	public void setConvId(String convId) {
		super.setString("convId", convId);
	}

	@Override
	public String getService() {
		return super.getString("service");
	}

	@Override
	public void setService(String service) {
		super.setString("service", service);

	}

	@Override
	public Date getTimeOutTimeStamp() {
		return super.getDate("timeOutTimeStamp");
	}

	/**
	 * @param timeStamp
	 *            The timeStamp to set.
	 */
	@Override
	public void setTimeOutTimeStamp(Date timeoutTimeStamp) {
		super.put("timeOutTimeStamp", Convertor.toTimestamp(timeoutTimeStamp));

	}

	@Override
	public Date getModifiedTimeStamp() {
		return super.getDate("modifiedTimeStamp");
	}

	/**
	 * @param timeStamp
	 *            The timeStamp to set.
	 */
	@Override
	public void setModifiedTimeStamp(Date modifiedTimeStamp) {
		super.put("modifiedTimeStamp", Convertor.toTimestamp(modifiedTimeStamp));

	}

	@Override
	public int getRetries() {
		return super.getInt("retries");
	}

	/**
	 * @param timeStamp
	 *            The timeStamp to set.
	 */
	@Override
	public void setRetries(int retries) {
		super.put("retries", retries);

	}
	
	@Override
	public String getMpc() {
		return super.getString("mpc");
	}

	@Override
	public void setMpc(String mpc) {
		super.setString("mpc", mpc);

	}

	@Override
	public String getMessageType() {
		return super.getString("messageType");
	}
	
	@Override
	public void setMessageType(String type) {
		super.setString("messageType", type);
	}
	

	@Override
	public void setIsRead(boolean isRead) {
		super.setString("isRead", String.valueOf(isRead));
	}
	
	@Override
	public boolean isRead() {
		return super.getBoolean("isRead");
	}

}