package hk.hku.cecid.edi.as4.pkg;

import static hk.hku.cecid.edi.as4.pkg.AS4MessageConstant.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import lombok.Getter;

public class AS4ReceiptMessage extends AbstractAS4Message implements AS4Message {

	@Getter
	private SOAPElement signalNode;

	@Getter
	private SOAPElement receiptNode;

	private SOAPElement userNode;
	private SOAPElement nriNode;
	private final static QName NRI_NODE_QNAME = new QName(EB3_EBBPSIG_URL, EB3_RECEIPT_EBBPSIG_NRI, EB3_EBBPSIG_PREFIX);
	private final static QName NRI_MSGPART_QNAME = new QName(EB3_EBBPSIG_URL, EB3_RECEIPT_EBBPSIG_MSGPARTNRI,
			EB3_EBBPSIG_PREFIX);
	private final static QName DS_SIGNATURE_NODE = new QName(EB3_DS_URL, EB3_SECURITY_SIGNATURE, EB3_DS_PREFIX);
	private final static QName DS_SIGNEDINFO_NODE = new QName(EB3_DS_URL, EB3_SECURITY_SIGNEDINFO, EB3_DS_PREFIX);
	private final static QName DS_REFERENCE_NODE = new QName(EB3_DS_URL, EB3_SECURITY_REFERENCE, EB3_DS_PREFIX);

	public AS4ReceiptMessage(SOAPMessage message, SOAPElement signalMsgNode) throws AS4Exception {
		super(message);
		this.signalNode = signalMsgNode;
		msgInfo = findRequiredNode(EB3_NS_URL, signalMsgNode, EB3_HEADER_MSGINFO);
		receiptNode = findRequiredNode(EB3_NS_URL, signalMsgNode, EB3_RECEIPTMSG_RECIEPT);
		userNode = findOptionalNode(EB3_NS_URL, receiptNode, EB3_HEADER_USERMSG, true);
		nriNode = findOptionalNode(EB3_EBBPSIG_URL, receiptNode, EB3_RECEIPT_EBBPSIG_NRI, true);
		if (userNode == null && nriNode == null)
			throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER, "Missing receiptNode details"));
		if (userNode != null && nriNode != null)
			throw new AS4Exception(
					new AS4Error(AS4ErrorCode.INVALID_HEADER, "Signed and unsigned receiptNode details both exist"));

	}

	public AS4ReceiptMessage() throws SOAPException {
		signalNode = msgNode.addChildElement(EB3_HEADER_SIGNALMSG, EB3_PREFIX);
		addMessageInfoNode(signalNode);
		receiptNode = signalNode.addChildElement(EB3_RECEIPTMSG_RECIEPT, EB3_PREFIX);
	}

	public void addReceiptDetail(AS4UserMessage msg, boolean requireSigning) throws AS4Exception, SOAPException {
		SOAPHeaderElement secHeader = msg.getSecurityHeader("");
		if (requireSigning && secHeader == null) {
			AS4Processor.core.log
					.error("Receipt requires signing but the incoming message (" + msg.getMessageId() + ") not signed");
			throw new AS4Exception(new AS4Error(AS4ErrorCode.PMODE_MISMATCH));
		}
		if (requireSigning) {
			if (nriNode != null)
				receiptNode.removeChild(nriNode);
			nriNode = receiptNode.addChildElement(NRI_NODE_QNAME);
			List<SOAPElement> signatures = new ArrayList<>();
			Iterator<SOAPElement> signature = secHeader.getChildElements(DS_SIGNATURE_NODE);
			while (signature.hasNext()) {
				SOAPElement signatureNode = signature.next();
				Iterator<SOAPElement> signed = signatureNode.getChildElements(DS_SIGNEDINFO_NODE);
				while (signed.hasNext()) {
					SOAPElement signedNode = signed.next();
					Iterator<SOAPElement> reference = signedNode.getChildElements(DS_REFERENCE_NODE);
					while (reference.hasNext()) {
						SOAPElement referenceNode = reference.next();
						SOAPElement msgpart = nriNode.addChildElement(NRI_MSGPART_QNAME);
						msgpart.addChildElement((SOAPElement) referenceNode.cloneNode(true));
					}
				}
			}
		} else {
			if (userNode != null)
				receiptNode.removeChild(userNode);
			userNode = receiptNode.addChildElement((SOAPElement) msg.getUserMessageNode().cloneNode(true));
		}

	}

	public String getAction() {
		SOAPElement colInfo;
		try {
			colInfo = findRequiredNode(EB3_NS_URL, userNode, EB3_USERMSG_COLINFO);
			if (colInfo != null) {
				return getChildValue(EB3_NS_URL, userNode, EB3_USERMSG_ACTION);
			}
		} catch (AS4Exception e) {
		}
		return null;

	}

	public String getService() {
		SOAPElement colInfo;
		try {
			colInfo = findRequiredNode(EB3_NS_URL, userNode, EB3_USERMSG_COLINFO);
			if (colInfo != null) {
				return getChildValue(EB3_NS_URL, userNode, EB3_USERMSG_SERVICE);
			}

		} catch (AS4Exception e) {
		}
		return null;
	}

}
