package hk.hku.cecid.edi.as4.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MshMessage {
	public enum RequestType {
		UserMessage, PullRequest
	}
	public enum Status {
		SUCCESS, PMODE_ERROR, DB_READ_ERROR, DB_WRITE_ERROR, OTHERS
	}
	private Object source = null;
	// private AS4Message message = null;

	private String pmodeId;
	private RequestType type;
	private Status status;
	private String result;
	private String msgId;
	private String timeStamp;
	private String convId;
	private String refId;
	private List<Attachment>attachments;

	public MshMessage() {
		this(null);
	}

	public MshMessage(Object source) {
		this.source = source;
	}

}
