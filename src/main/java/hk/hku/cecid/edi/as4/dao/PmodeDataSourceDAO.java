package hk.hku.cecid.edi.as4.dao;

import java.util.Date;
import java.util.List;

import hk.hku.cecid.edi.as4.pkg.AS4MessageConstant;
import hk.hku.cecid.piazza.commons.dao.DAOException;
import hk.hku.cecid.piazza.commons.dao.DVO;
import hk.hku.cecid.piazza.commons.dao.ds.DataSourceDAO;
import hk.hku.cecid.piazza.commons.dao.ds.DataSourceDVO;

/**
 * 
 *  
 */
public class PmodeDataSourceDAO extends DataSourceDAO implements PmodeDAO {

	@Override
	public List<PmodeDVO> findAllPmodes() throws DAOException {
		return (List<PmodeDVO>) super.find("find_all_pmodes", null);
	}

	@Override
	public DVO createDVO() {
		return new PmodeDataSourceDVO();
	}

	@Override
	public PmodeDVO findByPmodeId(String pmodeId) throws DAOException {
		List<?> l = super.find("find_pmode_by_pmode_id", new Object[] { pmodeId });
		if (l.size() > 0) {
			return (PmodeDVO) l.get(0);
		} else
			return null;
	}

	@Override
	public boolean findByServiceActionAgreement(PmodeDVO dvo) throws DAOException {
		List<?> l = super.find("find_pmode_by_service_action_agreement",
				new Object[] { dvo.getService(), dvo.getAction(), dvo.getAgreement() });
		if (l.size() > 0) {
			DataSourceDVO d = (DataSourceDVO) l.get(0);
			((DataSourceDVO) dvo).setData(d.getData());
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void create(DVO data) throws DAOException {
		((PmodeDVO) data).setModifiedTimeStamp(new Date());
		super.create(data);
	}

	@Override
	public boolean persist(DVO data) throws DAOException {
		((PmodeDVO) data).setModifiedTimeStamp(new Date());
		return super.persist(data);
	}

	@Override
	public List<PmodeDVO> findPmodeForPullByMpc(String mpc) throws DAOException {
		return super.find("find_pull_pmode_by_mpc", new Object[] { mpc, AS4MessageConstant.EB3_MEP_BINDING_PULL_URL });
	}

	@Override
	public List<PmodeDVO> findPmodeForPullByDefaultMpc() throws DAOException {
		return super.find("find_pull_pmode_by_default_mpc", new Object[] { AS4MessageConstant.EB3_DEFAULT_MPC, AS4MessageConstant.EB3_MEP_BINDING_PULL_URL });
	}	
}