package hk.hku.cecid.edi.as4.handler;

import javax.xml.soap.SOAPException;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.MessageStoreConstant;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.RepositoryDVO;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorMessage;
import hk.hku.cecid.edi.as4.pkg.AS4Message;
import hk.hku.cecid.piazza.commons.dao.DAOException;

public class ErrorMessageProcessor extends MessageTypeProcessor {
	private AS4ErrorMessage errmsg;
	MessageDVO originalMessage;

	public ErrorMessageProcessor(AS4ErrorMessage errmsg) throws DAOException {
		super(false);
		this.errmsg = errmsg;
	}

	public ErrorMessageProcessor(AS4ErrorMessage errmsg, MessageDVO originalMessage, PmodeDVO pmode)
			throws DAOException {
		super(true);
		this.errmsg = errmsg;
		this.originalMessage = originalMessage;
		this.pmode = pmode;
	}

	@Override
	void reset() {
	}

	@Override
	AS4Message process() throws AS4Exception {
		MessageDVO dvo = dao.createMessageDVO();
		String refId = null;
		AS4DvoConvertor.toMessageDvo(errmsg, dvo);
		dvo.setMessageBox(MessageStoreConstant.INBOX);
		dvo.setStatus(MessageStoreConstant.STATUS_PROCESSED);
		RepositoryDVO repoDvo = dao.createRepositoryDVO();
		AS4DvoConvertor.toRepositoryDvo(errmsg, repoDvo);
		repoDvo.setMessageBox(MessageStoreConstant.INBOX);
		addInsert(dvo, repoDvo);

		try {
			refId = errmsg.getRefMsgId();
			if (refId!= null && refId.isEmpty())
				refId = null;
		} catch (SOAPException e) {
			AS4Processor.core.log.error("Failed to retrieve reference message ID from error message", e);
		}

		// try to retrieve the original message with the refId
		if (originalMessage == null && refId != null) {
			originalMessage = (MessageDVO) dao.createDVO();
			originalMessage.setMessageBox(MessageStoreConstant.OUTBOX);
			originalMessage.setMessageId(refId);
			String pmodeId = null;
			try {
				if (!dao.retrieve(originalMessage)) {
					AS4Processor.core.log.warn("Error message (" + dvo.getMessageId() + ") reference message (" + refId
							+ ") is not found");
					dvo.setStatus(MessageStoreConstant.STATUS_PROCESSED_ERROR);
					dvo.setStatusDescription("Reference message not found");
					originalMessage = null;
				} else {
					pmodeId = originalMessage.getPmodeId();
					PmodeHandler.getInstance().findByPmodeId(pmodeId);
					if (pmode == null) {
						AS4Processor.core.log.warn("Pmode (" + pmodeId + ") of message (" + refId + ") is not found");
					}
				}
			} catch (DAOException e) {
				AS4Processor.core.log.error(
						"Failed to retrieve message (" + refId + ") or pmode (" + pmodeId + ") from database", e);
			}
		}
		if (originalMessage != null) {
			if (refId == null || !refId.equals(originalMessage.getMessageId())) {
				AS4Processor.core.log.info("The incoming reply error message ID does not match the request message req:"
						+ originalMessage.getMessageId() + " rep:" + refId);
			}
		}
		if( isReply && originalMessage != null) {
			addUpdate(originalMessage);
		}
		return null;
	}
}
