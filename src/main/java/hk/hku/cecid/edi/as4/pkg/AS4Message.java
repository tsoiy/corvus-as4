package hk.hku.cecid.edi.as4.pkg;

import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;

public interface AS4Message  {
	
	public String getMessageId();
	
	public String getContentType();
	
	public SOAPHeaderElement getSecurityHeader(String role);
	
	public String toString();
	
	public byte[] toByteArray();
	
	public SOAPMessage getSOAP();
	
}
