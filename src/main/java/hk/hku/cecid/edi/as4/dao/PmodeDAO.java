package hk.hku.cecid.edi.as4.dao;

import hk.hku.cecid.piazza.commons.dao.DAO;
import hk.hku.cecid.piazza.commons.dao.DAOException;

import java.util.List;

/**
 * @author Donahue Sze
 * 
 */
public interface PmodeDAO extends DAO {

	public List<PmodeDVO> findAllPmodes() throws DAOException;

	public PmodeDVO findByPmodeId(String pmodeId) throws DAOException;
	
	public List<PmodeDVO> findPmodeForPullByMpc(String mpc) throws DAOException;
	
	public List<PmodeDVO> findPmodeForPullByDefaultMpc() throws DAOException;	
	
	public boolean findByServiceActionAgreement(PmodeDVO dvo) throws DAOException;


}