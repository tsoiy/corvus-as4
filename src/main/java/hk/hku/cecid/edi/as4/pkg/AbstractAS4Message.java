package hk.hku.cecid.edi.as4.pkg;

import static hk.hku.cecid.edi.as4.pkg.AS4MessageConstant.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.Iterator;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.piazza.commons.util.Generator;

public abstract class AbstractAS4Message {
	protected SOAPBody body;
	protected SOAPEnvelope envelope;
	protected SOAPMessage soapMessage;
	protected SOAPHeader hdr;
	protected SOAPHeaderElement msgNode;
	protected SOAPHeaderElement securityNode;
	protected SOAPElement msgInfo;

	public SOAPMessage getSOAP() {
		return soapMessage;
	}

	public String getContentType() {
		return String.join("; ", soapMessage.getMimeHeaders().getHeader("Content-Type"));
	}

	public byte[] toByteArray() {
		ByteArrayOutputStream baos = new ByteArrayOutputStream(8192);
		try {
			soapMessage.writeTo(baos);
			return (baos.toByteArray());
		} catch (SOAPException | IOException e) {
			AS4Processor.core.log.error("Failed in converting soap message to byte array", e);
		}
		return null;
	}

	AbstractAS4Message(byte[] rawMessage) throws AS4Exception {
		ByteArrayInputStream in = new ByteArrayInputStream(rawMessage);

		SOAPMessage soap = null;
		try {
			soap = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null, in);
		} catch (IOException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.OTHER_ERROR));
		} catch (SOAPException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER));
		}
		fromSOAP(soap);
	}

	AbstractAS4Message(SOAPMessage message) throws AS4Exception {
		fromSOAP(message);
	}

	private void fromSOAP(SOAPMessage message) throws AS4Exception {
		try {
			this.soapMessage = message;

			SOAPPart soapPart = soapMessage.getSOAPPart();
			envelope = soapPart.getEnvelope();
			hdr = envelope.getHeader();
			msgNode = (SOAPHeaderElement) findRequiredNode(EB3_NS_URL, hdr, EB3_HEADER_MESSAGING);

			if (!msgNode.getMustUnderstand()) {
				throw new AS4Exception(
						new AS4Error(AS4ErrorCode.INVALID_HEADER, "Message header must understand not true"));
			}
		} catch (SOAPException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER,
					"Parse error of incoming soap message: " + e.getMessage()));
		}
	}

	AbstractAS4Message() throws SOAPException {
		soapMessage = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage();
		soapMessage.setProperty(SOAPMessage.WRITE_XML_DECLARATION, "true");
		SOAPPart soapPart = soapMessage.getSOAPPart();
		envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(EB3_PREFIX, EB3_NS_URL);
		hdr = envelope.getHeader();
		QName mes = new QName(EB3_NS_URL, EB3_HEADER_MESSAGING, EB3_PREFIX);
		msgNode = hdr.addHeaderElement(mes);
		msgNode.setMustUnderstand(true);
		body = soapMessage.getSOAPBody();

	}

	protected void addMessageInfoNode(SOAPElement basenode) throws SOAPException {
		msgInfo = basenode.addChildElement(EB3_HEADER_MSGINFO, EB3_PREFIX);
		msgInfo.addChildElement(EB3_HEADER_TIMESTAMP, EB3_PREFIX);
		msgInfo.addChildElement(EB3_HEADER_MSGID, EB3_PREFIX);
		setMessageId(Generator.generateMessageID());
		setTimeStamp(new Date());
	}

	protected SOAPElement findOrAddNode(String uri, String prefix, SOAPElement baseNode, String nodeName)
			throws SOAPException {
		QName nodeqname = new QName(uri, nodeName, prefix);
		Iterator<?> it = baseNode.getChildElements(nodeqname);
		if (it.hasNext()) {
			return (SOAPElement) it.next();
		} else {
			return baseNode.addChildElement(nodeqname);
		}
	}

	protected SOAPElement findRequiredNode(String uri, SOAPElement baseNode, String nodeName) throws AS4Exception {
		QName nodeqname = new QName(uri, nodeName);
		Iterator<?> it = baseNode.getChildElements(nodeqname);
		if (!it.hasNext()) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER, "Missing " + nodeName, getMessageId()));
		}
		SOAPElement node = (SOAPElement) it.next();
		if (it.hasNext()) {
			throw new AS4Exception(
					new AS4Error(AS4ErrorCode.INVALID_HEADER, "More than 1 " + nodeName, getMessageId()));
		}
		return node;
	}

	protected SOAPElement findOptionalNode(String uri, SOAPElement baseNode, String nodeName, boolean unique)
			throws AS4Exception {
		QName nodeqname = new QName(uri, nodeName);
		Iterator<?> it = baseNode.getChildElements(nodeqname);
		if (!it.hasNext()) {
			return null;
		}
		SOAPElement node = (SOAPElement) it.next();
		if (unique && it.hasNext()) {
			throw new AS4Exception(
					new AS4Error(AS4ErrorCode.INVALID_HEADER, "More than 1 " + nodeName, getMessageId()));
		}
		return node;
	}

	protected SOAPMessage getMsg() {
		return soapMessage;
	}

	protected String getChildValue(String uri, SOAPElement baseNode, String nodeName) {
		if (baseNode == null)
			return null;
		QName nodeqname = new QName(uri, nodeName);
		Iterator<?> it = baseNode.getChildElements(nodeqname);
		if (!it.hasNext()) {
			return null;
		}
		String value = ((SOAPElement) it.next()).getTextContent().trim();
		if (value.isEmpty())
			return null;
		else
			return value;
	}

	protected void setChildValue(String uri, String prefix, SOAPElement baseNode, String nodeName, String value)
			throws SOAPException {
		SOAPElement node;
		QName nodeqname = new QName(uri, nodeName, prefix);
		Iterator<?> it = baseNode.getChildElements(nodeqname);
		if (it.hasNext()) {
			node = (SOAPElement) it.next();
		} else {
			node = baseNode.addChildElement(nodeqname);
		}
		node.setTextContent(value);
	}

	public String getMessageId() {
		return getChildValue(EB3_NS_URL, msgInfo, EB3_HEADER_MSGID);
	}

	public void setMessageId(String msgId) throws SOAPException {
		setChildValue(EB3_NS_URL, EB3_PREFIX, msgInfo, EB3_HEADER_MSGID, msgId);
	}

	public String getRefMsgId() throws SOAPException {
		return getChildValue(EB3_NS_URL, msgInfo, EB3_RECEIPTMSG_REFMSGID);
	}

	public void setRefMsgId(String id) throws SOAPException {
		setChildValue(EB3_NS_URL, EB3_PREFIX, msgInfo, EB3_RECEIPTMSG_REFMSGID, id);
	}

	public Date getTimeStamp() {
		String timestamp = getChildValue(EB3_NS_URL, msgInfo, EB3_HEADER_TIMESTAMP);
		if (timestamp != null && !timestamp.isEmpty()) {
			if (!timestamp.endsWith("Z"))
				timestamp = timestamp + "Z";
			return Date.from(Instant.parse(timestamp));
		} else
			return null;

	}

	public void setTimeStamp(Date timeStamp) throws SOAPException {
		setChildValue(EB3_NS_URL, EB3_PREFIX, msgInfo, EB3_HEADER_TIMESTAMP, timeStamp.toInstant().toString());
	}


	public SOAPHeaderElement getSecurityHeader(String role) {
		if (role != null && role.isEmpty())
			role = null;
		QName qname = new QName(EB3_WSSE_URL, EB3_HEADER_SECURITY, EB3_WSSE_PREFIX);
		Iterator<SOAPHeaderElement> it = hdr.getChildElements(qname);
		while (it.hasNext()) {
			SOAPHeaderElement h = (SOAPHeaderElement) it.next();
			if ((role == null && h.getActor() == null) || (role != null && role.equals(h.getActor())))
				return h;
		}
		return null;
	}

	public static SOAPMessage cloneMessage(SOAPMessage msg) throws SOAPException, IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream(8192);
		msg.writeTo(out);
		ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
		return MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(msg.getMimeHeaders(), in);

	}
}
