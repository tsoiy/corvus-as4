package hk.hku.cecid.edi.as4;

import org.apache.log4j.BasicConfigurator;

import hk.hku.cecid.edi.as4.handler.MessageSecurityProcessor;
import hk.hku.cecid.piazza.commons.Sys;
import hk.hku.cecid.piazza.commons.module.ModuleException;
import hk.hku.cecid.piazza.commons.module.ModuleGroup;
import hk.hku.cecid.piazza.commons.module.SystemModule;
import hk.hku.cecid.piazza.commons.spa.Plugin;
import hk.hku.cecid.piazza.commons.spa.PluginException;
import hk.hku.cecid.piazza.commons.spa.PluginHandler;



/**
 * @author Leonard
 * AS4 plugin handler
 */
public class AS4Processor implements PluginHandler {

    private static ModuleGroup moduleGroup;
    
    public static SystemModule core;
    
    private static final String COMPONENT_SECURITY_PROCESSOR = "security-processor";
    
    private static final String COMPONENT_INCOMING_MSG_PROCESSOR = "incoming-message-processor";

    private static final String COMPONENT_INCOMING_PAYLOAD_REPOSITORY = "incoming-payload-repository";

    private static final String COMPONENT_OUTGOING_PAYLOAD_REPOSITORY = "outgoing-payload-repository";

    private static final String COMPONENT_ORIGINAL_MESSAGE_REPOSITORY = "original-message-repository";

    
    /**
     * @see hk.hku.cecid.piazza.commons.spa.PluginHandler#processActivation(hk.hku.cecid.piazza.commons.spa.Plugin)
     */
    public void processActivation(Plugin plugin) throws PluginException {
        try {
            init(plugin.getParameters().getProperty("module-group-descriptor"),
                    plugin.getClassLoader());
        }
        catch (Exception e) {
            throw new PluginException("Unable to initialize AS4 processor", e);
        }
    }
    
    /**
     * @see hk.hku.cecid.piazza.commons.spa.PluginHandler#processDeactivation(hk.hku.cecid.piazza.commons.spa.Plugin)
     */
    public void processDeactivation(Plugin plugin) throws PluginException {
        try {
            destroy();
        }
        catch (Exception e) {
            throw new PluginException("Unable to shutdown AS4 processor", e);
        }
    } 
    
    private static void destroy() {
        if (moduleGroup != null) {
            moduleGroup.stopActiveModules();
            Sys.getModuleGroup().removeChild(moduleGroup);
        }
    }
    
    private static void init(String moduleGroupDescriptor, ClassLoader loader) {
        if (moduleGroupDescriptor == null) {
            moduleGroupDescriptor = AS4Processor.class.getPackage().getName()
                .replace('.', '/') + "/conf/as4.module-group.xml";
        }
        if (loader == null) {
            loader = AS4Processor.class.getClassLoader();
        }
        
        moduleGroup = new ModuleGroup(moduleGroupDescriptor, loader);
        Sys.getModuleGroup().addChild(moduleGroup);
        core = getSystemModule();
        Sys.main.log.info("After set up core");
        core.log.info("After set up core");

        //initActivationFramework();

        //recover();
        moduleGroup.startActiveModules();
        
        core.log.info("AS4 server started up successfully");
    }
    
    public static SystemModule getSystemModule() {
        SystemModule module = getModuleGroup().getSystemModule();
        if (module == null) {
            throw new ModuleException("AS4 core system module not found");
        }
        else {
            return module;
        }
    }
    
    public static ModuleGroup getModuleGroup() {
        if (moduleGroup == null) {
            throw new ModuleException("AS4 module group not initialized");
        }
        else {
            return moduleGroup;
        }
    }

    public static MessageSecurityProcessor getSecurityProcessor() {
    	return (MessageSecurityProcessor)core.getComponent(COMPONENT_SECURITY_PROCESSOR);
    }
//    public static IncomingMessageProcessor getIncomingMessageProcessor() {
//        IncomingMessageProcessor p = (IncomingMessageProcessor) getSystemModule().getComponent(COMPONENT_INCOMING_MSG_PROCESSOR);
//        if (p==null) {
//            throw new ModuleException("AS4 incoming message processor not found");
//        }
//        else {
//            return p;
//        }
//    }
    
//    public static MessageRepository getMessageRepository() {
//        MessageRepository p = (MessageRepository) getSystemModule().getComponent(COMPONENT_ORIGINAL_MESSAGE_REPOSITORY);
//        if (p==null) {
//            throw new ModuleException("AS4 message repository not found");
//        }
//        else {
//            return p;
//        }
//    }
}
