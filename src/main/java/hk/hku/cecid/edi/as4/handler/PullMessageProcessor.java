package hk.hku.cecid.edi.as4.handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.soap.SOAPMessage;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.MessageStoreConstant;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.RepositoryDVO;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorCode;
import hk.hku.cecid.edi.as4.pkg.AS4Message;
import hk.hku.cecid.edi.as4.pkg.AS4MessageConstant;
import hk.hku.cecid.edi.as4.pkg.AS4PullMessage;
import hk.hku.cecid.edi.as4.pkg.AS4UserMessage;
import hk.hku.cecid.piazza.commons.dao.DAOException;

public class PullMessageProcessor extends MessageTypeProcessor {

	private static boolean pullAll = false;
	static {
		String pullAllString = AS4Processor.core.properties.getProperty("as4/pull_all_message_by_mpc");
		if (pullAllString != null) {
			pullAll = Boolean.parseBoolean(pullAllString);
		}
	}
	private AS4PullMessage pullmsg;

	public PullMessageProcessor(AS4PullMessage pullmsg) throws DAOException {
		super(false);
		this.pullmsg = pullmsg;

	}

	MessageDVO findMessageForPull(List<PmodeDVO> pmodes, String mpc) {
		List<MessageDVO> messages = new ArrayList<>();

		try {
			if (pullAll) {
				if( mpc == null || mpc.trim().isEmpty() || AS4MessageConstant.EB3_DEFAULT_MPC.equals(mpc))
					messages.addAll(msgDao.findPullMessageByDefaultMpc());
				else
					messages.addAll(msgDao.findPullMessageByMpc(mpc));
			} else {
				for (PmodeDVO p : pmodes) {
					messages.addAll(msgDao.findPullMessageByPmode(p.getPmodeId()));
				}
				if (messages.size() > 0) {
					Collections.sort(messages, (p1, p2) -> p1.getTimeStamp().compareTo(p2.getTimeOutTimeStamp()));
				}
			}
		} catch (DAOException e) {
			AS4Processor.core.log.error("Failed to retrieve message for pulling", e);
			return null;
		}
		if( messages.size() > 0 )
			return messages.get(0);
		else
			return null;
	}

	@Override
	void reset() {
	}

	@Override
	AS4Message process() throws AS4Exception {
		AS4Message reply = null;
		MessageDVO pullmsgDvo = dao.createMessageDVO();
		AS4DvoConvertor.toMessageDvo(pullmsg, pullmsgDvo);
		pullmsgDvo.setMessageBox(MessageStoreConstant.INBOX);
		pullmsgDvo.setStatus(MessageStoreConstant.STATUS_PROCESSED);

		RepositoryDVO detailDvo = dao.createRepositoryDVO();
		AS4DvoConvertor.toRepositoryDvo(pullmsg, detailDvo);
		detailDvo.setMessageBox(MessageStoreConstant.INBOX);

		addInsert(pullmsgDvo, detailDvo);

		List<PmodeDVO> l = PmodeHandler.getInstance().findPullMessagePmode(pullmsg);
		if (l.isEmpty()) {
			pullmsgDvo.setStatus(MessageStoreConstant.STATUS_PROCESSED_ERROR);
			pullmsgDvo.setStatusDescription("No matching pmode for pulling");
			throw new AS4Exception(
					new AS4Error(AS4ErrorCode.PMODE_MISMATCH, "No matching pmode for pulling", pullmsg.getMessageId()));
		}
		MessageDVO pulledMessage = findMessageForPull(l, pullmsgDvo.getMpc());
		if (pulledMessage != null) {
			pmode = PmodeHandler.getInstance().findByPmodeId(pulledMessage.getPmodeId());
			pullmsgDvo.setPmodeId(pulledMessage.getPmodeId());
			pullmsgDvo.setStatusDescription("Pulled: " + pulledMessage.getMessageId());
			RepositoryDVO repodvo = (RepositoryDVO) repoDao.createDVO();
			repodvo.setMessageId(pulledMessage.getMessageId());
			repodvo.setMessageBox(MessageStoreConstant.OUTBOX);
			try {
				repoDao.retrieve(repodvo);
			} catch (DAOException e) {
				AS4Processor.core.log.error("Failed to retrieve message " + pulledMessage.getMessageId()
						+ ") for pull request (" + pullmsg.getMessageId() + ")", e);
				pullmsgDvo.setStatus(MessageStoreConstant.STATUS_PROCESSED_ERROR);
				pullmsgDvo.setStatusDescription("Failed to retrieve message for pulling");
				// treat it as empty mpc
				throw new AS4Exception(new AS4Error(AS4ErrorCode.EMPTY_MPC,
						"Failed to retrieve message from DB for pulling", pullmsg.getMessageId(), pmode));
			}
			pulledMessage.setStatusDescription("Pulled by: " + pullmsgDvo.getMessageId());
			if (pulledMessage.isAckRequired())
				pulledMessage.setStatus(MessageStoreConstant.STATUS_PENDING_RECEIPT);
			else {
				pulledMessage.setStatus(MessageStoreConstant.STATUS_DELIVERED);
			}
			addUpdate(pulledMessage);
			SOAPMessage soap = AS4DvoConvertor.soapFromRepositoryDvo(repodvo);
			AS4UserMessage msg = new AS4UserMessage(soap);
			reply = msg;
		} else {
			pmode = l.get(0);
			pullmsgDvo.setPmodeId(l.get(0).getPmodeId());
			pullmsgDvo.setStatus(MessageStoreConstant.STATUS_PROCESSED_ERROR);
			pullmsgDvo.setStatusDescription("No message available for pulling");
			throw new AS4Exception(new AS4Error(AS4ErrorCode.EMPTY_MPC,
					"There are matching pmodes, but no message available", pullmsg.getMessageId(), l.get(0)));
		}

		return reply;
	}

}
