package hk.hku.cecid.edi.as4.dao;

import hk.hku.cecid.piazza.commons.dao.DVO;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Leonard
 *
 */
public interface MessageDVO extends DVO {

	/**
	 * @return Returns the messageId.
	 */
	public String getMessageId();

	/**
	 * @param messageId
	 *            The messageId to set.
	 */
	public void setMessageId(String messageId);

	/**
	 * @return Returns the ConvId.
	 */
	public String getConvId();

	/**
	 * @param convId
	 *            The convId to set.
	 */
	public void setConvId(String convId);

	/**
	 * @return Returns the messageType.
	 */
	public String getMessageBox();

	/**
	 * @param messageBox
	 *            The messageBox to set.
	 */
	public void setMessageBox(String messageBox);

	/**
	 * @return Returns the AS4 From Party ID .
	 */
	public String getFromPartyId();

	/**
	 * @param fromPartyId
	 *            The AS4 From Party ID to set.
	 */
	public void setFromPartyId(String fromPartyId);

	/**
	 * @return Returns the AS4 From Party role.
	 */
	public String getFromPartyRole();

	/**
	 * @param fromPartyRole
	 *            The AS4 From Party role to set.
	 */
	public void setFromPartyRole(String fromPartyRole);

	/**
	 * @return Returns the AS4 To Party ID.
	 */
	public String getToPartyId();

	/**
	 * @param toPartyId
	 *            The AS4 To to set.
	 */
	public void setToPartyId(String toPartyId);

	/**
	 * @return Returns the AS4 To Party role.
	 */
	public String getToPartyRole();

	/**
	 * @param toPartyRole
	 *            The AS4 To Party role to set.
	 */
	public void setToPartyRole(String toPartyRole);

	public boolean isAckRequired();

	public void setIsAckRequired(boolean isAckRequired);

	/**
	 * @return Is the message acknowledged by receiver
	 */
	public boolean isAcknowledged();

	/**
	 * @param isAcknowledged
	 *            The isAcknowledged to set.
	 */
	public void setIsAcknowledged(boolean isAcknowledged);

	/**
	 * @return Returns the ref to message ID.
	 */
	public String getRefToMessageId();

	/**
	 * @param refToMessageId
	 *            The RefToMessageId to set.
	 */
	public void setRefToMessageId(String refToMessageId);

	/**
	 * @return Returns the timeStamp.
	 */
	public Date getTimeStamp();

	/**
	 * @param timeStamp
	 *            The timeStamp to set.
	 */
	public void setTimeStamp(Date timeStamp);

	/**
	 * @return Returns the timeout timeStamp.
	 */
	public Date getTimeOutTimeStamp();

	/**
	 * @param timeoutTimeStamp
	 *            The timeStamp to set.
	 */
	public void setTimeOutTimeStamp(Date timeoutTimeStamp);

	/**
	 * @return Returns the timeStamp.
	 */
	public Date getModifiedTimeStamp();

	/**
	 * @param timeStamp
	 *            The timeStamp to set.
	 */
	public void setModifiedTimeStamp(Date modifiedTimeStamp);

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status);

	/**
	 * @return Returns the status.
	 */
	public String getStatus();

	/**
	 * @return Get the description of the message status
	 */
	public String getStatusDescription();

	/**
	 * @param desc
	 *            Set the description of the message status
	 */
	public void setStatusDescription(String desc);

	/**
	 * @return Return agreementRef
	 */
	public String getAgreementRef();

	/**
	 * @param agreementRef
	 *            Set agreementRef
	 */
	public void setAgreementRef(String agreementRef);

	/**
	 * @return Return service
	 */
	public String getService();

	/**
	 * @param service
	 *            Set service
	 */
	public void setService(String service);

	/**
	 * @return Return service type
	 */
	public String getServiceType();

	/**
	 * @param serviceType
	 *            Set service type
	 */
	public void setServiceType(String serviceType);

	/**
	 * @return Return action
	 */
	public String getAction();

	/**
	 * @param action
	 *            Set action
	 */
	public void setAction(String action);

	/**
	 * @return
	 */
	String getPmodeId();

	/**
	 * @param pmodeId
	 */
	void setPmodeId(String pmodeId);

	/**
	 * @return
	 */
	public int getRetries();

	/**
	 * @param retries
	 */
	public void setRetries(int retries);
	
	public String getAddress();
	
	public void setAddress(String address);

	public String getMpc();
	
	public void setMpc(String mpc);
	
	public void setMessageType(String type);
	
	public String getMessageType();
	
	public boolean isRead();
	
	public void setIsRead(boolean isRead);
	
}