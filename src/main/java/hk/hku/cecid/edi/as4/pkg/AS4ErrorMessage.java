package hk.hku.cecid.edi.as4.pkg;

import static hk.hku.cecid.edi.as4.pkg.AS4MessageConstant.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import lombok.Getter;

public class AS4ErrorMessage extends AbstractAS4Message implements AS4Message {
	private SOAPElement signalMsgNode;
	@Getter
	private List<AS4Error> errors;

	public AS4ErrorMessage() throws SOAPException {
		super();
		errors = new ArrayList<>();
		signalMsgNode = msgNode.addChildElement(EB3_HEADER_SIGNALMSG, EB3_PREFIX);
		addMessageInfoNode(signalMsgNode);

	}

	public AS4ErrorMessage(SOAPMessage message, SOAPElement signalMsgNode) throws AS4Exception {
		super(message);
		this.signalMsgNode = signalMsgNode;
		errors = new ArrayList<>();
		msgInfo = findRequiredNode(EB3_NS_URL, signalMsgNode, EB3_HEADER_MSGINFO);
		Iterator it = signalMsgNode.getChildElements(new QName(EB3_NS_URL, EB3_ERRORMSG_ERROR, EB3_PREFIX));
		while (it.hasNext()) {
			SOAPElement error = (SOAPElement) it.next();
			String errorCode = AS4DvoConvertor.trimString(error.getAttribute(EB3_ERRORMSG_CODE));
			String desc = AS4DvoConvertor.trimString(error.getAttribute(EB3_ERRORMSG_DESC));
			String origin = AS4DvoConvertor.trimString(error.getAttribute(EB3_ERRORMSG_ORIGIN));
			String category = AS4DvoConvertor.trimString(error.getAttribute(EB3_ERRORMSG_CATEGORY));
			String severity = AS4DvoConvertor.trimString(error.getAttribute(EB3_ERRORMSG_SEVERITY));
			String refId = AS4DvoConvertor.trimString(error.getAttribute(EB3_ERRORMSG_REFMSGID));

			if (errorCode != null) {
				AS4Error e = new AS4Error(AS4ErrorCode.getError(errorCode, desc, origin, category, severity));
				e.setRefId(refId);
				errors.add(e);
			}
		}
	}

	public SOAPElement getSignalNode() {
		return signalMsgNode;
	}

	public void addError(AS4Error error) throws SOAPException {
		AS4ErrorCode code = error.getCode();
		SOAPElement errorNode = signalMsgNode.addChildElement(EB3_ERRORMSG_ERROR, EB3_PREFIX);
		errorNode.setAttribute(EB3_ERRORMSG_CODE, code.getErrorCode());
		errorNode.setAttribute(EB3_ERRORMSG_SEVERITY, code.getSeverity().toString());
		if (error.getRefId() != null && !error.getRefId().isEmpty()) {
			errorNode.setAttribute(EB3_ERRORMSG_REFMSGID, error.getRefId());
			setRefMsgId(error.getRefId());
		}
		errors.add(error);
	}

}
