package hk.hku.cecid.edi.as4.dao;

import java.security.cert.X509Certificate;
import java.util.Date;

import hk.hku.cecid.piazza.commons.dao.DVO;


public interface PmodeDVO extends DVO {

    public String getPmodeId();

    public void setPmodeId(String pmodeId);

    public boolean isDisabled();

    public void setIsDisabled(boolean isDisabled);

    public String getAction();
    
    public void setAction(String action);
    
    public String getAgreement();
    
    public void setAgreement(String agreement);
    
    public String getService();
    
    public void setService(String service);
    
    public String getServiceType();
    
    public void setServiceType(String serviceType);

    public boolean isRetryRequired();

    public void setIsRetryRequired(boolean isRetryRequired);
    
    public boolean isReceiptRequired();

    public void setIsReceiptRequired(boolean isReceiptRequired);

    public boolean isPmodeAuthRequired();

    public void setIsPmodeAuthRequired(boolean isPmodeAuthRequired);
    
    public boolean isTokenRequired();
	
    public void setIsTokenRequired(boolean isTokenRequired);
    
    public boolean isTokenDigestRequired();
	
    public void setIsTokenDigestRequired(boolean isTokenDigestRequired);

    public boolean isTokenCreatedRequired();
	
    public void setIsTokenCreatedRequired(boolean isTokenCreatedRequired);
    
    public boolean isSignRequired();

    public void setIsSignRequired(boolean isSignRequired);

    public boolean isEncryptRequired();

    public void setIsEncryptRequired(boolean isEncryptRequired);
    
    public boolean isReportErrorAsResponse();
	
    public void setIsReportErrorAsResponse(boolean isReportErrorAsResponse);

    public boolean isReportProcessError();
	
    public void setIsReportProcessError(boolean isReportProcessError);

    public boolean isReportDeliveryFailure();
	
    public void setIsReportDeliveryFailure(boolean isReportDeliveryFailture);

    public String getInitPartyId();
    
    public void setInitPartyId(String initPartyId);

    public String getInitPartyRole();
    
    public void setInitPartyRole(String initPartyRole);

    public String getInitUsername();
    
    public void setInitUsername(String initUsername);
    
    public String getInitPassword();
    
    public void setInitPassword(String Password);
    
    public String getRespPartyId();
    
    public void setRespPartyId(String respPartyId);

    public String getRespPartyRole();
    
    public void setRespPartyRole(String respPartyRole);
    
    public String getRespUsername();
    
    public void setRespUsername(String respUsername);
    
    public String getRespPassword();
    
    public void setRespPassword(String respPassword);
    
    public String getReportReceiverErrorTo();
    
    public void setReportReceiverErrorTo(String reportReceiverErrorTo);

    public String getReceiptReplyPattern();
    
    public void setReceiptReplyPattern(String receiptReplyPattern);

    public String getTokenUsername();
    
    public void setTokenUsername(String tokenUsername);
    
    public String getTokenPassword();
    
    public void setTokenPassword(String tokenPassword);
    
    public String getAddress();
    
    public void setAddress(String address);

    public String getPullOrPush();
    
    public void setPullOrPush(String pullPush);
    
    public String getMpc();
    
    public void setMpc(String mpc);
    
    public int getRetries();

    public void setRetries(int retries);

    public int getRetryInterval();

    public void setRetryInterval(int retryInterval);

    public String getSignCert();
    
    public void setSignCert(String signCert);
    
    public String getSignAlgorithm();

    public void setSignAlgorithm(String signAlgorithm);

    public String getSignHashFunc();

    public void setSignHashFunc(String signHashFunc);

    public String getEncryptAlgorithm();

    public void setEncryptAlgorithm(String encryptAlgorithm);

    public String getEncryptCert();
    
    public void setEncryptCert(String encryptCert);

    public Date getModifiedTimeStamp();

    public void setModifiedTimeStamp(Date modifiedTimeStamp);
    
}