package hk.hku.cecid.edi.as4.pkg;

import static hk.hku.cecid.edi.as4.pkg.AS4MessageConstant.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import lombok.Getter;

public class AS4BundledMessage extends AbstractAS4Message implements AS4Message {
	@Getter
	private AS4UserMessage userMessage = null;
	@Getter
	private AS4ErrorMessage errorMessage = null;
	@Getter
	private AS4PullMessage pullMessage = null;
	@Getter
	private AS4ReceiptMessage receiptMessage = null;
	
	private final static QName USERMSGNAME = new QName(EB3_NS_URL, EB3_HEADER_USERMSG);
	private final static QName SIGNALMSGNAME = new QName(EB3_NS_URL, EB3_HEADER_SIGNALMSG);

	public AS4BundledMessage(SOAPMessage message) throws AS4Exception, SOAPException {
		super(message);

	}

	public AS4BundledMessage() throws SOAPException {
	}


	public List<AS4Message> extractMessages() throws AS4Exception {

		List<AS4Message> msgs = new ArrayList<>(4);
		Iterator<?> children = msgNode.getChildElements();
		while (children.hasNext()) {
			Node n = (Node) children.next();
			if (n instanceof SOAPElement) {
				SOAPElement s = (SOAPElement) n;
				QName nodename = s.getElementQName();
				if (nodename.equals(USERMSGNAME)) {
					if (userMessage == null) {
						userMessage = new AS4UserMessage(soapMessage);
						msgs.add(userMessage);
					} else {
						throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER,
								"More than 1 user message in bundled message"));
					}
				} else if (nodename.equals(SIGNALMSGNAME)) {
					if ((findOptionalNode(EB3_NS_URL, s, EB3_ERRORMSG_ERROR, false)) != null) {
						if (errorMessage == null) {
							errorMessage = new AS4ErrorMessage(soapMessage, s);
							msgs.add(errorMessage);
						} else {
							throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER,
									"More than 1 error signal message in bundled message"));
						}
					} else if ((findOptionalNode(EB3_NS_URL, s, EB3_PULLMSG_REQ, true)) != null) {
						if (pullMessage == null) {
							pullMessage = new AS4PullMessage(soapMessage, s);
							msgs.add(pullMessage);
						} else {
							throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER,
									"More than 1 pull signal message in bundled message"));
						}
					} else if ((findOptionalNode(EB3_NS_URL, s, EB3_RECEIPTMSG_RECIEPT, true)) != null) {
						if (receiptMessage == null) {
							receiptMessage = new AS4ReceiptMessage(soapMessage, s);
							msgs.add(receiptMessage);
						} else {
							throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER,
									"More than 1 receipt singal message in bundled message"));
						}
					}
				}
			}
		}
		return msgs;
	}

	public void addMessage(AS4Message message) throws AS4Exception {
		try {

			if (message instanceof AS4UserMessage) {

				if (userMessage != null) {
					throw new AS4Exception(
							new AS4Error(AS4ErrorCode.INVALID_HEADER, "More than 1 user message in bundled message"));
				}
				AbstractAS4Message msg = (AbstractAS4Message) message;
				msgNode.addChildElement(findRequiredNode(EB3_NS_URL, msg.msgNode, EB3_HEADER_USERMSG));
				Iterator<AttachmentPart> attachments = msg.getSOAP().getAttachments();
				while (attachments.hasNext()) {
					soapMessage.addAttachmentPart(attachments.next());
				}
			} else if (message instanceof AS4PullMessage) {
				if (pullMessage != null) {
					throw new AS4Exception(
							new AS4Error(AS4ErrorCode.INVALID_HEADER, "More than 1 user message in bundled message"));
				}
				AbstractAS4Message msg = (AbstractAS4Message) message;
				msgNode.addChildElement(findRequiredNode(EB3_NS_URL, msg.msgNode, EB3_HEADER_SIGNALMSG));
			} else if (message instanceof AS4ReceiptMessage) {
				if (receiptMessage != null) {
					throw new AS4Exception(
							new AS4Error(AS4ErrorCode.INVALID_HEADER, "More than 1 user message in bundled message"));
				}
				AbstractAS4Message msg = (AbstractAS4Message) message;
				msgNode.addChildElement(findRequiredNode(EB3_NS_URL, msg.msgNode, EB3_HEADER_SIGNALMSG));
			} else if (message instanceof AS4ErrorMessage) {
				if (errorMessage != null) {
					throw new AS4Exception(
							new AS4Error(AS4ErrorCode.INVALID_HEADER, "More than 1 error message in bundled message"));
				}
				AbstractAS4Message msg = (AbstractAS4Message) message;
				msgNode.addChildElement(findRequiredNode(EB3_NS_URL, msg.msgNode, EB3_HEADER_SIGNALMSG));
			} else {
				throw new AS4Exception(
						new AS4Error(AS4ErrorCode.INVALID_HEADER, "Unknown message " + message.getClass().getName()));
			}
			soapMessage.saveChanges();

		} catch (SOAPException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.OTHER_ERROR, "Internal error in constructing message"));
		}
	}

	@Override
	public String getMessageId() {
		if( userMessage != null)
			return userMessage.getMessageId();
		else if (receiptMessage != null)
			return receiptMessage.getMessageId();
		else if (errorMessage != null)
			return errorMessage.getMessageId();
		else if (pullMessage != null)
			return pullMessage.getMessageId();
		else return null;
	}

}
