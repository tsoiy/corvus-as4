package hk.hku.cecid.edi.as4.dao;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorCode;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorMessage;
import hk.hku.cecid.edi.as4.pkg.AS4Message;
import hk.hku.cecid.edi.as4.pkg.AS4MessageConstant;
import hk.hku.cecid.edi.as4.pkg.AS4PullMessage;
import hk.hku.cecid.edi.as4.pkg.AS4ReceiptMessage;
import hk.hku.cecid.edi.as4.pkg.AS4UserMessage;

public class AS4DvoConvertor {

	private AS4DvoConvertor() {
	}


	public static String trimString(String input) {
		if (input != null) {
			String ret = input.trim();
			if (ret.isEmpty())
				return null;
			else
				return ret;
		}
		return null;
	}
	
	public static SOAPMessage soapFromRepositoryDvo(RepositoryDVO repoDvo) throws AS4Exception {
		MimeHeaders mimeheaders = new MimeHeaders();
		mimeheaders.addHeader("Content-Type", repoDvo.getContentType());
		InputStream in = new ByteArrayInputStream(repoDvo.getContent());
		SOAPMessage msg = null;
		try {
			msg= MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(mimeheaders, in);
		} catch (IOException | SOAPException e) {
			AS4Processor.core.log.error("Unexpected error in preparing SOAP message for sending out message ("
					+ repoDvo.getMessageId() + ")", e);
			throw new AS4Exception(new AS4Error(AS4ErrorCode.OTHER_ERROR, "Internal processing error"));
		}
		return msg;
	}
	
	public static void toMessageDvo(AS4Message message, MessageDVO msgDvo) throws AS4Exception {
		try {
			if (message instanceof AS4UserMessage) {
				convertAS4UserMessage((AS4UserMessage) message, msgDvo);
			} else if (message instanceof AS4PullMessage) {
				convertAS4PullMessage((AS4PullMessage) message, msgDvo);
			} else if (message instanceof AS4ReceiptMessage) {
				convertAS4ReceiptMessage((AS4ReceiptMessage) message, msgDvo);
			} else if (message instanceof AS4ErrorMessage) {
				convertAS4ErrorMessage((AS4ErrorMessage) message, msgDvo);
			} else {
				throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER));
			}
		} catch (SOAPException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER, e.getMessage()));
		}
	}

	public static void toRepositoryDvo(AS4Message msg, RepositoryDVO repoDvo) {
		// need to call get content first
		repoDvo.setContent(msg.toByteArray());
		repoDvo.setContentType(msg.getContentType());
		repoDvo.setMessageId(msg.getMessageId());
	}

	private static void convertAS4UserMessage(AS4UserMessage msg, MessageDVO msgDvo) throws SOAPException {
		msgDvo.setMessageId(msg.getMessageId());
		msgDvo.setTimeStamp(msg.getTimeStamp());
		msgDvo.setRefToMessageId(msg.getRefMsgId());
		msgDvo.setAction(msg.getAction());
		msgDvo.setServiceType(msg.getServiceType());
		msgDvo.setService(msg.getService());
		msgDvo.setConvId(msg.getConvId());
		msgDvo.setFromPartyId(msg.getFromPartyId());
		msgDvo.setFromPartyRole(msg.getFromPartyRole());
		msgDvo.setToPartyId(msg.getToPartyId());
		msgDvo.setToPartyRole(msg.getToPartyRole());
		msgDvo.setAgreementRef(msg.getAgreeRef());
		if(!msg.getMpc().isEmpty())
			msgDvo.setMpc(msg.getMpc());
		msgDvo.setPmodeId(msg.getPmodeId());
		msgDvo.setMessageType(MessageStoreConstant.MESSAGE_TYPE_USERMSG);
	}

	private static void convertAS4PullMessage(AS4PullMessage msg, MessageDVO msgDvo) throws SOAPException {
		msgDvo.setMessageId(msg.getMessageId());
		msgDvo.setMpc(msg.getMpc());
		msgDvo.setTimeStamp(msg.getTimeStamp());
		msgDvo.setMessageType(MessageStoreConstant.MESSAGE_TYPE_PULL);
	}

	private static void convertAS4ReceiptMessage(AS4ReceiptMessage msg, MessageDVO msgDvo) throws SOAPException {
		msgDvo.setMessageId(msg.getMessageId());
		msgDvo.setRefToMessageId(msg.getRefMsgId());
		msgDvo.setTimeStamp(msg.getTimeStamp());
		msgDvo.setMessageType(MessageStoreConstant.MESSAGE_TYPE_RECEIPT);
	}

	private static void convertAS4ErrorMessage(AS4ErrorMessage msg, MessageDVO msgDvo) throws SOAPException {
		msgDvo.setMessageId(msg.getMessageId());
		msgDvo.setRefToMessageId(msg.getRefMsgId());
		msgDvo.setTimeStamp(msg.getTimeStamp());
		if(!msg.getErrors().isEmpty()) {
			msgDvo.setStatusDescription(msg.getErrors().get(0).getShortDescription());
			if( msg.getRefMsgId() == null ) {
				for(AS4Error e:msg.getErrors()) {
					if (e.getRefId() != null ) {
						msgDvo.setRefToMessageId(e.getRefId());
					}
				}
			}
		}
		msgDvo.setMessageType(MessageStoreConstant.MESSAGE_TYPE_ERROR);
	}

	public static void convertOutboundUserMessage(AS4UserMessage msg, PmodeDVO pmode, MessageDVO dvo,
			RepositoryDVO repoDvo) {
		try {
			convertAS4UserMessage(msg, dvo);
			toRepositoryDvo(msg, repoDvo);
			dvo.setMessageBox(MessageStoreConstant.OUTBOX);
			repoDvo.setMessageBox(MessageStoreConstant.OUTBOX);
			dvo.setAddress(pmode.getAddress());
			dvo.setRetries(pmode.isRetryRequired() ? pmode.getRetries() : 1);
			dvo.setIsAckRequired(pmode.isReceiptRequired());
			dvo.setIsAcknowledged(false);
			if (pmode.getPullOrPush().equals(AS4MessageConstant.EB3_MEP_BINDING_PULL_URL)) {
				dvo.setStatus(MessageStoreConstant.STATUS_TO_BE_PULLED);
				dvo.setRetries(0);
			} else {
				dvo.setStatus(MessageStoreConstant.STATUS_PENDING_DELIVERY);
				dvo.setTimeOutTimeStamp(new Date());
			}

		} catch (SOAPException e) {
			AS4Processor.core.log.warn("Unexpected SOAP error in processing self-constructed message, possbily bugs?",
					e);
		}
	}

	public static void convertOutboundPullMessage(AS4PullMessage msg, PmodeDVO pmode, MessageDVO dvo,
			RepositoryDVO repoDvo) {
		try {
			convertAS4PullMessage(msg, dvo);
			toRepositoryDvo(msg, repoDvo);
			dvo.setMessageBox(MessageStoreConstant.OUTBOX);
			repoDvo.setMessageBox(MessageStoreConstant.OUTBOX);
			dvo.setPmodeId(pmode.getPmodeId());
			dvo.setAddress(pmode.getAddress());
			// Pull message are sent regularily, no need to retry
			dvo.setRetries(1);
			dvo.setIsAckRequired(false);
			dvo.setIsAcknowledged(false);
			dvo.setStatus(MessageStoreConstant.STATUS_PENDING_DELIVERY);
			dvo.setTimeOutTimeStamp(new Date());

		} catch (SOAPException e) {
			AS4Processor.core.log.warn("Unexpected SOAP error in processing self-constructed message, possbily bugs?",
					e);
		}
	}

	public static boolean convertOutboundReceiptMessage(AS4ReceiptMessage msg, PmodeDVO pmode, MessageDVO dvo,
			RepositoryDVO repoDvo, boolean isReply) {
		boolean reply = true;
		try {
			convertAS4ReceiptMessage(msg, dvo);
			toRepositoryDvo(msg, repoDvo);
			dvo.setMessageBox(MessageStoreConstant.OUTBOX);
			repoDvo.setMessageBox(MessageStoreConstant.OUTBOX);
			dvo.setPmodeId(pmode.getPmodeId());
			dvo.setAddress(pmode.getAddress());
			dvo.setIsAckRequired(false);
			dvo.setIsAcknowledged(false);
			if (isReply
					|| pmode.getReceiptReplyPattern().equals(AS4MessageConstant.EB3_RECEIPT_REPLY_PATTERN_CALLBACK)) {
				if (isReply && pmode.getReceiptReplyPattern()
						.equals(AS4MessageConstant.EB3_RECEIPT_REPLY_PATTERN_RESPONSE)) {
					// does not allow response if we are initiator (i.e pulled user message)
					// send it via call back
					AS4Processor.core.log.warn("Error in pmode config (" + pmode.getPmodeId()
							+ "): reponse receipt pattern not allowed for pull MEP");
				}
				dvo.setStatus(MessageStoreConstant.STATUS_PENDING_DELIVERY);
				dvo.setTimeOutTimeStamp(new Date());
				dvo.setRetries(pmode.isRetryRequired() ? pmode.getRetries() : 1);
				reply = false;
			} else {
				dvo.setStatus(MessageStoreConstant.STATUS_DELIVERED);
				dvo.setRetries(0);
			}
		} catch (SOAPException e) {
			AS4Processor.core.log.warn("Unexpected SOAP error in processing self-constructed message, possbily bugs?",
					e);
		}
		return reply;
	}

	public static boolean convertOutboundErrorMessage(AS4ErrorMessage msg, PmodeDVO pmode, MessageDVO dvo,
			RepositoryDVO repoDvo, boolean isReply) {
		boolean reply = true;
		try {
			convertAS4ErrorMessage(msg, dvo);
			toRepositoryDvo(msg, repoDvo);
			dvo.setMessageBox(MessageStoreConstant.OUTBOX);
			repoDvo.setMessageBox(MessageStoreConstant.OUTBOX);
			// for error message, there is a chance that there is no matching pmode
			if( pmode != null ) {
				dvo.setPmodeId(pmode.getPmodeId());
				dvo.setAddress(pmode.getAddress());
			}
			dvo.setIsAckRequired(false);
			dvo.setIsAcknowledged(false);
			dvo.setRefToMessageId(msg.getRefMsgId());
			dvo.setStatusDescription(msg.getErrors().get(0).getShortDescription());
			if (isReply || (pmode != null && !pmode.isReportErrorAsResponse())) {
				dvo.setStatus(MessageStoreConstant.STATUS_PENDING_DELIVERY);
				dvo.setTimeOutTimeStamp(new Date());
				if(pmode != null)
					dvo.setRetries(pmode.isRetryRequired() ? pmode.getRetries() : 1);
				else
					dvo.setRetries(1);
				reply = false;
			} else {
				dvo.setStatus(MessageStoreConstant.STATUS_DELIVERED);
				dvo.setRetries(0);
			}
		} catch (SOAPException e) {
			AS4Processor.core.log.warn("Unexpected SOAP error in processing self-constructed message, possbily bugs?",
					e);
		}
		return reply;
	}
}
