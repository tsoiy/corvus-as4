package hk.hku.cecid.edi.as4.handler;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.wss4j.common.ext.WSPasswordCallback;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * A Callback Handler implementation for the case of finding a password to access a
 * cert/private key in a keystore.
 */
public class PasswordCallbackHandler implements CallbackHandler {

	Map<String, String>passwords;
	
	public PasswordCallbackHandler() {
		passwords = new HashMap<String, String>();
	}
	
	public PasswordCallbackHandler(String username, String password) {
		this();
		addPassword(username, password);
	}
	
	public void addPassword(String username, String password) {
		passwords.put(username, password);
	}
	
    public void handle(Callback[] callbacks)
        throws IOException, UnsupportedCallbackException {
        for (int i = 0; i < callbacks.length; i++) {
            if (callbacks[i] instanceof WSPasswordCallback) {
                WSPasswordCallback pc = (WSPasswordCallback) callbacks[i];
            	pc.setPassword(passwords.get(pc.getIdentifier()));
            } else {
                throw new UnsupportedCallbackException(callbacks[i], "Unrecognized Callback");
            }
        }
    }
}