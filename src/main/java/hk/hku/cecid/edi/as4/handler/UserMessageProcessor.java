package hk.hku.cecid.edi.as4.handler;

import javax.xml.soap.SOAPException;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.MessageStoreConstant;
import hk.hku.cecid.edi.as4.dao.PmodeDAO;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.RepositoryDVO;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorCode;
import hk.hku.cecid.edi.as4.pkg.AS4Message;
import hk.hku.cecid.edi.as4.pkg.AS4ReceiptMessage;
import hk.hku.cecid.edi.as4.pkg.AS4UserMessage;
import hk.hku.cecid.piazza.commons.dao.DAOException;

public class UserMessageProcessor extends MessageTypeProcessor {

	private AS4UserMessage usermsg;
	private MessageDVO originalMessage;

	public UserMessageProcessor(AS4UserMessage usermsg) throws DAOException {
		super(false);
		this.usermsg = usermsg;
		this.pmode = null;
		this.originalMessage = null;
	}

	public UserMessageProcessor(AS4UserMessage usermsg, MessageDVO originalMessage, PmodeDVO pmode)
			throws DAOException {
		super(true);
		this.usermsg = usermsg;
		this.pmode = pmode;
		this.originalMessage = originalMessage;
	}

	AS4ReceiptMessage generateReceipt(AS4UserMessage msg, PmodeDVO pmode) throws AS4Exception {
		AS4ReceiptMessage receipt = null;
		try {
			receipt = new AS4ReceiptMessage();
			receipt.setRefMsgId(msg.getMessageId());
			receipt.addReceiptDetail(msg, pmode.isSignRequired());
		} catch (SOAPException e) {
			AS4Processor.core.log.error("Error in generating receipt for user message " + msg.getMessageId(), e);
			throw new AS4Exception(new AS4Error(AS4ErrorCode.OTHER_ERROR));
		} catch (AS4Exception e) {
			AS4Processor.core.log.error("Error in generating receipt for user message " + msg.getMessageId(), e);
			throw e;
		}
		return receipt;
	}

	@Override
	void reset() {
		// in case of reset, this is the situation of original message already updated,
		// should be a pull message(?)
		// we cannot discard the pulled message, just go ahead and skip the update of
		// the original message
		try {
			if( originalMessage == null ) {
				AS4Processor.core.log.error("Unexpected retry due to concurrent update, but the original update message does not exist");
				return;
			}
			if (dao.retrieve(originalMessage)) {
				switch (originalMessage.getStatus()) {
				case MessageStoreConstant.STATUS_DELIVERED:
				case MessageStoreConstant.STATUS_DELIVERY_FAILURE:
					AS4Processor.core.log.debug("Orignal message (" + originalMessage.getMessageId()
							+ ") already updated, skip updating, the received user message will be stored anyway");
					originalMessage = null;
					break;
				case MessageStoreConstant.STATUS_PENDING_DELIVERY:
					AS4Processor.core.log.debug("Original message (" + originalMessage.getMessageId()
							+ ") still pending for delivery, but got updated somehow, retry");
					break;
				default:
					AS4Processor.core.log.debug("Original message (" + originalMessage.getMessageId()
							+ ") has unexpected status of (" + originalMessage.getStatus() + "), retry anyway");
					break;
				}
			} else {
				AS4Processor.core.log.error("Original message (" + originalMessage.getMessageId() + ") not found, skip updating");
				originalMessage = null;
			}
		} catch (DAOException e) {
			AS4Processor.core.log.error(
					"Cannot retrieve original message (" + originalMessage.getMessageId() + "), skip updating");
			originalMessage = null;
		}
	}

	@Override
	AS4Message process() throws AS4Exception {

		AS4Message reply = null;
		PmodeDVO msgPmode = null;
		MessageDVO usermsgDvo = dao.createMessageDVO();
		AS4DvoConvertor.toMessageDvo(usermsg, usermsgDvo);
		usermsgDvo.setMessageBox(MessageStoreConstant.INBOX);
		usermsgDvo.setStatus(MessageStoreConstant.STATUS_PROCESSED);

		RepositoryDVO usermsgDetailDvo = dao.createRepositoryDVO();
		AS4DvoConvertor.toRepositoryDvo(usermsg, usermsgDetailDvo);
		usermsgDetailDvo.setMessageBox(MessageStoreConstant.INBOX);

		addInsert(usermsgDvo, usermsgDetailDvo);

		if (isReply && originalMessage != null) {
			originalMessage.setStatus(MessageStoreConstant.STATUS_DELIVERED);
			originalMessage.setStatusDescription("Received user message (" + usermsgDvo.getMessageId() + ")");
			addUpdate(originalMessage);
		}

		// find pmode
		msgPmode = PmodeHandler.getInstance().findUserMessagePmode(usermsg);
		if (msgPmode == null) {
			usermsgDvo.setStatus(MessageStoreConstant.STATUS_PROCESSED_ERROR);
			usermsgDvo.setStatusDescription("Cannot find matching pmode");
			throw (new AS4Exception(new AS4Error(AS4ErrorCode.PMODE_MISMATCH, "Cannot find matching pmode.",
					usermsgDvo.getMessageId())));
		}
		if (isReply && !pmode.getPmodeId().equals(msgPmode.getPmodeId())) {
			AS4Processor.core.log.warn("The pulled user message is having different pmode (" + msgPmode.getPmodeId()
					+ ") from the pull signal message pmode (" + pmode.getPmodeId() + ")");
		}
		pmode = msgPmode;
		usermsgDvo.setPmodeId(pmode.getPmodeId());
		usermsgDvo.setIsAckRequired(pmode.isReceiptRequired());

		AS4Processor.getSecurityProcessor().authEbmsUser(usermsg, pmode, !isReply);
		// generate receipt
		if (pmode.isReceiptRequired()) {
			reply = generateReceipt(usermsg, pmode);
			usermsgDvo.setStatusDescription("Receipt message ID:" + reply.getMessageId());
			MessageDVO replyDvo = dao.createMessageDVO();
			RepositoryDVO replyDetailsDvo = dao.createRepositoryDVO();
			if (!AS4DvoConvertor.convertOutboundReceiptMessage((AS4ReceiptMessage) reply, pmode, replyDvo,
					replyDetailsDvo, isReply))
				reply = null;
			addInsert(replyDvo, replyDetailsDvo);
		}
		return reply;
	}
}
