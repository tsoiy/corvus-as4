package hk.hku.cecid.edi.as4.handler;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.dao.MessageDAO;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.MessageStoreConstant;
import hk.hku.cecid.edi.as4.dao.MessageStoreDAO;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.RepositoryDVO;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4BundledMessage;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorCode;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorMessage;
import hk.hku.cecid.edi.as4.pkg.AS4Message;
import hk.hku.cecid.edi.as4.pkg.AS4PullMessage;
import hk.hku.cecid.edi.as4.pkg.AS4ReceiptMessage;
import hk.hku.cecid.edi.as4.pkg.AS4UserMessage;
import hk.hku.cecid.piazza.commons.dao.DAOException;

/**
 *
 * 
 * 
 */
public class InboundMessageProcessor {

	private static class HelperHolder {
		public static final InboundMessageProcessor singleton;
		static {
			singleton = new InboundMessageProcessor();
		}
	}

	public static InboundMessageProcessor getInstance() {
		return HelperHolder.singleton;
	}

	private InboundMessageProcessor() {
	}

	boolean storeErrorMessage(MessageDVO originalMsg, List<AS4Message> incomingMessages, AS4ErrorMessage errorMessage,
			boolean isReply) {
		try {

			MessageStoreDAO storeDao = (MessageStoreDAO) AS4Processor.core.dao.createDAO(MessageStoreDAO.class);
			List<MessageDVO> insertMsgs = new ArrayList<>(incomingMessages.size() + 1);
			List<RepositoryDVO> insertDetails = new ArrayList<>(incomingMessages.size() + 1);
			MessageDVO dvo;
			RepositoryDVO repoDvo;
			String pmodeId = null;
			dvo = storeDao.createMessageDVO();
			repoDvo = storeDao.createRepositoryDVO();

			AS4Error error = errorMessage.getErrors().get(0);
			if (error.getPmode() != null) {
				pmodeId = error.getPmode().getPmodeId();
			}
			AS4DvoConvertor.convertOutboundErrorMessage(errorMessage, error.getPmode(), dvo, repoDvo, isReply);
			insertMsgs.add(dvo);
			insertDetails.add(repoDvo);

			for (AS4Message m : incomingMessages) {
				try {
					dvo = storeDao.createMessageDVO();
					repoDvo = storeDao.createRepositoryDVO();
					AS4DvoConvertor.toMessageDvo(m, dvo);
					AS4DvoConvertor.toRepositoryDvo(m, repoDvo);
					dvo.setMessageBox(MessageStoreConstant.INBOX);
					dvo.setStatus(MessageStoreConstant.STATUS_PROCESSED_ERROR);
					dvo.setStatusDescription(error.getShortDescription());
					dvo.setPmodeId(pmodeId);
					repoDvo.setMessageBox(MessageStoreConstant.INBOX);
					insertMsgs.add(dvo);
					insertDetails.add(repoDvo);
				} catch (AS4Exception e) {
					AS4Processor.core.log.info("Failed to store incoming message to database\n" + m.toString());
				}
			}
			storeDao.updateAndInsert(originalMsg == null ? null : Arrays.asList(originalMsg), insertMsgs,
					insertDetails);
			return true;
		} catch (DAOException e) {
			AS4Processor.core.log.error("Failed to store error reply to database (" + errorMessage.getMessageId() + ")",
					e);
		}
		return false;
	}

	void unpackAndValidateReply(SOAPMessage message, List<AS4Message> msgs, PmodeDVO pmode) throws AS4Exception {
		msgs.clear();
		AS4BundledMessage request = null;
		try {
			AS4Processor.core.log.debug("Converting incoming reply to bundled message");
			request = new AS4BundledMessage(message);
		} catch (SOAPException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER));
		}
		// must first extract message to retrieve individual messages
		msgs.addAll(request.extractMessages());
		AS4Processor.core.log.debug("Reply contains " + msgs.size() + " messages");
		if (msgs.isEmpty()) {
			AS4Processor.core.log.info("Empty message received");
			return;
		}
		// retrieve different message types for security checking
		SOAPHeaderElement secuHeader = request.getSecurityHeader(null);

		// if exists try to locate a pmode and perform secuirty check
		if (secuHeader != null) {
			AS4Processor.core.log
					.debug("Reply contains security header, using request pmode to verify: " + pmode.getPmodeId());
			try {
				AS4Processor.getSecurityProcessor().verifyMessage(request, pmode, true);
			} catch (AS4Exception e) {
				e.getError().setPmode(pmode);
				throw e;
			}
		}
		return;
	}

	PmodeDVO unpackAndValidateRequest(SOAPMessage message, List<AS4Message> msgs) throws AS4Exception {
		PmodeDVO pmode = null;
		msgs.clear();
		AS4BundledMessage request = null;
		try {
			AS4Processor.core.log.debug("Converting incoming message to bundled message");
			request = new AS4BundledMessage(message);
		} catch (SOAPException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.INVALID_HEADER));
		}
		// must first extract message to retrieve individual messages
		msgs.addAll(request.extractMessages());
		AS4Processor.core.log.debug("Request contains " + msgs.size() + " messages");
		if (msgs.isEmpty()) {
			AS4Processor.core.log.info("Empty message received");
			return null;
		}
		// retrieve different message types for security checking
		SOAPHeaderElement secuHeader = request.getSecurityHeader(null);

		// if exists try to locate a pmode and perform secuirty check
		if (secuHeader != null) {
			pmode = getMessagePmode(request);
			if (pmode != null) {
				try {
					AS4Processor.getSecurityProcessor().verifyMessage(request, pmode, false);
				} catch (AS4Exception e) {
					e.getError().setPmode(pmode);
					throw e;
				}
			} else {
				throw new AS4Exception(new AS4Error(AS4ErrorCode.PMODE_MISMATCH));
			}
		}
		return pmode;

	}

	/**
	 * <li>if there is a user message, use its pmode for security checking
	 * <li>if no user message, then use receipt signal message's reference ID
	 * original user message for identifying the pmode
	 * <li>then use error signal message's reference ID original user message if
	 * present, this is not too reliable as well, as a single error message can have
	 * reference to more than 1 message and thus potentially more than 1 pmode
	 * <li>lastly use pull signal message, but this can be unreliable as pull signal
	 * message use MPC to identify the pmode and a MPC can be used by multiple
	 * pmodes
	 * 
	 * @param msgs
	 * @return
	 */
	private PmodeDVO getMessagePmode(AS4BundledMessage message) throws AS4Exception {
		PmodeDVO pmode = null;
		String msgId = null;

		try {
			if (message.getUserMessage() != null) {
				AS4UserMessage usermsg = message.getUserMessage();
				msgId = usermsg.getMessageId();
				pmode = PmodeHandler.getInstance().findUserMessagePmode(usermsg);
			} else if (message.getReceiptMessage() != null) {
				AS4ReceiptMessage receiptMsg = message.getReceiptMessage();
				msgId = receiptMsg.getMessageId();
				pmode = PmodeHandler.getInstance().findSignalMessagePmode(receiptMsg.getRefMsgId());
			} else if (message.getErrorMessage() != null) {
				AS4ErrorMessage errorMsg = message.getErrorMessage();
				msgId = errorMsg.getMessageId();
				pmode = PmodeHandler.getInstance().findSignalMessagePmode(errorMsg.getRefMsgId());
			} else if (message.getPullMessage() != null) {
				AS4PullMessage pullMsg = message.getPullMessage();
				msgId = pullMsg.getMessageId();
				List<PmodeDVO>pmodes = PmodeHandler.getInstance().findPullMessagePmode(pullMsg);
				if( pmodes != null && !pmodes.isEmpty())
					pmode = pmodes.get(0);
			}
		} catch (SOAPException e) {
			AS4Processor.core.log.error("Malformed SOAP message", e);
			AS4Error error = new AS4Error(AS4ErrorCode.INVALID_HEADER, "Malformed SOAP message", msgId);
			throw new AS4Exception(error);
		}
		return pmode;

	}

	SOAPMessage constructReply(List<AS4Message>response, PmodeDVO pmode) {
		AS4Message reply;
		AS4Processor.core.log.debug("Replying with " + response.size() + " messages");
		if (response.size() == 0)
			reply = null;
		else if (response.size() == 1)
			reply = response.get(0);
		else {
			AS4BundledMessage bundledReply = null;
			try {
				bundledReply = new AS4BundledMessage();
				for (AS4Message r : response) {
					bundledReply.addMessage(r);
				}
			} catch (SOAPException | AS4Exception e) {
				AS4Processor.core.log.error("Failed to construct reply", e);
				bundledReply = null;
			}
			reply = bundledReply;
		}
		if (reply != null && pmode != null) {
			if (!AS4Processor.getSecurityProcessor().addSecurityProcess(reply, false, pmode))
				reply = null;
		}

		return reply == null ? null : reply.getSOAP();
	}
	
	public SOAPMessage processRequest(SOAPMessage soapMessage) {
		List<AS4Message> response = new ArrayList<>();
		MessageTypeProcessor processor = null;
		PmodeDVO pmode = null;

		AS4Processor.core.log.debug("Start to process request");
		List<AS4Message> msgs = new ArrayList<>(4);
		try {
			pmode = unpackAndValidateRequest(soapMessage, msgs);
		} catch (AS4Exception e) {
			try {
				AS4ErrorMessage errReply = new AS4ErrorMessage();
				errReply.addError(e.getError());
				if (storeErrorMessage(null, msgs, errReply, false))
					response.add(errReply);
			} catch (SOAPException e1) {
				AS4Processor.core.log.error("Failed to construct error reply messaage", e1);
			}
			return constructReply(response, pmode);
		}
		// if there are messages to process
		if (!msgs.isEmpty()) {
			for (AS4Message msg : msgs) {
				try {
					processor = null;
					if (msg instanceof AS4UserMessage) {
						processor = new UserMessageProcessor((AS4UserMessage) msg);
						AS4Processor.core.log.debug("Begin to process user message (" + msg.getMessageId() + ")");
					} else if (msg instanceof AS4PullMessage) {
						processor = new PullMessageProcessor((AS4PullMessage) msg);
						AS4Processor.core.log.debug("Begin to process pull message (" + msg.getMessageId() + ")");
					} else if (msg instanceof AS4ReceiptMessage) {
						processor = new ReceiptMessageProcessor((AS4ReceiptMessage) msg);
						AS4Processor.core.log.debug("Begin to process receipt message (" + msg.getMessageId() + ")");
					} else if (msg instanceof AS4ErrorMessage) {
						processor = new ErrorMessageProcessor((AS4ErrorMessage) msg);
						AS4Processor.core.log.debug("Begin to process error message (" + msg.getMessageId() + ")");
					} else {
						AS4Processor.core.log.warn("Unknown message type " + msg.getClass().getName());
					}
					if (processor != null) {
						AS4Message r = processor.handle();
						AS4Processor.core.log
								.debug("Processed completed (" + msg.getMessageId() + ") with" +( r == null ? "out reply"
										: " reply"));
						if (r != null) {
							response.add(r);
							if (pmode == null)
								pmode = processor.getPmode();
						}
					}
				} catch (DAOException e) {
					AS4Processor.core.log.error("Failed to persist messages to database, dropping request messaage ID ("
							+ msg.getMessageId() + ")", e);
					continue;
				}
			}
		}

		return constructReply(response, pmode);
	}

	public void processReply(SOAPMessage reply, MessageDVO originalMsg, PmodeDVO pmode) {
		MessageTypeProcessor processor = null;
		List<AS4Message> msgs = new ArrayList<>(4);

		AS4Processor.core.log.debug("Start to process reply");
		try {
			// for the reply, the same pmode should be used as the request
			unpackAndValidateReply(reply, msgs, pmode);
		} catch (AS4Exception e) {
			try {
				AS4ErrorMessage errReply = new AS4ErrorMessage();
				errReply.addError(e.getError());
				storeErrorMessage(originalMsg, msgs, errReply, true);
			} catch (SOAPException e1) {
				AS4Processor.core.log.error("Failed to construct error reply message", e1);
			}
			return;
		}
		boolean allFail = true;
		for (AS4Message msg : msgs) {
			processor = null;
			try {

				String logmsg = "(" + msg.getMessageId() + ") as reply for ("
						+ originalMsg.getMessageId() + ") of pmode (" + pmode.getPmodeId() + ")";
				if (msg instanceof AS4UserMessage) {
					processor = new UserMessageProcessor((AS4UserMessage) msg, originalMsg, pmode);
					AS4Processor.core.log
							.debug("Begin to process user message " + logmsg);
				} else if (msg instanceof AS4PullMessage) {
					AS4Processor.core.log.warn("Unexpected pull message in reply, ignoring the pull request");
					continue;
				} else if (msg instanceof AS4ReceiptMessage) {
					processor = new ReceiptMessageProcessor((AS4ReceiptMessage) msg, originalMsg, pmode);
					AS4Processor.core.log
							.debug("Begin to process receipt message " + logmsg);
				} else if (msg instanceof AS4ErrorMessage) {
					processor = new ErrorMessageProcessor((AS4ErrorMessage) msg, originalMsg, pmode);
					AS4Processor.core.log
							.debug("Begin to process error message " + logmsg);
				} else {
					AS4Processor.core.log.warn("Unknown message (" + msg.getMessageId() + ") of message type ("
							+ msg.getClass().getName() + ")");
				}
				if (processor != null)
					processor.handle();
				allFail = false;
			} catch (DAOException e) {
				AS4Processor.core.log.error(
						"Failed to store messages to database, dropping reply message ID (" + msg.getMessageId() + ")", e);
				continue;
			}
		}
		// in case of all fail, there is chance due to fail to insert message, still try to update original 
		// message status to avoid excessive retry
		if( allFail ) {
			try {
				MessageDAO dao = (MessageDAO) AS4Processor.core.dao.createDAO(MessageDAO.class);
				if (originalMsg.getRetries() > 0) {
					originalMsg.setStatus(MessageStoreConstant.STATUS_PENDING_DELIVERY);
					Date timeout = Date.from(Instant.now().plusSeconds(pmode.getRetryInterval()));
					originalMsg.setStatusDescription(null);
					originalMsg.setTimeOutTimeStamp(timeout);
				} else {
					// Assume failure
					originalMsg.setStatus(MessageStoreConstant.STATUS_DELIVERY_FAILURE);
					originalMsg.setStatusDescription("Failed to deliver message");
				}				
				dao.persist(originalMsg);
			} catch (DAOException e) {
				AS4Processor.core.log.error(
						"Failed to update original message status (" + originalMsg.getMessageId() + ")", e);

			}
		}
	}
}
