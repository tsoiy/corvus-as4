package hk.hku.cecid.edi.as4.pkg;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AS4ErrorCode {

	private String errorCode;
	private String description;
	private String origin;
	private String severity;
	private String category;

	private final static String ORIGIN_EBMS = "ebMS";
	private final static String ORIGIN_SECURITY = "security";
	private final static String ORIGIN_RELIABILITY = "reliability";

	private final static String SEVERITY_FAILURE = "failure";
	private final static String SEVERITY_WARNING = "warning";

	private final static String CATEGORY_CONTENT = "Content";
	private final static String CATEGORY_COMMUNICATION = "Communication";
	private final static String CATEGORY_UNPACKAGING = "Unpackaging";
	private final static String CATEGORY_PROCESSING = "Processing";

	public final static AS4ErrorCode VALUE_NOT_RECOGNIZED = new AS4ErrorCode("EBMS:0001", "Value not recognized",
			ORIGIN_EBMS, SEVERITY_FAILURE, CATEGORY_CONTENT);
	public final static AS4ErrorCode FEATURE_NOT_SUPPORTED_CONTENT = new AS4ErrorCode("EBMS:0002",
			"Feature not supported by MSH", ORIGIN_EBMS, SEVERITY_WARNING, CATEGORY_CONTENT);
	public final static AS4ErrorCode VALUE_INCONSISTENT = new AS4ErrorCode("EBMS:0003",
			"Value inconsistent within the message", ORIGIN_EBMS, SEVERITY_FAILURE, CATEGORY_CONTENT);
	public final static AS4ErrorCode OTHER_ERROR = new AS4ErrorCode("EBMS:0004", "Other error", ORIGIN_EBMS,
			SEVERITY_FAILURE, CATEGORY_CONTENT);
	public final static AS4ErrorCode CONNECTION_FAILURE = new AS4ErrorCode("EBMS:0005", "Connection failure",
			ORIGIN_EBMS, SEVERITY_FAILURE, CATEGORY_COMMUNICATION);

	public final static AS4ErrorCode EMPTY_MPC = new AS4ErrorCode("EBMS:0006", "Empty message partition channel",
			ORIGIN_EBMS, SEVERITY_WARNING, CATEGORY_COMMUNICATION);
	public final static AS4ErrorCode MIME_INCONSISTENCY = new AS4ErrorCode("EBMS:0007", "Inconsistent MIME headers",
			ORIGIN_EBMS, SEVERITY_FAILURE, CATEGORY_UNPACKAGING);
	public final static AS4ErrorCode FEATURE_NOT_SUPPORTED_PACKAGE = new AS4ErrorCode("EBMS:0008",
			"Feature not supported", ORIGIN_EBMS, SEVERITY_FAILURE, CATEGORY_UNPACKAGING);
	public final static AS4ErrorCode INVALID_HEADER = new AS4ErrorCode("EBMS:0009", "Invalid AS4 header", ORIGIN_EBMS,
			SEVERITY_FAILURE, CATEGORY_UNPACKAGING);
	public final static AS4ErrorCode PMODE_MISMATCH = new AS4ErrorCode("EBMS:0010", "Processing mode mismatch",
			ORIGIN_EBMS, SEVERITY_FAILURE, CATEGORY_PROCESSING);
	public final static AS4ErrorCode PAYLOAD_ERROR = new AS4ErrorCode("EBMS:0011", "External payload error",
			ORIGIN_EBMS, SEVERITY_FAILURE, CATEGORY_CONTENT);

	public final static AS4ErrorCode FAILED_AUTH = new AS4ErrorCode("EBMS:0101", "Failed authentication",
			ORIGIN_SECURITY, SEVERITY_FAILURE, CATEGORY_PROCESSING);
	public final static AS4ErrorCode FAILED_DECRYPT = new AS4ErrorCode("EBMS:0102", "Failed decryption",
			ORIGIN_SECURITY, SEVERITY_FAILURE, CATEGORY_PROCESSING);
	public final static AS4ErrorCode POLICY_NONCOMP = new AS4ErrorCode("EBMS:0103", "Security policy non-compliance",
			ORIGIN_SECURITY, SEVERITY_FAILURE, CATEGORY_PROCESSING);

	public final static AS4ErrorCode DYSFUNCTIONAL_RELIABILITY = new AS4ErrorCode("EBMS:0201",
			"Dysfuncational reliability", ORIGIN_RELIABILITY, SEVERITY_FAILURE, CATEGORY_PROCESSING);

	public final static AS4ErrorCode MISSING_RECEIPT = new AS4ErrorCode("EBMS:0301", "Missing receipt",
			ORIGIN_RELIABILITY, SEVERITY_FAILURE, CATEGORY_COMMUNICATION);
	public final static AS4ErrorCode INVALID_RECEIPT = new AS4ErrorCode("EBMS:0302", "Invalid receipt",
			ORIGIN_RELIABILITY, SEVERITY_FAILURE, CATEGORY_COMMUNICATION);
	public final static AS4ErrorCode DECOMPRESSION_ERROR = new AS4ErrorCode("EBMS:0303", "Decompression error",
			ORIGIN_RELIABILITY, SEVERITY_FAILURE, CATEGORY_COMMUNICATION);

	public final static AS4ErrorCode DEFAULT_ERROR = OTHER_ERROR;

	private final static List<AS4ErrorCode> errors = Arrays.asList(VALUE_NOT_RECOGNIZED, FEATURE_NOT_SUPPORTED_CONTENT,
			VALUE_INCONSISTENT, OTHER_ERROR, CONNECTION_FAILURE, EMPTY_MPC, MIME_INCONSISTENCY,
			FEATURE_NOT_SUPPORTED_PACKAGE, INVALID_HEADER, PMODE_MISMATCH, PAYLOAD_ERROR, FAILED_AUTH, FAILED_DECRYPT,
			POLICY_NONCOMP, DYSFUNCTIONAL_RELIABILITY, MISSING_RECEIPT, INVALID_RECEIPT, DECOMPRESSION_ERROR);

	public static AS4ErrorCode getError(String code, String description, String origin, String severity,
			String category) {
		Optional<AS4ErrorCode> e = errors.stream().filter(s -> s.errorCode.equals(code)).findFirst();
		if (e.isPresent())
			return e.get();
		else {
			return new AS4ErrorCode(code, description, origin, severity, category);
		}

	}

}
