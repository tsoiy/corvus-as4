package hk.hku.cecid.edi.as4.handler;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.soap.AttachmentPart;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.dao.MessageDAO;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.MessageStoreConstant;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.RepositoryDAO;
import hk.hku.cecid.edi.as4.dao.RepositoryDVO;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.model.Attachment;
import hk.hku.cecid.edi.as4.model.MshMessage;
import hk.hku.cecid.piazza.commons.dao.DAOException;

/**

 */
public class MessageServiceHandler {

	private final static int MAX_MESSAGES = 10000;
	private MessageServiceHandler() {
	}

	private static class HelperHolder {
		public static final MessageServiceHandler singleton = new MessageServiceHandler();
	}

	public static MessageServiceHandler getInstance() {
		return HelperHolder.singleton;
	}

	public List<PmodeDVO> getPmodes() throws DAOException {
		return PmodeHandler.getInstance().getAllPmode();
	}

	public PmodeDVO createPmodeDVO() {
		return PmodeHandler.getInstance().createDVO();
	}
	
	public boolean addPmode(PmodeDVO pmode) throws DAOException {
		return PmodeHandler.getInstance().addPmode(pmode);
		
	}
	
	public boolean removePmode(String pmodeId) throws DAOException {
		return PmodeHandler.getInstance().deletePmode(pmodeId);
	}
	
	public PmodeDVO getPmode(String pmodeId) throws DAOException {
		return PmodeHandler.getInstance().findByPmodeId(pmodeId);
	}
	
	public SOAPMessage processInboundMessage(SOAPMessage request) {
		InboundMessageProcessor inboundMessageProcessor = InboundMessageProcessor.getInstance();
		return inboundMessageProcessor.processRequest(request);
	}

	public void processOutboundMessage(MshMessage request, MshMessage response) {
		OutboundMessageProcessor outboundMessageProcessor = OutboundMessageProcessor.getInstance();
		outboundMessageProcessor.processOutgoingRequest(request, response);

	}

	public List<MessageDVO> getMessages(String pmode, boolean includeRead) throws DAOException {
		MessageDAO dao = (MessageDAO) AS4Processor.core.dao.createDAO(MessageDAO.class);
		dao = (MessageDAO) AS4Processor.core.dao.createDAO(MessageDAO.class);
		return dao.findReceivedMessagesByPmode(pmode, includeRead);
	}
	
	public boolean setMessageReadStatus(List<String>ids, boolean isRead) throws DAOException{
		MessageDAO dao = (MessageDAO) AS4Processor.core.dao.createDAO(MessageDAO.class);
		if( dao.updateMessageReadStatus(ids, isRead) > 0 ) 
			return true;
		else
			return false;
	}
	
	public List<MessageDVO> searchMessages(Map<String, String>conditions) throws DAOException {

		MessageDAO dao = (MessageDAO) AS4Processor.core.dao.createDAO(MessageDAO.class);
		MessageDVO dvo = (MessageDVO) dao.createDVO();
		
		String messageId = conditions.get("message_id");
		if (messageId == null) {
			messageId = "%";
		}
		String messageBox = conditions.get("message_box");
		if (messageBox == null) {
			messageBox = "%";
		}
		String action = conditions.get("action");
		if (action == null) {
			action = "%";
		}
		String service = conditions.get("service");
		if (service == null) {
			service = "%";
		}
		String agreement = conditions.get("agreement");
		if (agreement == null) {
			agreement = "%";
		}
		String fromPartyId = conditions.get("from_party");
		if (fromPartyId == null) {
			fromPartyId = "%";
		}
		String toPartyId = conditions.get("to_party");
		if (toPartyId == null) {
			toPartyId = "%";
		}
		String status = conditions.get("status");
		if (status == null) {
			status = "%";
		}
		String limitString = conditions.get("limit");
		int limit = MAX_MESSAGES;
		if (limitString != null) {
			try {
				limit = Integer.parseInt(limitString);
				if (limit < 0) {
					limit = MAX_MESSAGES;
				}
			} catch (NumberFormatException e) {
			}
		}

		dvo.setMessageId(messageId);
		dvo.setMessageBox(messageBox);
		dvo.setFromPartyId(fromPartyId);
		dvo.setToPartyId(toPartyId);
		dvo.setAction(action);
		dvo.setService(service);
		dvo.setAgreementRef(agreement);
		dvo.setStatus(status);
		return dao.findMessagesByHistory(dvo, limit);
		
	}
	
	public MessageDVO getMessage(String messageId, String mailbox, List<Attachment> attachments) throws DAOException {
		MessageDVO details = null;
		try {

			MessageDAO dao = (MessageDAO) AS4Processor.core.dao.createDAO(MessageDAO.class);
			details = (MessageDVO) dao.createDVO();
			details.setMessageBox(mailbox);
			details.setMessageId(messageId);
			if (!dao.retrieve(details))
				return null;

			RepositoryDAO repoDao = (RepositoryDAO) AS4Processor.core.dao.createDAO(RepositoryDAO.class);
			RepositoryDVO repoDvo = (RepositoryDVO) repoDao.createDVO();/////////////////////////// ''/''/////////////////////////
			repoDvo.setMessageBox(details.getMessageBox());
			repoDvo.setMessageId(details.getMessageId());
			if (!repoDao.retrieve(repoDvo))
				return null;
			SOAPMessage msg = AS4DvoConvertor.soapFromRepositoryDvo(repoDvo);
			if (attachments != null && MessageStoreConstant.MESSAGE_TYPE_USERMSG.equals(details.getMessageType())) {
				Iterator it = msg.getAttachments();
				while (it.hasNext()) {
					AttachmentPart part = (AttachmentPart) it.next();
					Attachment a = new Attachment();
					a.setContent(part.getRawContentBytes());
					a.setContentType(part.getContentType());
					String[] headers = part.getMimeHeader("Content-Disposition");
					if (headers != null && headers.length > 0)
						a.setFilename(Attachment.extractFileNameFromMime(headers[0]));
					attachments.add(a);
				}
			}
		} catch (AS4Exception e) {
			AS4Processor.core.log.error("Fail to retrieve message (" + messageId + ", " + mailbox + ") from database",
					e);
			return null;
		} catch (SOAPException e) {
			AS4Processor.core.log.error("Error in processing attachments of (" + messageId + ", " + mailbox + ")", e);
			return null;
		}
		return details;
	}

}