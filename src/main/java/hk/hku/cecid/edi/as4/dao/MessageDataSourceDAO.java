package hk.hku.cecid.edi.as4.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import hk.hku.cecid.edi.as4.pkg.AS4MessageConstant;
import hk.hku.cecid.piazza.commons.dao.DAOException;
import hk.hku.cecid.piazza.commons.dao.DVO;
import hk.hku.cecid.piazza.commons.dao.ds.DataSourceDAO;
import hk.hku.cecid.piazza.commons.dao.ds.DataSourceProcess;
import hk.hku.cecid.piazza.commons.dao.ds.DataSourceTransaction;
import hk.hku.cecid.piazza.commons.util.Convertor;

/**
 * @author Donahue Sze
 * 
 */
public class MessageDataSourceDAO extends DataSourceDAO implements MessageDAO {

	@Override
	public List<MessageDVO> findPendingOutboxMessage() throws DAOException {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		return super.find("find_outbox_pending_messages", new Object[] { MessageStoreConstant.OUTBOX, MessageStoreConstant.STATUS_PENDING_DELIVERY, now });
	}

	@Override
	public List<MessageDVO> findPullMessageByPmode(String pmodeId) throws DAOException {
		return super.find("find_pull_message_by_pmode", new Object[] { pmodeId, MessageStoreConstant.STATUS_TO_BE_PULLED, MessageStoreConstant.OUTBOX });
	}

	@Override
	public List<MessageDVO> findMessagesByHistory(MessageDVO data, int numberOfMessage) throws DAOException {
		return super.find("find_messages_by_history",
				new Object[] { data.getMessageId(), data.getMessageBox(), data.getFromPartyId(), data.getToPartyId(),
						data.getAction(), data.getService(), data.getAgreementRef(), data.getStatus(),
						numberOfMessage });
	}

	@Override
	public List<MessageDVO> findReceivedMessagesByPmode(String pmode, boolean includeRead) throws DAOException {
		if (includeRead)
			return super.find("find_received_messages_by_pmode", new Object[] { pmode });
		else
			return super.find("find_new_received_messages_by_pmode", new Object[] { MessageStoreConstant.INBOX, pmode });

	}
	
	@Override
	public int updateMessageReadStatus(List<String>ids, boolean isRead) throws DAOException {
		String sql = this.getSQL("update_read_status");
		DataSourceProcess process = new DataSourceProcess(this) {
			protected void doTransaction(DataSourceTransaction tx) throws DAOException {
				getDAO().setTransaction(tx);

				for(String id:ids) 
					 MessageDataSourceDAO.super.executeUpdate(sql, new Object[]{isRead, Convertor.toTimestamp(new Date()), id});
			}
		};
		process.start();
		return ids.size();
	}
	
	@Override
	public List<MessageDVO> findPullMessageByMpc(String mpc) throws DAOException {
		return super.find("find_pull_message_by_mpc",  new Object[] {mpc, MessageStoreConstant.STATUS_TO_BE_PULLED, MessageStoreConstant.OUTBOX});
	}

	@Override
	public List<MessageDVO> findPullMessageByDefaultMpc() throws DAOException {
		return super.find("find_pull_message_by_default_mpc",  new Object[] {AS4MessageConstant.EB3_DEFAULT_MPC, MessageStoreConstant.STATUS_TO_BE_PULLED, MessageStoreConstant.OUTBOX});
	}
	

	@Override
	public DVO createDVO() {
		return new MessageDataSourceDVO();
	}

	@Override
	public void create(DVO data) throws DAOException {
		((MessageDVO) data).setModifiedTimeStamp(new Date());
		super.create(data);
	}

	@Override
	public boolean persist(DVO data) throws DAOException {
		((MessageDVO) data).setModifiedTimeStamp(new Date());
		return super.persist(data);
	}

	@Override
	public boolean persistWithCondition(MessageDVO dvo, Map<String, Object> conditions) throws DAOException {
		dvo.setModifiedTimeStamp(new Date());
		return super.persistWithCondition(dvo, conditions);
	}


}