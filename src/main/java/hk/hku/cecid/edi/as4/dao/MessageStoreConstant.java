package hk.hku.cecid.edi.as4.dao;

public class MessageStoreConstant {

	// message box
    public final static String INBOX = "inbox";

    public final static String OUTBOX = "outbox";
    
    // internal status    
    
    public final static String STATUS_PENDING_DELIVERY = "PD";
    
    public final static String STATUS_PENDING_RECEIPT  = "PR";
    
    public final static String STATUS_PROCESSED        = "PS";

    public final static String STATUS_PROCESSED_ERROR  = "PE";

    public final static String STATUS_DELIVERED        = "DL";

    public final static String STATUS_DELIVERY_FAILURE = "DF";
    
    
    public final static String STATUS_TO_BE_PULLED     = "TP";
    

    // message type
    public final static String MESSAGE_TYPE_USERMSG = "UserMessage";

    public final static String MESSAGE_TYPE_PULL = "Pull";
    
    public final static String MESSAGE_TYPE_ERROR = "Error";
    
    public final static String MESSAGE_TYPE_RECEIPT = "Receipt";
    



}
