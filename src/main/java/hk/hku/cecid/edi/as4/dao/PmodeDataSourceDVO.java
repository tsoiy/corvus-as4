package hk.hku.cecid.edi.as4.dao;

import java.util.Date;

import hk.hku.cecid.piazza.commons.dao.ds.DataSourceDVO;
import hk.hku.cecid.piazza.commons.util.Convertor;

public class PmodeDataSourceDVO extends DataSourceDVO implements PmodeDVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8602946335798799972L;

	@Override
	public String getPmodeId() {
		return super.getString("pmodeId");
	}

	@Override
	public void setPmodeId(String pmodeId) {
		super.setString("pmodeId", pmodeId);
	}

	@Override
	public boolean isDisabled() {
		return super.getBoolean("isDisabled");
	}

	@Override
	public void setIsDisabled(boolean isDisabled) {
		super.setBoolean("isDisabled", isDisabled);
	}

	@Override
	public int getRetries() {
		return super.getInt("retries");
	}

	@Override
	public void setRetries(int retries) {
		super.setInt("retries", retries);
	}

	@Override
	public int getRetryInterval() {
		return super.getInt("retryInterval");
	}

	@Override
	public void setRetryInterval(int retryInterval) {
		super.setInt("retryInterval", retryInterval);
	}

	@Override
	public String getSignCert() {
		return super.getString("signCert");
	}

	@Override
	public void setSignCert(String signCert) {
		super.setString("signCert", signCert);
	}
	
	@Override
	public String getSignAlgorithm() {
		return super.getString("signAlgorithm");
	}

	@Override
	public void setSignAlgorithm(String signAlgorithm) {
		super.setString("signAlgorithm", signAlgorithm);
	}

	@Override
	public String getSignHashFunc() {
		return super.getString("signHashFunc");
	}

	@Override
	public void setSignHashFunc(String signHashFunc) {
		super.setString("signHashFunc", signHashFunc);
	}

	@Override
	public String getEncryptCert() {
		return super.getString("encryptCert");
	}

	@Override
	public void setEncryptCert(String encryptCert) {
		super.setString("encryptCert", encryptCert);
	}
	
	@Override
	public String getEncryptAlgorithm() {
		return super.getString("encryptAlgorithm");
	}

	@Override
	public void setEncryptAlgorithm(String encryptAlgorithm) {
		super.setString("encryptAlgorithm", encryptAlgorithm);
	}

	@Override
	public String getAction() {
		return super.getString("action");
	}

	@Override
	public void setAction(String action) {
		super.setString("action", action);
	}

	@Override
	public String getAgreement() {
		return super.getString("agreement");
	}

	@Override
	public void setAgreement(String agreement) {
		super.setString("agreement", agreement);
	}

	@Override
	public boolean isReceiptRequired() {
		return super.getBoolean("isReceiptRequired");
	}

	@Override
	public void setIsReceiptRequired(boolean isReceiptRequired) {
		super.setBoolean("isReceiptRequired", isReceiptRequired);
	}

	@Override
	public boolean isRetryRequired() {
		return super.getBoolean("isRetryRequired");
	}

	@Override
	public void setIsRetryRequired(boolean isRetryRequired) {
		super.setBoolean("isRetryRequired", isRetryRequired);

	}

	@Override
	public boolean isPmodeAuthRequired() {
		return super.getBoolean("isPmodeAuthRequired");
	}

	@Override
	public void setIsPmodeAuthRequired(boolean isPmodeAuthRequired) {
		super.setBoolean("isPmodeAuthRequired", isPmodeAuthRequired);
	}
	
	@Override
	public boolean isSignRequired() {
		return super.getBoolean("isSignRequired");
	}

	@Override
	public void setIsSignRequired(boolean isSignRequired) {
		super.setBoolean("isSignRequired", isSignRequired);
	}

	@Override
	public boolean isEncryptRequired() {
		return super.getBoolean("isEncryptRequired");
	}

	@Override
	public void setIsEncryptRequired(boolean isEncryptRequired) {
		super.setBoolean("isEncryptRequired", isEncryptRequired);
	}
	
	@Override
	public boolean isReportErrorAsResponse() {
		return super.getBoolean("isReportErrorAsResponse");
	}

	@Override
	public void setIsReportErrorAsResponse(boolean isReportErrorAsResponse) {
		super.setBoolean("isReportErrorAsResponse", isReportErrorAsResponse);

	}

	@Override
	public boolean isReportProcessError() {
		return super.getBoolean("isReportProcessError");
	}

	@Override
	public void setIsReportProcessError(boolean isReportProcessError) {
		super.setBoolean("isReportProcessError", isReportProcessError);
	}

	@Override
	public boolean isReportDeliveryFailure() {
		return super.getBoolean("isReportDeliveryFailure");
	}

	@Override
	public void setIsReportDeliveryFailure(boolean isReportDeliveryFailture) {
		super.setBoolean("isReportDeliveryFailture", isReportDeliveryFailture);

	}
	@Override
	public boolean isTokenRequired() {
		return super.getBoolean("isTokenRequired");
	}
	
	@Override
	public void setIsTokenRequired(boolean isTokenRequired) {
		super.setBoolean("isTokenRequired", isTokenRequired);
	}
	
	@Override
	public boolean isTokenDigestRequired() {
		return super.getBoolean("isTokenDigestRequired");
	}

	@Override
	public void setIsTokenDigestRequired(boolean isTokenDigestRequired) {
		super.setBoolean("isTokenDigestRequired", isTokenDigestRequired);
	}

	@Override
	public boolean isTokenCreatedRequired() {
		return super.getBoolean("isTokenCreatedRequired");
	}

	@Override
	public void setIsTokenCreatedRequired(boolean isTokenCreatedRequired) {
		super.setBoolean("isTokenCreatedRequired", isTokenCreatedRequired);
	}

	@Override
	public String getInitPartyId() {
		return super.getString("initPartyId");
	}

	@Override
	public void setInitPartyId(String initPartyId) {
		super.setString("initPartyId", initPartyId);
	}

	@Override
	public String getInitPartyRole() {
		return super.getString("initPartyRole");
	}

	@Override
	public void setInitPartyRole(String initPartyRole) {
		super.setString("initPartyRole", initPartyRole);
	}
	
	@Override
	public String getInitUsername() {
		return super.getString("initUsername");
	}

	@Override
	public void setInitUsername(String initUsername) {
		super.setString("initUsername", initUsername);
	}

	@Override
	public String getInitPassword() {
		return super.getString("initPassword");
	}

	@Override
	public void setInitPassword(String initPassword) {
		super.setString("initPassword", initPassword);
	}

	@Override
	public String getRespPartyId() {
		return super.getString("respPartyId");
	}

	@Override
	public void setRespPartyId(String respPartyId) {
		super.setString("respPartyId", respPartyId);
	}

	@Override
	public String getRespPartyRole() {
		return super.getString("respPartyRole");
	}

	@Override
	public void setRespPartyRole(String respPartyRole) {
		super.setString("respPartyRole", respPartyRole);
	}
	
	@Override
	public String getRespUsername() {
		return super.getString("respUsername");
	}

	@Override
	public void setRespUsername(String respUsername) {
		super.setString("respUsername", respUsername);
	}

	@Override
	public String getRespPassword() {
		return super.getString("respPassword");
	}

	@Override
	public void setRespPassword(String respPassword) {
		super.setString("respPassword", respPassword);
	}

	@Override
	public String getAddress() {
		return super.getString("address");
	}

	@Override
	public void setAddress(String address) {
		super.setString("address", address);
	}

	@Override
	public String getPullOrPush() {
		return super.getString("mepBinding");
	}

	@Override
	public void setPullOrPush(String pullOrPush) {
		super.setString("mepBinding", pullOrPush);
	}

	@Override
	public String getMpc() {
		return super.getString("mpc");
	}

	@Override
	public void setMpc(String mpc) {
		super.setString("mpc", mpc);
	}
	
	@Override
	public String getReportReceiverErrorTo() {
		return super.getString("reportReceiverErrorTo");
	}

	@Override
	public void setReportReceiverErrorTo(String reportReceiverErrorTo) {
		super.setString("reportReceiverErrorTo", reportReceiverErrorTo);
	}

	@Override
	public String getReceiptReplyPattern() {
		return super.getString("receiptReplyPattern");
	}

	@Override
	public void setReceiptReplyPattern(String receiptReplyPattern) {
		super.setString("receiptReplyPattern", receiptReplyPattern);
	}

	@Override
	public String getTokenUsername() {
		return super.getString("tokenUsername");
	}

	@Override
	public void setTokenUsername(String tokenUsername) {
		super.setString("tokenUsername", tokenUsername);
	}

	@Override
	public String getTokenPassword() {
		return super.getString("tokenPassword");
	}

	@Override
	public void setTokenPassword(String tokenPassword) {
		super.setString("tokenPassword", tokenPassword);
	}
	
	@Override
	public String getService() {
		return super.getString("service");
	}

	@Override
	public void setService(String service) {
		super.setString("service", service);

	}

	@Override
	public String getServiceType() {
		return super.getString("serviceType");
	}

	@Override
	public void setServiceType(String serviceType) {
		super.setString("serviceType", serviceType);
	}

    /**
     * @return Returns the timeStamp.
     */
	@Override
    public Date getModifiedTimeStamp() {
    	
        return (Date) super.get("modifiedTimeStamp");
    }

    /**
     * @param timeStamp
     *            The timeStamp to set.
     */
	@Override
    public void setModifiedTimeStamp(Date modifiedTimeStamp) {
		super.put("modifiedTimeStamp", Convertor.toTimestamp(modifiedTimeStamp));
    }
    
	
}