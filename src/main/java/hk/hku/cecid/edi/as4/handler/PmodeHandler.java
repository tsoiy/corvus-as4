package hk.hku.cecid.edi.as4.handler;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.MessageDAO;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.MessageStoreConstant;
import hk.hku.cecid.edi.as4.dao.PmodeDAO;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4MessageConstant;
import hk.hku.cecid.edi.as4.pkg.AS4PullMessage;
import hk.hku.cecid.edi.as4.pkg.AS4UserMessage;
import hk.hku.cecid.piazza.commons.dao.DAOException;

public class PmodeHandler {

	private Object lock;
	private PmodeDAO dao;
	private MessageDAO messageDao;
	private PmodeDVO defaultPmode;
	private final static String DEFAULT_PMODE_ID = "__DEFAULT_PMODE_ID__";

	private static class HelperHolder {
		public static final PmodeHandler singleton;
		static {
			singleton = new PmodeHandler();
		}
	}

	public static PmodeHandler getInstance() {
		return HelperHolder.singleton;
	}

	private PmodeHandler() {
		try {
			lock = new Object();
			dao = (PmodeDAO) AS4Processor.core.dao.createDAO(PmodeDAO.class);
			messageDao = (MessageDAO) AS4Processor.core.dao.createDAO(MessageDAO.class);
			defaultPmode = (PmodeDVO) dao.createDVO();
			defaultPmode.setPmodeId(DEFAULT_PMODE_ID);
			defaultPmode.setPullOrPush(AS4MessageConstant.EB3_MEP_BINDING_PUSH_URL);
			defaultPmode.setIsDisabled(false);
			defaultPmode.setIsEncryptRequired(false);
			defaultPmode.setIsSignRequired(false);
			defaultPmode.setIsPmodeAuthRequired(false);
			defaultPmode.setIsTokenCreatedRequired(false);
			defaultPmode.setIsReceiptRequired(false);
			defaultPmode.setIsReportProcessError(false);

		} catch (DAOException e) {
			AS4Processor.core.log.error("Fail to initialize database connection for Pmode handler", e);
		}
	}

	private PmodeDVO findByMessageId(String messageId, String mailbox) {
		PmodeDVO result = null;
		MessageDVO dvo = (MessageDVO) messageDao.createDVO();
		dvo.setMessageId(messageId);
		dvo.setMessageBox(mailbox);

		try {
			synchronized (lock) {
				if (messageDao.retrieve(dvo)) {
					result = dao.findByPmodeId(dvo.getPmodeId());
				} else {
					AS4Processor.core.log.info("Reference message (" + messageId + ", " + mailbox + ") not found");
				}
			}
		} catch (DAOException e) {
			AS4Processor.core.log
					.error("Database error in retrieving pmode by messageId (" + messageId + ", " + mailbox + ")", e);
		}
		return result;
	}

	public PmodeDVO findByPmodeId(String pmodeId) {
		PmodeDVO result = null;
		try {
			synchronized (lock) {
				result = dao.findByPmodeId(pmodeId);
			}
		} catch (DAOException e) {
			AS4Processor.core.log.error("Database error in retrieving pmode: " + pmodeId, e);
		}
		return result;
	}

	private List<PmodeDVO> findPmodeForPullByMpc(String mpc) {
		List<PmodeDVO> results = null;
		try {
			synchronized (lock) {
				results = dao.findPmodeForPullByMpc(mpc);
			}
		} catch (DAOException e) {
			AS4Processor.core.log.error("Database error in finding pmode for pulling mpc: " + mpc, e);
		}
		return results;
	}

	private List<PmodeDVO> findPmodeForPullByDefaultMpc() {
		List<PmodeDVO> results = null;
		try {
			synchronized (lock) {
				results = dao.findPmodeForPullByDefaultMpc();
			}
		} catch (DAOException e) {
			AS4Processor.core.log.error("Database error in finding pmode for pulling default", e);
		}
		return results;
	}

	private PmodeDVO findByServiceActionAgreement(String service, String action, String agreement) {
		PmodeDVO result = (PmodeDVO) dao.createDVO();
		result.setService(service);
		result.setAction(action);
		result.setAgreement(agreement);
		try {
			synchronized (lock) {
				if (dao.findByServiceActionAgreement(result))
					return result;
			}
		} catch (DAOException e) {
			AS4Processor.core.log.error("Database error in finding pmode for pulling default", e);
		}
		return null;
	}
	
	public PmodeDVO createDVO() {
		return (PmodeDVO)dao.createDVO();
	}

	public PmodeDVO findUserMessagePmode(AS4UserMessage message) {
		PmodeDVO pmode = null;
		// if pmode is explicitly defined, must use it to look up
		// see https://issues.oasis-open.org/browse/EBXMLMSG-48
		// https://issues.oasis-open.org/browse/EBXMLMSG-68
		AS4Processor.core.log.debug("Getting pmode of user message: " + message.getMessageId());
		if (message.getPmodeId() != null && !message.getPmodeId().isEmpty()) {
			AS4Processor.core.log.debug("Getting pmode of user message: " + message.getMessageId() + " by pmode ID: "
					+ message.getPmodeId());
			pmode = findByPmodeId(message.getPmodeId());
		} else {
			AS4Processor.core.log.debug(
					"Getting pmode of user message: " + message.getMessageId() + " by service: " + message.getService()
							+ " action: " + message.getAction() + " agreementRef: " + message.getAgreeRef());
			// try to find by service, action & agreementRef
			pmode = findByServiceActionAgreement(message.getService(), message.getAction(), message.getAgreeRef());
		}
		return pmode;
	}

	public PmodeDVO findSignalMessagePmode(String refId) {
		AS4Processor.core.log.debug("Getting pmode of signal message by reference message ID: " + refId);
		return findByMessageId(refId, MessageStoreConstant.OUTBOX);
	}

	public List<PmodeDVO> getAllPmode() throws DAOException {
		List<PmodeDVO> pmodes = null;
		AS4Processor.core.log.info("Getting all pmodes");
		try {
			synchronized (lock) {
				pmodes = dao.findAllPmodes();
			}
		} catch (DAOException e) {
			AS4Processor.core.log.error("Database error in retrieving all pmodes", e);
			throw e;
		}
		return pmodes;
	}

	public boolean deletePmode(String pmode) throws DAOException {
		PmodeDVO dvo = (PmodeDVO) dao.createDVO();
		dvo.setPmodeId(pmode);
		AS4Processor.core.log.info("Deleting pmode: " + dvo.getPmodeId());

		try {
			synchronized (lock) {
				return dao.remove(dvo);
			}
		} catch (DAOException e) {
			AS4Processor.core.log.error("Database error in deleting pmode " + pmode, e);
			throw e;
		}
	}

	public boolean addPmode(PmodeDVO pmode) throws DAOException {
		AS4Processor.core.log.info("Add pmode");
		if (pmode.getPmodeId() == null || pmode.getPmodeId().trim().isEmpty()) {
			AS4Processor.core.log.error("Missing pmode ID when adding a pmode");
			return false;
		}
		
        try {
            URL url = new URL(pmode.getAddress());
            if( !"HTTP".equalsIgnoreCase(url.getProtocol()) && !"HTTPS".equalsIgnoreCase(url.getProtocol())) {
            	AS4Processor.core.log.error("Invalid protocol: " + pmode.getAddress());
            	return false;
            }
        } catch (MalformedURLException e) {
        	AS4Processor.core.log.error("Invalid address: " + pmode.getAddress());
            return false;
        }
        
		if (pmode.getPullOrPush() == null || !(AS4MessageConstant.EB3_MEP_BINDING_PULL_URL.equals(pmode.getPullOrPush())
				|| AS4MessageConstant.EB3_MEP_BINDING_PUSH_URL.equals(pmode.getPullOrPush()))) {
			AS4Processor.core.log.error("Invalid MEP binding: " + pmode.getPullOrPush());
			return false;
		}
		if (pmode.isRetryRequired() && pmode.getRetries() <= 0) {
			AS4Processor.core.log.error("Invalid retry count, must be greater than 0: " + pmode.getRetries());
			return false;
		}
		
		if (pmode.isRetryRequired() && pmode.getRetryInterval() <= 0) {
			AS4Processor.core.log.error("Invalid retry interval, must be greater than 0: " + pmode.getRetryInterval());
			return false;
		}		

		if (pmode.isReceiptRequired() && !(AS4MessageConstant.EB3_RECEIPT_REPLY_PATTERN_CALLBACK
				.equals(pmode.getReceiptReplyPattern())
				|| AS4MessageConstant.EB3_RECEIPT_REPLY_PATTERN_RESPONSE.equals(pmode.getReceiptReplyPattern()))) {
			AS4Processor.core.log.error("Invalid receipt reply pattern: " + pmode.getReceiptReplyPattern());
			return false;
		}

		if (pmode.isEncryptRequired()
				&& !(AS4MessageConstant.EB3_SECURITY_ENCRYPT_ALGO_3DES.equals(pmode.getEncryptAlgorithm())
						|| AS4MessageConstant.EB3_SECURITY_ENCRYPT_ALGO_AES128.equals(pmode.getEncryptAlgorithm())
						|| AS4MessageConstant.EB3_SECURITY_ENCRYPT_ALGO_AES128_GCM.equals(pmode.getEncryptAlgorithm())
						|| AS4MessageConstant.EB3_SECURITY_ENCRYPT_ALGO_AES256.equals(pmode.getEncryptAlgorithm())))
		{
			AS4Processor.core.log.error("Invalid encryption algorithm: " + pmode.getEncryptAlgorithm());
			return false;
		}
		
		if (pmode.isSignRequired()
				&& !(AS4MessageConstant.EB3_SECURITY_SIGN_ALGO_DSA_SHA1.equals(pmode.getSignAlgorithm())
						|| AS4MessageConstant.EB3_SECURITY_SIGN_ALGO_RSA_SHA1.equals(pmode.getSignAlgorithm())
						|| AS4MessageConstant.EB3_SECURITY_SIGN_ALGO_RSA_SHA256.equals(pmode.getSignAlgorithm())))
		{
			AS4Processor.core.log.error("Invalid signing algorithm: " + pmode.getSignAlgorithm());
			return false;
		}
		
		if (pmode.isSignRequired()
				&& !(AS4MessageConstant.EB3_SECURITY_SIGN_HASH_FUNC_SHA1.equals(pmode.getSignHashFunc())
						|| AS4MessageConstant.EB3_SECURITY_SIGN_HASH_FUNC_SHA256.equals(pmode.getSignHashFunc())
						|| AS4MessageConstant.EB3_SECURITY_SIGN_HASH_FUNC_SHA384.equals(pmode.getSignHashFunc())
						|| AS4MessageConstant.EB3_SECURITY_SIGN_HASH_FUNC_SHA512.equals(pmode.getSignHashFunc())))
		{
			AS4Processor.core.log.error("Invalid signing hash function: " + pmode.getSignHashFunc());
			return false;
		}
		
		AS4Processor.core.log.debug("About to add/update pmode: " + pmode);
		if( dao.findByPmodeId(pmode.getPmodeId()) != null) {
			AS4Processor.core.log.info("Updating existing pmode: " + pmode.getPmodeId());
			dao.persist(pmode);
		} else {
			AS4Processor.core.log.info("Creating new pmode: " + pmode.getPmodeId());
			dao.create(pmode);
		}
		return true;
	}

	public List<PmodeDVO> findPullMessagePmode(AS4PullMessage message) {
		List<PmodeDVO> pmodes;
		List<PmodeDVO> result = new ArrayList<>();

		if (message.getMpc() != null && !message.getMpc().equals(AS4MessageConstant.EB3_DEFAULT_MPC)) {
			AS4Processor.core.log.debug("Finding pmodes of mpc: " + message.getMpc());
			pmodes = findPmodeForPullByMpc(message.getMpc());
		} else {
			AS4Processor.core.log.debug("Finding pmodes of default mpc");
			pmodes = findPmodeForPullByDefaultMpc();
		}

		AS4Processor.core.log.debug("Potental match before security check: " + pmodes.size());
		// do not check if there is no security header
		if (message.getSecurityHeader(null) != null) {
			for (PmodeDVO pmode : pmodes) {
				AS4Processor.core.log.debug("Trying: " + pmode.getPmodeId());
				try {
					AS4Processor.getSecurityProcessor().verifyMessage(message, pmode, true);
					AS4Processor.getSecurityProcessor().authEbmsUser(message, pmode, true);
					result.add(pmode);
				} catch (AS4Exception e) {
					AS4Processor.core.log.debug(pmode.getPmodeId() + " failed due to: " + e.getMessage());
					// ignore those pmodes not matching the security
				}
			}
		} else {
			result = pmodes;
		}
		AS4Processor.core.log.debug("Potental match after security check: " + result.size());

		return result;
	}
}
