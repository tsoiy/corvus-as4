package hk.hku.cecid.edi.as4.dao;

import java.io.InputStream;
import java.util.Date;

import hk.hku.cecid.piazza.commons.dao.ds.DataSourceDVO;
import hk.hku.cecid.piazza.commons.util.Convertor;

/**
 * @author Donahue Sze
 *  
 */
public class RepositoryDataSourceDVO extends DataSourceDVO implements
        RepositoryDVO {

    /**
	 * 
	 */
	private static final long serialVersionUID = -532425313187444145L;

	public RepositoryDataSourceDVO() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see hk.hku.cecid.ebms.spa.dao.RepositoryDVO#getMessageId()
     */
    public String getMessageId() {
        return super.getString("messageId");
    }

    /*
     * (non-Javadoc)
     * 
     * @see hk.hku.cecid.ebms.spa.dao.RepositoryDVO#setMessageId(java.lang.String)
     */
    public void setMessageId(String messageId) {
        super.setString("messageId", messageId);
    }

    public String getContentType() {
        return super.getString("contentType");
    }

    public void setContentType(String contentType) {
        super.setString("contentType", contentType);
    }

    /*
     * (non-Javadoc)
     * 
     * @see hk.hku.cecid.ebms.spa.dao.RepositoryDVO#getContent()
     */
    public byte[] getContent() {
    	return (byte[])super.get("content");
    }    

    /*
     * (non-Javadoc)
     * 
     * @see hk.hku.cecid.ebms.spa.dao.RepositoryDVO#setContent(java.lang.String)
     */
    public void setContent(byte[] content) {
        super.put("content", content);
    }

    public void setContent(InputStream is) {
        super.put("content", is);
    }

    /**
     * @return Returns the timeStamp.
     */
    public Date getModifiedTimeStamp() {
    	
        return (Date) super.get("modifiedTimeStamp");
    }

    /**
     * @param timeStamp
     *            The timeStamp to set.
     */
    public void setModifiedTimeStamp(Date modifiedTimeStamp) {
		super.put("modifiedTimeStamp", Convertor.toTimestamp(modifiedTimeStamp));
    }

    /**
     * @return Returns the messageBox.
     */
    public String getMessageBox() {
        return super.getString("messageBox");
    }

    /**
     * @param messageBox
     *            The messageBox to set.
     */
    public void setMessageBox(String messageBox) {
        super.setString("messageBox", messageBox);
    }
}