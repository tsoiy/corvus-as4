package hk.hku.cecid.edi.as4.dao;

import java.util.List;
import java.util.Map;

import hk.hku.cecid.piazza.commons.dao.DAO;
import hk.hku.cecid.piazza.commons.dao.DAOException;

/**
 *
 *  
 */
public interface MessageDAO extends DAO {

	public List<MessageDVO> findPendingOutboxMessage() throws DAOException;
	
	public List<MessageDVO> findPullMessageByPmode(String pmodeId) throws DAOException;
	
	public List<MessageDVO> findPullMessageByMpc(String mpc) throws DAOException;

	public List<MessageDVO> findPullMessageByDefaultMpc() throws DAOException;

    public List<MessageDVO> findMessagesByHistory(MessageDVO data, int numberOfMessage) 
            throws DAOException;

    public List<MessageDVO> findReceivedMessagesByPmode(String pmodeId, boolean includeRead) 
            throws DAOException;
    
    public int updateMessageReadStatus(List<String>ids, boolean isRead) throws DAOException;
    
//    public int findNumberOfMessagesByHistory(MessageDVO data)
//            throws DAOException;
//
//    public int recoverProcessingMessages()
//            throws DAOException;
    
	public boolean persistWithCondition(MessageDVO dvo, Map<String, Object>conditions) throws DAOException;

    
}