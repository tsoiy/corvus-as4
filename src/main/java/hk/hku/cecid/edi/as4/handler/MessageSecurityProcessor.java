package hk.hku.cecid.edi.as4.handler;

import static org.apache.wss4j.common.WSS4JConstants.*;
import static org.apache.wss4j.dom.WSConstants.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.wss4j.common.WSEncryptionPart;
import org.apache.wss4j.common.crypto.Crypto;
import org.apache.wss4j.common.crypto.CryptoFactory;
import org.apache.wss4j.common.ext.WSSecurityException;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.WSDataRef;
import org.apache.wss4j.dom.engine.WSSConfig;
import org.apache.wss4j.dom.engine.WSSecurityEngine;
import org.apache.wss4j.dom.engine.WSSecurityEngineResult;
import org.apache.wss4j.dom.handler.RequestData;
import org.apache.wss4j.dom.handler.WSHandlerResult;
import org.apache.wss4j.dom.message.WSSecEncrypt;
import org.apache.wss4j.dom.message.WSSecHeader;
import org.apache.wss4j.dom.message.WSSecSignature;
import org.apache.wss4j.dom.message.WSSecUsernameToken;
import org.apache.wss4j.dom.message.token.UsernameToken;
import org.apache.wss4j.dom.processor.Processor;
import org.opensaml.soap.util.SOAPConstants;
import org.w3c.dom.Document;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorCode;
import hk.hku.cecid.edi.as4.pkg.AS4Message;
import hk.hku.cecid.edi.as4.pkg.AS4MessageConstant;
import hk.hku.cecid.piazza.commons.Sys;
import hk.hku.cecid.piazza.commons.module.Component;

public class MessageSecurityProcessor extends Component {

	private WSSecurityEngine decryptor;
	private WSSecurityEngine signChecker;
	private WSSecurityEngine usernameChecker;
	private PasswordCallbackHandler encryptCertPasswordCb;
	private Crypto decrypCrypto;
	private Crypto verifyCrypto;

	private int keyIdentifierType = BST_DIRECT_REFERENCE;
	private String encryptAlgo;
	private String signCertName;
	private String signCertPassword;
	private String signAlgo;
	private String signHash;

	@Override
	protected void init() throws Exception {
		Properties params = getParameters();
		WSSConfig config = WSSConfig.getNewInstance();

		config = WSSConfig.getNewInstance();
		// disable all other checkings, only check for username token
		QName[] onlyUserName = new QName[] { BINARY_TOKEN, TIMESTAMP, SIGNATURE_CONFIRMATION, SIGNATURE, ENCRYPTED_KEY,
				ENCRYPTED_DATA, REFERENCE_LIST, SAML_TOKEN, SAML2_TOKEN, ENCRYPTED_ASSERTION };
		for (QName q : onlyUserName) {
			config.setProcessor(q, (Processor) null);
		}
		usernameChecker = new WSSecurityEngine();
		usernameChecker.setWssConfig(config);

		config = WSSConfig.getNewInstance();
		// disable all other checkings, only check for dencryption
		QName[] onlyDecrypt = new QName[] { TIMESTAMP, USERNAME_TOKEN, SAML_TOKEN, SAML2_TOKEN, SIGNATURE,
				SIGNATURE_CONFIRMATION };
		for (QName q : onlyDecrypt) {
			config.setProcessor(q, (Processor) null);
		}
		decryptor = new WSSecurityEngine();
		decryptor.setWssConfig(config);
		signCertName = params.getProperty("sign-cert-alias");
		signCertPassword = params.getProperty("sign-cert-password");
		signAlgo = params.getProperty("sign-algo");
		signHash = params.getProperty("sign-hash");

		Sys.main.log.info("Initializing own sign cert: " + signCertName);
		Sys.main.log.info("Initializing own sign algo: " + signAlgo);
		Sys.main.log.info("Initializing own sign hash: " + signHash);
		encryptAlgo = params.getProperty("encrypt-algo");
		encryptCertPasswordCb = new PasswordCallbackHandler();
		encryptCertPasswordCb.addPassword(params.getProperty("encrypt-cert-alias"),
				params.getProperty("encrypt-cert-password"));
		Sys.main.log.info("Initializing own encryption cert: " + params.getProperty("encrypt-cert-alias"));
		Sys.main.log.info("Initializing own sign algo: " + encryptAlgo);

		config = WSSConfig.getNewInstance();
		// disable all other checkings, only check for signature
		QName[] onlySign = new QName[] { USERNAME_TOKEN, TIMESTAMP, SIGNATURE_CONFIRMATION, ENCRYPTED_KEY,
				ENCRYPTED_DATA, SAML_TOKEN, SAML2_TOKEN, ENCRYPTED_ASSERTION };
		for (QName q : onlySign) {
			config.setProcessor(q, (Processor) null);
		}
		signChecker = new WSSecurityEngine();
		signChecker.setWssConfig(config);

		String keyIdType = params.getProperty("key-identifier-type", "BST");
		if (keyIdType != null) {
			switch (keyIdType) {
			case "ISSUER":
				keyIdentifierType = ISSUER_SERIAL;
				break;
			case "SUBJECTKEY":
				keyIdentifierType = SKI_KEY_IDENTIFIER;
				break;
			default:
//				Sys.main.log.warn("Unknown key-identifier-type: " + keyIdType + ", default to BST");
				// fall through
			case "BST":
				keyIdentifierType = BST_DIRECT_REFERENCE;
				keyIdType = "BST";
				break;
			}
		}
		Sys.main.log.info("Key indenifier type: " + keyIdType);
		
		Properties p = new Properties();
		p.setProperty("org.apache.wss4j.crypto.provider", "org.apache.wss4j.common.crypto.Merlin");
		p.setProperty("org.apache.wss4j.crypto.merlin.keystore.type", params.getProperty("private-keystore-type"));
		p.setProperty("org.apache.wss4j.crypto.merlin.keystore.password",
				params.getProperty("private-keystore-password"));
		p.setProperty("org.apache.wss4j.crypto.merlin.keystore.file", params.getProperty("private-keystore-path"));
		decrypCrypto = CryptoFactory.getInstance(p);
		
		Sys.main.log.info("Public keystore: " + params.getProperty("public-keystore-path"));
		Sys.main.log.info("Private keystore: " + params.getProperty("private-keystore-path"));

		p = new Properties();
		p.setProperty("org.apache.wss4j.crypto.provider", "org.apache.wss4j.common.crypto.Merlin");
		p.setProperty("org.apache.wss4j.crypto.merlin.keystore.type", params.getProperty("public-keystore-type"));
		p.setProperty("org.apache.wss4j.crypto.merlin.keystore.password",
				params.getProperty("public-keystore-password"));
		p.setProperty("org.apache.wss4j.crypto.merlin.keystore.file", params.getProperty("public-keystore-path"));
		verifyCrypto = CryptoFactory.getInstance(p);

	}

	void addUserNameToken(SOAPMessage msg, PmodeDVO dvo, boolean initiator) throws SOAPException, WSSecurityException {
		Document doc = null;
		// first see if we need to add ebms security header
		String username = null;
		String password = null;
		if (dvo.isPmodeAuthRequired()) {
			AS4Processor.core.log.debug("Adding username token for EBMS role");
			if (initiator) {
				username = dvo.getInitUsername();
				password = dvo.getInitPassword();
			} else {
				username = dvo.getRespUsername();
				password = dvo.getRespPassword();
			}
			doc = msg.getSOAPPart().getEnvelope().getOwnerDocument();
			WSSecHeader secHeader = new WSSecHeader(doc);
			secHeader.setMustUnderstand(true);
			secHeader.setActor(AS4MessageConstant.EB3_SECURITY_EBMS_ROLE);
			secHeader.insertSecurityHeader();
			WSSecUsernameToken token = new WSSecUsernameToken(secHeader);
			token.setUserInfo(username, password);
			token.build();
		}

		if (dvo.isTokenRequired()) {
			AS4Processor.core.log.debug("Adding username token for default role");
			if (doc == null)
				doc = msg.getSOAPPart().getEnvelope().getOwnerDocument();
			WSSecHeader secHeader = new WSSecHeader(doc);
			secHeader.setMustUnderstand(true);
			secHeader.insertSecurityHeader();
			WSSecUsernameToken token = new WSSecUsernameToken(secHeader);
			token.setUserInfo(dvo.getTokenUsername(), dvo.getTokenPassword());
			if (dvo.isTokenDigestRequired()) {
				token.setPasswordType(PASSWORD_DIGEST);
			} else {
				token.setPasswordType(PASSWORD_TEXT);
			}
			if (dvo.isTokenCreatedRequired()) {
				token.addCreated();
			}

			token.build();

		}

	}

	void authUser(SOAPMessage msg, PmodeDVO pmode) throws AS4Exception {
		Document doc = null;
		WSHandlerResult res;

		if (pmode.isTokenRequired()) {
			try {
				SOAPEnvelope unsignedEnvelope = msg.getSOAPPart().getEnvelope();
				doc = unsignedEnvelope.getOwnerDocument();

				PasswordCallbackHandler cb = new PasswordCallbackHandler(pmode.getTokenUsername(),
						pmode.getTokenPassword());
				res = usernameChecker.processSecurityHeader(doc, null, cb, decrypCrypto);
				if (res == null)
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP,
							"Missing authorization in default security header"));
				List<WSSecurityEngineResult> actionResult = res.getActionResults().get(WSConstants.UT);
				if (actionResult == null || actionResult.isEmpty())
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Missing authorization"));
				UsernameToken t = (UsernameToken) actionResult.get(0).get(WSSecurityEngineResult.TAG_USERNAME_TOKEN);
				if (t == null)
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP));
				if (pmode.isTokenDigestRequired() && !PASSWORD_DIGEST.equals(t.getPasswordType()))
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Password type mismatch"));
				if (pmode.isTokenCreatedRequired() && t.getCreated() == null)
					throw new AS4Exception(
							new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Missing created in username token"));
			} catch (WSSecurityException e) {
				throw new AS4Exception(new AS4Error(AS4ErrorCode.FAILED_AUTH,
						"Failed authentication in default security header -" + e.getMessage()));
			} catch (SOAPException e) {
				throw new AS4Exception(new AS4Error(AS4ErrorCode.FAILED_AUTH, e.getMessage()));
			}
		}
	}

	public void authEbmsUser(AS4Message message, PmodeDVO pmode, boolean isInitiator) throws AS4Exception {
		Document doc = null;
		WSHandlerResult res;

		if (pmode.isPmodeAuthRequired()) {
			try {
				if (doc == null) {
					SOAPEnvelope unsignedEnvelope = message.getSOAP().getSOAPPart().getEnvelope();
					doc = unsignedEnvelope.getOwnerDocument();
				}
				PasswordCallbackHandler ebmsCb;
				if (isInitiator)
					ebmsCb = new PasswordCallbackHandler(pmode.getInitUsername(), pmode.getInitPassword());
				else
					ebmsCb = new PasswordCallbackHandler(pmode.getRespUsername(), pmode.getRespPassword());

				res = usernameChecker.processSecurityHeader(doc, AS4MessageConstant.EB3_SECURITY_EBMS_ROLE, ebmsCb,
						decrypCrypto);
				if (res == null)
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP,
							"Missing authorization in ebms security header", message.getMessageId(), pmode));
				List<WSSecurityEngineResult> actionResult = res.getActionResults().get(WSConstants.UT);
				if (actionResult == null || actionResult.isEmpty())
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP,
							"Missing authorization in ebms security header", message.getMessageId(), pmode));

			} catch (WSSecurityException e) {
				throw new AS4Exception(new AS4Error(AS4ErrorCode.FAILED_AUTH,
						"Failed authentication in ebms security header -" + e.getMessage(), message.getMessageId(),
						pmode));
			} catch (SOAPException e) {
				throw new AS4Exception(
						new AS4Error(AS4ErrorCode.FAILED_AUTH, e.getMessage(), message.getMessageId(), pmode));
			}
		}
	}

	void decryptMessage(SOAPMessage msg, PmodeDVO pmode) throws AS4Exception {
		WSHandlerResult res = null;
		Iterator<AttachmentPart> iterator = msg.getAttachments();
		List<AttachmentPart> list = new ArrayList<>();
		iterator.forEachRemaining(list::add);

		try {
			Set<String> encryptedIds = new HashSet<>();
			Document doc = msg.getSOAPPart().getEnvelope().getOwnerDocument();
			RequestData data = new RequestData();
			data.setDecCrypto(decrypCrypto);
			data.setCallbackHandler(encryptCertPasswordCb);
			data.setAttachmentCallbackHandler(new AttachmentCallbackHandler(list));
			data.setActor(null);
			res = decryptor.processSecurityHeader(doc, data);
			boolean foundBody = false;
			if (pmode.isEncryptRequired()) {
				QName bodyName = new QName(SOAPConstants.SOAP12_NS, "Body");
				// check if the encryption algorithm matches with pmode
				if (res == null)
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Message not encrypted"));
				List<WSSecurityEngineResult> actionResults = res.getActionResults().get(WSConstants.ENCR);
				if (actionResults == null || actionResults.isEmpty())
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Message not encrypted"));
				for (WSSecurityEngineResult actionResult : actionResults) {
					List<WSDataRef> refs = (List<WSDataRef>) actionResult.get(WSSecurityEngineResult.TAG_DATA_REF_URIS);
					if (refs == null || refs.size() == 0) {
						throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Missing encrypted content"));
					}
					for (WSDataRef ref : refs) {
						if (ref.getAlgorithm() == null || !encryptAlgo.equals(ref.getAlgorithm())) {
							throw new AS4Exception(
									new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Encryption algorithm not match"));
						}
						if (ref.isAttachment())
							// remove "cid:" from ID
							encryptedIds.add(ref.getWsuId().replace("cid:", ""));
						else if (ref.getName().equals(bodyName))
							foundBody = true;

					}
				}
				// check if all the required parts are encrypted
				// AS4 profile section 5.1.6, body + attachments
				if (!foundBody) {
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "SOAP body not encrypted"));
				}
				for (AttachmentPart a : list) {
					// remove angle bracket from ID
					if (!encryptedIds.contains(a.getContentId().replaceAll("<|>", ""))) {
						throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP,
								"Attachment part " + a.getContentId() + " not encrypted"));
					}
				}

			}
		} catch (WSSecurityException | IOException | SOAPException e) {
			throw new AS4Exception(
					new AS4Error(AS4ErrorCode.FAILED_DECRYPT, "Failed to decrypt message -" + e.getMessage()));
		}

	}

	void verifySignature(SOAPMessage msg, PmodeDVO pmode) throws AS4Exception {

		if (!pmode.isSignRequired())
			return;

		WSHandlerResult res = null;

		try {
			SOAPEnvelope unsignedEnvelope = msg.getSOAPPart().getEnvelope();
			Document doc = unsignedEnvelope.getOwnerDocument();
			RequestData data = new RequestData();
			data.setSigVerCrypto(verifyCrypto);
			data.setCallbackHandler(new PasswordCallbackHandler());
			data.setActor(null);
			Iterator<AttachmentPart> iterator = msg.getAttachments();
			if (iterator.hasNext()) {
				List<AttachmentPart> list = new ArrayList<>();
				iterator.forEachRemaining(list::add);
				data.setAttachmentCallbackHandler(new AttachmentCallbackHandler(list));
			}
			res = signChecker.processSecurityHeader(doc, data);
			if (pmode.isSignRequired()) {
				if (res == null)
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Message not signed"));
				List<WSSecurityEngineResult> actionResults = res.getActionResults().get(WSConstants.SIGN);
				if (actionResults == null || actionResults.isEmpty())
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Message not signed"));
				String signAlgo = (String) actionResults.get(0).get(WSSecurityEngineResult.TAG_SIGNATURE_METHOD);
				if (!signAlgo.equals(pmode.getSignAlgorithm()))
					throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Signing algorithm not match"));
				for (WSSecurityEngineResult actionResult : actionResults) {
					List<WSDataRef> refs = (List<WSDataRef>) actionResult.get(WSSecurityEngineResult.TAG_DATA_REF_URIS);
					if (refs == null || refs.size() == 0) {
						throw new AS4Exception(new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Digest algorithm not match"));
					}
					for (WSDataRef ref : refs) {
						if (!ref.getDigestAlgorithm().equals(pmode.getSignHashFunc()))
							throw new AS4Exception(
									new AS4Error(AS4ErrorCode.POLICY_NONCOMP, "Digest algorithm not match"));
					}
				}
			}
		} catch (WSSecurityException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.FAILED_AUTH, "Error code:" + e.getErrorCode()));
		} catch (SOAPException | IOException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.FAILED_AUTH, e.getMessage()));
		}
	}

	public void verifyMessage(AS4Message message, PmodeDVO pmode, boolean isInitiator) throws AS4Exception {

		try {
			SOAPMessage msg = message.getSOAP();
			// first verify username token
			authUser(msg, pmode);

			// TODO only decrypt included attachments?
			// ebms_core 3.0 section 7.6 message should be signed then encrypted, so
			// decrypt then verify signature
			decryptMessage(msg, pmode);

			// then check signature
			verifySignature(msg, pmode);
		} catch (AS4Exception e) {
			e.getError().setRefId(message.getMessageId());
			e.getError().setPmode(pmode);
			throw e;
		}
	}

	void encryptSOAP(SOAPMessage msg, PmodeDVO pmode) throws AS4Exception {
		if (pmode == null || !pmode.isEncryptRequired())
			return;
		Document doc;
		try {
			doc = msg.getSOAPPart().getEnvelope().getOwnerDocument();

			WSSecHeader secHeader = new WSSecHeader(doc);
			secHeader.setMustUnderstand(true);
			secHeader.insertSecurityHeader();

			WSSecEncrypt encrypter = new WSSecEncrypt(secHeader);

			encrypter.setUserInfo(pmode.getEncryptCert());
			encrypter.setSymmetricEncAlgorithm(pmode.getEncryptAlgorithm());
			// encrypter.setStoreBytesInAttachment(true);
			encrypter.setKeyIdentifierType(keyIdentifierType);
			encrypter.getParts().add(new WSEncryptionPart("Body", SOAPConstants.SOAP12_NS, "Content"));
			Iterator<AttachmentPart> iterator = msg.getAttachments();
			List<AttachmentPart> parts = new ArrayList<>();
			iterator.forEachRemaining(parts::add);
			if (parts.size() > 0) {
				AttachmentCallbackHandler attachmentCallbackHandler = new AttachmentCallbackHandler(parts);
				encrypter.getParts().add(new WSEncryptionPart("cid:Attachments", "Content"));
				encrypter.setAttachmentCallbackHandler(attachmentCallbackHandler);
			}
			encrypter.build(verifyCrypto);
		} catch (SOAPException | WSSecurityException | IOException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.OTHER_ERROR, e.getMessage()));
		}
	}

	void signSOAP(SOAPMessage msg, PmodeDVO pmode) throws AS4Exception {
		try {
			Document doc = msg.getSOAPPart().getEnvelope().getOwnerDocument();
			WSSecHeader secHeader = new WSSecHeader(doc);
			secHeader.setMustUnderstand(true);
			secHeader.insertSecurityHeader();

			WSSecSignature signer = new WSSecSignature(secHeader);

			signer.setUserInfo(signCertName, signCertPassword);
			signer.setSignatureAlgorithm(signAlgo);
			signer.setSigCanonicalization(C14N_EXCL_OMIT_COMMENTS);
			signer.setDigestAlgo(signHash);
			signer.setKeyIdentifierType(keyIdentifierType);
			signer.setAddInclusivePrefixes(false);

			signer.getParts().add(new WSEncryptionPart("Body", SOAPConstants.SOAP12_NS, "Content"));
			signer.getParts().add(new WSEncryptionPart(AS4MessageConstant.EB3_HEADER_MESSAGING,
					AS4MessageConstant.EB3_NS_URL, "Content"));

			Iterator<AttachmentPart> iterator = msg.getAttachments();
			List<AttachmentPart> parts = new ArrayList<>();
			iterator.forEachRemaining(parts::add);
			if (parts.size() > 0) {
				AttachmentCallbackHandler attachmentCallbackHandler = new AttachmentCallbackHandler(parts);
				signer.getParts().add(new WSEncryptionPart("cid:Attachments", "Content"));
				signer.setAttachmentCallbackHandler(attachmentCallbackHandler);
			}

			signer.build(decrypCrypto);
		} catch (SOAPException | WSSecurityException | IOException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.OTHER_ERROR, e.getMessage()));
		}
	}

	public boolean addSecurityProcess(SOAPMessage msg, boolean isInitiator, PmodeDVO pmode) {
		if (pmode == null)
			return true;
		try {
			// ebms_core 3.0 section 7.6 message should be signed then encrypted
			if (pmode.isSignRequired()) {
				AS4Processor.core.log.debug("Adding singature");
				signSOAP(msg, pmode);
			}
			// encrypt if needed
			if (pmode.isEncryptRequired())
				AS4Processor.core.log.debug("Encrypting message");
				encryptSOAP(msg, pmode);
			// add username token
			addUserNameToken(msg, pmode, isInitiator);
		} catch (AS4Exception | WSSecurityException | SOAPException e) {
			AS4Processor.core.log.error(
					"Error in add security processing to the reply message with pmode (" + pmode.getPmodeId() + ")", e);
			return false;
		}
		return true;

	}
	
	public boolean addSecurityProcess(AS4Message message, boolean isInitiator, PmodeDVO pmode) {
		if (pmode == null)
			return true;
		AS4Processor.core.log.info("Applying security processing to message (" + message.getMessageId()
				+ ") with pmode (" + pmode.getPmodeId() + ")");
		return addSecurityProcess(message.getSOAP(), isInitiator, pmode);
	}
}
