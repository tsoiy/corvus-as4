package hk.hku.cecid.edi.as4.task;

import java.time.Instant;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.List;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.MessageStoreConstant;
import hk.hku.cecid.edi.as4.dao.MessageStoreDAO;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.RepositoryDVO;
import hk.hku.cecid.edi.as4.handler.InboundMessageProcessor;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorCode;
import hk.hku.cecid.edi.as4.pkg.AS4MessageConstant;
import hk.hku.cecid.piazza.commons.dao.DAOException;
import hk.hku.cecid.piazza.commons.module.ActiveTask;

/**
 * 
 * 
 */
public class OutboxTask implements ActiveTask {

	/**
	 * The DVO Format representing <code>EbXML Message</code> needed to deliver in
	 * this task.
	 */
	MessageDVO messageDvo;
	PmodeDVO pmodeDvo;
	RepositoryDVO repoDvo;

	/**
	 * Explicit Constructor.
	 * 
	 * @param message
	 */
	public OutboxTask(MessageDVO message, RepositoryDVO repoDvo, PmodeDVO pmode) {
		this.messageDvo = message;
		this.repoDvo = repoDvo;
		this.pmodeDvo = pmode;
	}

	SOAPMessage sendMessage() throws AS4Exception {
		SOAPMessage message = null;
		SOAPMessage reply;
		message = AS4DvoConvertor.soapFromRepositoryDvo(repoDvo);
		AS4Processor.getSecurityProcessor().addSecurityProcess(message, true, pmodeDvo);
		SOAPConnectionFactory f;
		try {
			f = SOAPConnectionFactory.newInstance();
			SOAPConnection c = f.createConnection();
			reply = c.call(message, messageDvo.getAddress());
			c.close();

		} catch (UnsupportedOperationException | SOAPException e) {
			throw new AS4Exception(new AS4Error(AS4ErrorCode.CONNECTION_FAILURE));
		}
		return reply;
	}

	@Override
	public void execute() throws Exception {
		boolean replyExpected = false;
		String msgId = messageDvo.getMessageId();
		// are we expecting reply?
		switch (messageDvo.getMessageType()) {
		case MessageStoreConstant.MESSAGE_TYPE_USERMSG:
			if (pmodeDvo.isReceiptRequired()) {
				messageDvo.setStatus(MessageStoreConstant.STATUS_PENDING_RECEIPT);
				if (pmodeDvo.getReceiptReplyPattern().equals(AS4MessageConstant.EB3_RECEIPT_REPLY_PATTERN_RESPONSE))
					replyExpected = true;
			}
			break;
		case MessageStoreConstant.MESSAGE_TYPE_PULL:
			replyExpected = true;
			break;
		}

		List<MessageDVO> update = Arrays.asList(messageDvo);
		SOAPMessage reply = null;
		try {
			AS4Processor.core.log.debug("Sending out " + messageDvo.getMessageType() + " message (" + msgId + ")");
			messageDvo.setRetries(messageDvo.getRetries() - 1);
			reply = sendMessage();
			messageDvo.setStatus(MessageStoreConstant.STATUS_DELIVERED);
			AS4Processor.core.log.debug(reply == null ? "No reply" : "Reply" + " recevied for message (" + msgId + ")");
			if (reply == null && replyExpected) {
				AS4Processor.core.log.info("Reply expected for message (" + msgId + "), but none received as reply");
			}

		} catch (AS4Exception e) {
			if (messageDvo.getRetries() > 0) {
				messageDvo.setStatus(MessageStoreConstant.STATUS_PENDING_DELIVERY);
				Date timeout = Date.from(Instant.now().plusSeconds(pmodeDvo.getRetryInterval()));
				messageDvo.setTimeOutTimeStamp(timeout);
			} else {
				// Assume failure
				messageDvo.setStatus(MessageStoreConstant.STATUS_DELIVERY_FAILURE);
				messageDvo.setStatusDescription("Failed to deliver message");
			}
		}
		// no reply, just update the sent message status
		if (reply == null) {
			try {
				MessageStoreDAO storeDao = (MessageStoreDAO) AS4Processor.core.dao.createDAO(MessageStoreDAO.class);
				storeDao.updateAndInsert(update, null, null);
			} catch (DAOException e) {
				AS4Processor.core.log
						.error("Failed to update pending delivery for message (" + messageDvo.getMessageId() + ")", e);
				throw e;
			} catch (ConcurrentModificationException e2) {
				AS4Processor.core.log.info("Concurrent update detected when updating messages of ID ("
						+ messageDvo.getMessageId() + "), skipping handling");
			}
		} else {

			// process the reply by InboundMessageProcessor
			InboundMessageProcessor.getInstance().processReply(reply, messageDvo, pmodeDvo);
		}

	}

	// Below are required by the interface ActiveTask, but not using it
	// The original design based on a thread sleep model which is not an efficient
	// way of handling retries
	@Override
	public int getMaxRetries() {
		return 0;
	}

	@Override
	public void setRetried(int retried) {
	}

	@Override
	public void onFailure(Throwable t) {
		AS4Processor.core.log.error("Error in outbox task", t);
	}

	@Override
	public boolean isSucceedFast() {
		return true;
	}

	@Override
	public boolean isRetryEnabled() {
		return false;
	}

	@Override
	public long getRetryInterval() {
		return 0;
	}

	@Override
	public void onAwake() {
	}

}