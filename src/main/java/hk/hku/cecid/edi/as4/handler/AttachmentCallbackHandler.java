package hk.hku.cecid.edi.as4.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.SOAPException;

import org.apache.wss4j.common.ext.Attachment;
import org.apache.wss4j.common.ext.AttachmentRequestCallback;
import org.apache.wss4j.common.ext.AttachmentResultCallback;

public class AttachmentCallbackHandler implements CallbackHandler {

	private Map<String, AttachmentPart> originalMap = new HashMap<>();
	private Map<String, Attachment> attachmentMap = new HashMap<>();

	public AttachmentCallbackHandler() {
	}

	public AttachmentCallbackHandler(List<AttachmentPart> attachments) throws IOException, SOAPException {
		if (attachments != null) {
			for (AttachmentPart part : attachments) {
				String id = part.getContentId().replaceAll("<|>", "");
				originalMap.put(id, part);
				Attachment a = new Attachment();
				a.setId(id);
				a.setMimeType(part.getContentType());
				a.setSourceStream(part.getDataHandler().getDataSource().getInputStream());
				attachmentMap.put(id, a);
			}
		}
	}

	@Override
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		for (int i = 0; i < callbacks.length; i++) {
			if (callbacks[i] instanceof AttachmentRequestCallback) {
				AttachmentRequestCallback attachmentRequestCallback = (AttachmentRequestCallback) callbacks[i];
				String id = attachmentRequestCallback.getAttachmentId();
				List<Attachment> result;
				if (id.equals("Attachments")) {
					result = new ArrayList<Attachment>(attachmentMap.values());
				} else if (attachmentMap.containsKey(id)) {
					result = Collections.singletonList(attachmentMap.get(id));
				} else { 
					result = new ArrayList<>(0);
				}
				attachmentRequestCallback.setAttachments(result);
			} else if (callbacks[i] instanceof AttachmentResultCallback) {
				AttachmentResultCallback attachmentResultCallback = (AttachmentResultCallback) callbacks[i];
				Attachment a = attachmentResultCallback.getAttachment();
				AttachmentPart part = originalMap.get(a.getId());
				if( part != null ) {
					try {
						part.setRawContent(a.getSourceStream(), a.getMimeType());
					} catch (SOAPException e) {
						throw new IOException(e);
					}
				}
			} else {
				throw new UnsupportedCallbackException(callbacks[i], "Unrecognized Callback");
			}
		}
	}

}