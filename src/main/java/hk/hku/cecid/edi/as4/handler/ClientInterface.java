package hk.hku.cecid.edi.as4.handler;

import java.util.Date;
import java.util.List;

import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.Attachment;

public interface ClientInterface {
	public void attachmentCallback(String pmodeId, String messageId, Date timestamp, List<Attachment>attachments);
	
	public void reportProcessError(String pmodeId, String messageId, Date timestamp, AS4Error error);
	
	public void reportDeliveryError(String pmodeId, String messageId, Date timestamp, AS4Error error);
}
