package hk.hku.cecid.edi.as4.model;

import hk.hku.cecid.piazza.commons.GenericException;
import lombok.Getter;

/**
 * AS4Exception represents an exception related to AS4 message.
 * 
 *  
 */
public class AS4Exception extends GenericException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8929245277354928601L;
	
	@Getter
	private AS4Error error;
    /**
     * Creates a new instance of AS2MessageException.
     */
    public AS4Exception(AS4Error error) {
        super(error.getDetailDescription());
        this.error = error;
    }

}