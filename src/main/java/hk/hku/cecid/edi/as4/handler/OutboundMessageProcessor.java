package hk.hku.cecid.edi.as4.handler;

import java.time.Instant;
import java.util.Date;

import javax.xml.soap.SOAPException;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.MessageStoreConstant;
import hk.hku.cecid.edi.as4.dao.MessageStoreDAO;
import hk.hku.cecid.edi.as4.dao.PmodeDAO;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.RepositoryDVO;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.model.Attachment;
import hk.hku.cecid.edi.as4.model.MshMessage;
import hk.hku.cecid.edi.as4.model.MshMessage.Status;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorMessage;
import hk.hku.cecid.edi.as4.pkg.AS4Message;
import hk.hku.cecid.edi.as4.pkg.AS4MessageConstant;
import hk.hku.cecid.edi.as4.pkg.AS4PullMessage;
import hk.hku.cecid.edi.as4.pkg.AS4UserMessage;
import hk.hku.cecid.piazza.commons.dao.DAOException;

/**
 *
 * 
 */
public class OutboundMessageProcessor {

	private MessageStoreDAO dao;

	private static class HelperHolder {
		public static final OutboundMessageProcessor singleton;
		static {
			try {
				singleton = new OutboundMessageProcessor();
			} catch (DAOException e) {
				throw new ExceptionInInitializerError(e);
			}
		}
	}

	public static OutboundMessageProcessor getInstance() {
		return HelperHolder.singleton;
	}

	private OutboundMessageProcessor() throws DAOException {
		dao = (MessageStoreDAO) AS4Processor.core.dao.createDAO(MessageStoreDAO.class);
	}

	private void fillDvoWithPmode(MessageDVO dvo, PmodeDVO pmode) {

		dvo.setMessageBox(MessageStoreConstant.OUTBOX);
		dvo.setPmodeId(pmode.getPmodeId());
		dvo.setToPartyId(pmode.getInitPartyId());
		dvo.setToPartyRole(pmode.getInitPartyRole());
		dvo.setFromPartyId(pmode.getRespPartyId());
		dvo.setFromPartyRole(pmode.getRespPartyRole());
		dvo.setAddress(pmode.getAddress());
		dvo.setAgreementRef(pmode.getAgreement());
		dvo.setAction(pmode.getAction());
		dvo.setService(pmode.getService());
		dvo.setRetries(pmode.isRetryRequired() ? pmode.getRetries() : 0);
		dvo.setIsAckRequired(pmode.isReceiptRequired());
		dvo.setMpc(pmode.getMpc());

		// default to be sent out
		dvo.setStatus(MessageStoreConstant.STATUS_PENDING_DELIVERY);

		// message to be pulled
		if (pmode.getPullOrPush().equals(AS4MessageConstant.EB3_MEP_BINDING_PULL_URL)
				&& dvo.getMessageType() == MessageStoreConstant.MESSAGE_TYPE_USERMSG) {
			dvo.setStatus(MessageStoreConstant.STATUS_TO_BE_PULLED);
		} else {
			// immediate timeout for sending out
			dvo.setTimeOutTimeStamp(new Date(0));
		}

		String serviceType = pmode.getServiceType();
		if (serviceType != null) {
			dvo.setServiceType(serviceType);
		}
	}

	private AS4UserMessage createUserMessage(MshMessage request, PmodeDVO pmode) throws SOAPException {

		AS4UserMessage msg = new AS4UserMessage();
		msg.setPmodeId(pmode.getPmodeId());
		msg.setAction(pmode.getAction());
		msg.setService(pmode.getService());
		msg.setServiceType(pmode.getServiceType());
		msg.setAgreeRef(pmode.getAgreement());
		msg.setConvId(request.getConvId());

		// if we are sending out a message to be pulled, we are the responder, not the
		// initiator
		// the initiator is the puller. See EBMS spec section D.3.1. General P-Mode
		// Parameters
		if (pmode.getPullOrPush().equals(AS4MessageConstant.EB3_MEP_BINDING_PULL_URL)) {
			msg.setFromPartyId(pmode.getRespPartyId());
			msg.setFromPartyRole(pmode.getRespPartyRole());
			msg.setToPartyId(pmode.getInitPartyId());
			msg.setToPartyRole(pmode.getInitPartyRole());
		} else {
			msg.setFromPartyId(pmode.getInitPartyId());
			msg.setFromPartyRole(pmode.getInitPartyRole());
			msg.setToPartyId(pmode.getRespPartyId());
			msg.setToPartyRole(pmode.getRespPartyRole());

		}
		if (pmode.getMpc() != null && !pmode.getMpc().isEmpty()) {
			msg.setMpc(pmode.getMpc());
		}
		if (request.getAttachments() != null) {
			for (Attachment a : request.getAttachments()) {
				msg.addAttachment(a);
			}
		}

		return msg;

	}

	private AS4PullMessage createPullMessage(PmodeDVO pmode) throws SOAPException {
		AS4PullMessage msg = new AS4PullMessage();
		if (pmode.getMpc() != null && !pmode.getMpc().isEmpty()) {
			msg.setMpc(pmode.getMpc());
		}
		return msg;
	}

	public void sendErrorMessage(AS4ErrorMessage errorMsg) throws AS4Exception, DAOException {
		MessageDVO dvo = dao.createMessageDVO();
		RepositoryDVO repoDvo = dao.createRepositoryDVO();
		AS4DvoConvertor.toMessageDvo(errorMsg, dvo);
		AS4DvoConvertor.toRepositoryDvo(errorMsg, repoDvo);
		dvo.setMessageBox(MessageStoreConstant.OUTBOX);
		repoDvo.setMessageBox(MessageStoreConstant.OUTBOX);
		dvo.setStatus(MessageStoreConstant.STATUS_PENDING_DELIVERY);
		dao.storeMessage(dvo, repoDvo);
	}

	public static void setMessageStatus(MessageDVO message, PmodeDVO pmode, String statusMessage) {
		if (message.getRetries() > 0) {
			message.setStatus(MessageStoreConstant.STATUS_PENDING_DELIVERY);
			Date timeout = Date.from(Instant.now().plusSeconds(pmode.getRetryInterval()));
			message.setTimeOutTimeStamp(timeout);
		} else {
			message.setStatus(MessageStoreConstant.STATUS_DELIVERY_FAILURE);
			message.setStatusDescription(statusMessage);
		}

	}

	public void processOutgoingRequest(MshMessage request, MshMessage response) {

		if (request.getPmodeId() == null || request.getPmodeId().isEmpty()) {
			response.setResult("Pmode is missing");
			return;
		}

		PmodeDVO pmode = PmodeHandler.getInstance().findByPmodeId(request.getPmodeId());
		if (pmode == null) {
			response.setStatus(Status.PMODE_ERROR);
			response.setResult("Pmode " + request.getPmodeId() + " not found");
			return;
		}

		AS4Message message;
		MessageDVO dvo = dao.createMessageDVO();
		RepositoryDVO repoDvo = dao.createRepositoryDVO();
		try {

			switch (request.getType()) {
			case UserMessage:
				AS4UserMessage usermsg = createUserMessage(request, pmode);
				AS4DvoConvertor.convertOutboundUserMessage(usermsg, pmode, dvo, repoDvo);
				message = usermsg;
				break;
			case PullRequest:
				if (!pmode.getPullOrPush().equals(AS4MessageConstant.EB3_MEP_BINDING_PULL_URL)) {
					response.setStatus(Status.PMODE_ERROR);
					response.setResult("Pmode (" + pmode.getPmodeId() + ") not set up for pulling");
					return;
				}
				message = createPullMessage(pmode);
				AS4DvoConvertor.convertOutboundPullMessage((AS4PullMessage) message, pmode, dvo, repoDvo);
				break;
			default:
				response.setStatus(Status.OTHERS);
				response.setResult("Unknown request type " + request.getType());
				return;
			}

			dao.storeMessage(dvo, repoDvo);
			response.setMsgId(message.getMessageId());
			response.setResult("Success");

		} catch (SOAPException e) {
			response.setStatus(Status.OTHERS);
			response.setResult("Error in creating outgoing message");
		} catch (DAOException e) {
			response.setStatus(Status.DB_WRITE_ERROR);
			response.setResult("Error in storing outgoing message");
		}

	}

}