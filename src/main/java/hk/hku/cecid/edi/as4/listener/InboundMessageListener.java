package hk.hku.cecid.edi.as4.listener;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeader;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;

import org.apache.wss4j.common.util.XMLUtils;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.handler.MessageServiceHandler;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorCode;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorMessage;
import hk.hku.cecid.piazza.commons.servlet.RequestListenerException;
import hk.hku.cecid.piazza.commons.servlet.http.HttpRequestAdaptor;

/**
 * InboundMessageListener
 * 
 * @author Leonard
 *
 */
public class InboundMessageListener extends HttpRequestAdaptor {

	// public abstract AS4Message processRequest(AS4Message request)
	// throws RequestListenerException;

	void setMimeHeaders(MimeHeaders headers, HttpServletResponse response) {
		Iterator it = headers.getAllHeaders();
		while (it.hasNext()) {
			MimeHeader mh = (MimeHeader) it.next();
			response.addHeader(mh.getName(), mh.getValue());
		}
	}

	MimeHeaders getMimeHeaders(HttpServletRequest request) {
		MimeHeaders mh = new MimeHeaders();
		Enumeration<String> e = request.getHeaderNames();
		while (e.hasMoreElements()) {

			String header = e.nextElement();
			Enumeration<String> values = request.getHeaders(header);
			while (values.hasMoreElements()) {
				mh.addHeader(header, values.nextElement());
			}
		}
		return mh;
	}

	@Override
	public void listenerCreated() throws RequestListenerException {
		super.listenerCreated();
	}

	@Override
	public String processRequest(HttpServletRequest request, HttpServletResponse response)
			throws RequestListenerException {
		AS4Processor.core.log.debug("Receive request from :" + request.getRemoteHost());
		SOAPMessage reply = null;
		try {
			MessageFactory factory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
			SOAPMessage reqMsg = factory.createMessage(getMimeHeaders(request), request.getInputStream());
			reply = MessageServiceHandler.getInstance().processInboundMessage(reqMsg);

		} catch (IOException e) {
			AS4Processor.core.log.error("Error in receving request from remote host:" + request.getRemoteHost());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} catch (SOAPException e) {
			AS4Processor.core.log.error("Cannot convert to SOAP message");
			try {
				AS4ErrorMessage error = new AS4ErrorMessage();
				error.addError(new AS4Error(AS4ErrorCode.INVALID_HEADER));
				reply = error.getSOAP();
			} catch (SOAPException e1) {
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
		if (reply != null) {
			try {
				reply.saveChanges();
				setMimeHeaders(reply.getMimeHeaders(), response);
				reply.writeTo(response.getOutputStream());
				response.getOutputStream().close();
				AS4Processor.core.log.debug("Reponse of http status (" + response.getStatus()
						+ ") sent back successfully to " + request.getRemoteHost());
			} catch (SOAPException | IOException e) {
				AS4Processor.core.log.error("Error in sending response to remote host:" + request.getRemoteHost());
			}
		}
		return null;

	}
}