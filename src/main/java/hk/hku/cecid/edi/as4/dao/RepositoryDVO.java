package hk.hku.cecid.edi.as4.dao;

import java.io.InputStream;
import java.util.Date;

import hk.hku.cecid.piazza.commons.dao.DVO;

/**
 * @author Donahue Sze
 *  
 */
public interface RepositoryDVO extends DVO {
    /**
     * @return Returns the messageId.
     */
    public String getMessageId();

    /**
     * @param messageId The messageId to set.
     */
    public void setMessageId(String messageId);
    
    public String getContentType();
    
    public void setContentType(String contentType);

    /**
     * @return Returns the content.
     */
    public byte[] getContent();

    /**
     * @param content The content to set.
     */
    public void setContent(byte[] content);
    
    public void setContent(InputStream is);

    /**
     * @return Returns the modifiedTimeStamp.
     */
    public Date getModifiedTimeStamp();

    /**
     * @param modifiedTimeStamp The modifiedTimeStamp to set.
     */
    public void setModifiedTimeStamp(Date modifiedTimeStamp);

    /**
     * @return Returns the messageBox.
     */
    public String getMessageBox();

    /**
     * @param messageBox The messageBox to set.
     */
    public void setMessageBox(String messageBox);
}