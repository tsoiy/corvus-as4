package hk.hku.cecid.edi.as4.dao;

import java.util.Date;

import hk.hku.cecid.piazza.commons.dao.DAOException;
import hk.hku.cecid.piazza.commons.dao.DVO;
import hk.hku.cecid.piazza.commons.dao.ds.DataSourceDAO;

/**
 *  
 */
public class RepositoryDataSourceDAO extends DataSourceDAO implements
        RepositoryDAO {

	@Override
    public DVO createDVO() {
        return new RepositoryDataSourceDVO();
    }

	@Override
	public void create(DVO data) throws DAOException {
		((RepositoryDVO)data).setModifiedTimeStamp(new Date());
		super.create(data);
	}

	@Override
	public boolean persist(DVO data) throws DAOException {
		((RepositoryDVO)data).setModifiedTimeStamp(new Date());
		return super.persist(data);
	}		
}