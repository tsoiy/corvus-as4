package hk.hku.cecid.edi.as4.handler;

import javax.xml.soap.SOAPException;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.AS4DvoConvertor;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.MessageStoreConstant;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.RepositoryDVO;
import hk.hku.cecid.edi.as4.model.AS4Error;
import hk.hku.cecid.edi.as4.model.AS4Exception;
import hk.hku.cecid.edi.as4.pkg.AS4ErrorCode;
import hk.hku.cecid.edi.as4.pkg.AS4Message;
import hk.hku.cecid.edi.as4.pkg.AS4ReceiptMessage;
import hk.hku.cecid.piazza.commons.dao.DAOException;

public class ReceiptMessageProcessor extends MessageTypeProcessor {

	AS4ReceiptMessage receiptMsg;
	MessageDVO originalMsgDvo;

	public ReceiptMessageProcessor(AS4ReceiptMessage receipt) throws DAOException {
		super(false);
		receiptMsg = receipt;
		originalMsgDvo = null;
		pmode = null;
	}

	public ReceiptMessageProcessor(AS4ReceiptMessage receipt, MessageDVO usermsgDvo, PmodeDVO pmodeDvo)
			throws DAOException {
		super(true);
		receiptMsg = receipt;
		originalMsgDvo = usermsgDvo;
		this.pmode = pmodeDvo;
	}

	@Override
	void reset() {
		// force reload of the original message by setting the DVO to null
		originalMsgDvo = null;
	}
	
	@Override
	AS4Message process() throws AS4Exception {
		String refId = null;
		String errorMsg = null;

		try {
			refId = receiptMsg.getRefMsgId();
		} catch (SOAPException e) {
			throw new AS4Exception(
					new AS4Error(AS4ErrorCode.INVALID_HEADER, e.getMessage(), receiptMsg.getMessageId()));
		}

		MessageDVO receiptDvo = (MessageDVO) dao.createMessageDVO();
		RepositoryDVO repoDvo = (RepositoryDVO) dao.createRepositoryDVO();
		AS4DvoConvertor.toMessageDvo(receiptMsg, receiptDvo);
		AS4DvoConvertor.toRepositoryDvo(receiptMsg, repoDvo);
		receiptDvo.setMessageBox(MessageStoreConstant.INBOX);
		receiptDvo.setStatus(MessageStoreConstant.STATUS_PROCESSED);
		repoDvo.setMessageBox(MessageStoreConstant.INBOX);
		addInsert(receiptDvo, repoDvo);

		if (originalMsgDvo == null) {
			originalMsgDvo = (MessageDVO) dao.createMessageDVO();
			originalMsgDvo.setMessageBox(MessageStoreConstant.OUTBOX);
			originalMsgDvo.setMessageId(refId);

			try {
				if (msgDao.retrieve(originalMsgDvo) != true) {
					originalMsgDvo = null;
					errorMsg = "Original message for receipt ref message ID (" + refId + ") is not found";
					AS4Processor.core.log.warn(errorMsg);
					receiptDvo.setStatus(MessageStoreConstant.STATUS_PROCESSED_ERROR);
					receiptDvo.setStatusDescription(errorMsg);
				} else {
					String pmodeId = originalMsgDvo.getPmodeId();
					pmode = PmodeHandler.getInstance().findByPmodeId(pmodeId);
					if (pmode == null) {
						AS4Processor.core.log.warn("Pmode (" + pmodeId + ") of message (" + refId + ") is not found");
						receiptDvo.setStatus(MessageStoreConstant.STATUS_PROCESSED_ERROR);
						receiptDvo.setStatusDescription(errorMsg);
					}
				}
			} catch (DAOException e) {
				AS4Processor.core.log.error("Failed to retrieve record form DB for message " + refId + ") for receipt ("
						+ receiptMsg.getMessageId() + ")", e);
				originalMsgDvo = null;
				pmode = null;
			}
		}

		if (originalMsgDvo != null && pmode != null) {
			receiptDvo.setPmodeId(pmode.getPmodeId());
			if (!pmode.isReceiptRequired()) {
				errorMsg = "Receipt for message (" + pmode.getPmodeId() + ":" + originalMsgDvo.getMessageId()
						+ ") does not require ack";
				AS4Processor.core.log.info(errorMsg);
				receiptDvo.setStatus(MessageStoreConstant.STATUS_PROCESSED_ERROR);
				receiptDvo.setStatusDescription(errorMsg);
				throw new AS4Exception(new AS4Error(AS4ErrorCode.PMODE_MISMATCH,
						"Not expecting receipt for this pmode", receiptMsg.getMessageId()));
			}

			if (!originalMsgDvo.getMessageType().equals(MessageStoreConstant.MESSAGE_TYPE_USERMSG)) {
				errorMsg = "Unexpected receipt for non-user message (" + originalMsgDvo.getMessageId()
						+ ") of message type: " + originalMsgDvo.getMessageType();
				AS4Processor.core.log.warn(errorMsg);
				receiptDvo.setStatus(MessageStoreConstant.STATUS_PROCESSED_ERROR);
				receiptDvo.setStatusDescription(errorMsg);
				throw new AS4Exception(new AS4Error(AS4ErrorCode.VALUE_INCONSISTENT,
						"Not expected receipt for non-user message", receiptMsg.getMessageId(), pmode));
			}

			if (originalMsgDvo.isAcknowledged()) {
				AS4Processor.core.log
						.info("Receipt for message (" + originalMsgDvo.getMessageId() + ") already processed");
			} else {
				originalMsgDvo.setIsAcknowledged(true);
				originalMsgDvo.setStatus(MessageStoreConstant.STATUS_DELIVERED);
				originalMsgDvo.setStatusDescription("Receipt Id: " + receiptDvo.getMessageId());
				addUpdate(originalMsgDvo);
			}
		}
		return null;
	}
}
