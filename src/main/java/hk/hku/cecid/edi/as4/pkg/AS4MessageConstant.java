package hk.hku.cecid.edi.as4.pkg;

public class AS4MessageConstant {
	public static final String EB3_PREFIX = "eb";
	public static final String EB3_WSSE_PREFIX = "wsse";
	public static final String EB3_DS_PREFIX = "ds";
	public static final String EB3_EBBPSIG_PREFIX = "ebbpsig";

	public static final String EB3_NS_URL = "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/";
	public static final String EB3_WSSE_URL = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	public static final String EB3_WSU_URL = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
	public static final String EB3_DS_URL = "http://www.w3.org/2000/09/xmldsig#";
	public static final String EB3_EBBPSIG_URL = "http://docs.oasis-open.org/ebxml-bp/ebbp-signals-2.0";

	public final static String EB3_MEP_BINDING_PUSH_URL = "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/push";
	public final static String EB3_MEP_BINDING_PULL_URL = "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/pull";
	public final static String EB3_DEFAULT_MPC = "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC";

	public final static String EB3_AS4_DEFAULT_ACTION = "http://docs.oasis-open.org/ebxml-msg/as4/200902/action";
	public final static String EB3_AS4_DEFAULT_SERVICE = "http://docs.oasis-open.org/ebxml-msg/as4/200902/service";
	
	public static final String EB3_HEADER_MESSAGING = "Messaging";
	public static final String EB3_HEADER_USERMSG = "UserMessage";
	public static final String EB3_HEADER_SIGNALMSG = "SignalMessage";
	public static final String EB3_HEADER_SECURITY = "Security";

	public static final String EB3_HEADER_MSGINFO = "MessageInfo";
	public static final String EB3_HEADER_TIMESTAMP = "Timestamp";
	public static final String EB3_HEADER_MSGID = "MessageId";

	public static final String EB3_USERMSG_PARTYINFO = "PartyInfo";
	public static final String EB3_USERMSG_PARTYID = "PartyId";
	public static final String EB3_USERMSG_FROM = "From";
	public static final String EB3_USERMSG_TO = "To";
	public static final String EB3_USERMSG_ROLE = "Role";
	public static final String EB3_USERMSG_COLINFO = "CollaborationInfo";
	public static final String EB3_USERMSG_AGREEREF = "AgreementRef";
	public static final String EB3_USERMSG_SERVICE = "Service";
	public static final String EB3_USERMSG_ACTION = "Action";
	public static final String EB3_USERMSG_CONVID = "ConversationId";
	public static final String EB3_USERMSG_PAYLOADINFO = "PayloadInfo";
	public static final String EB3_USERMSG_MSGPROPERTIES = "MessageProperties";
	public static final String EB3_USERMSG_PROPERTY = "Property";
	public static final String EB3_USERMSG_PARTINFO = "PartInfo";

	public static final String EB3_ERRORMSG_ERROR = "Error";
	public static final String EB3_ERRORMSG_CODE = "errorCode";
	public static final String EB3_ERRORMSG_DESC = "shortDescription";
	public static final String EB3_ERRORMSG_ORIGIN = "origin";
	public static final String EB3_ERRORMSG_CATEGORY = "category";
	public static final String EB3_ERRORMSG_SEVERITY = "severity";
	public static final String EB3_ERRORMSG_REFMSGID = "refToMessageInError";

	public static final String EB3_PULLMSG_REQ = "PullRequest";
	public static final String EB3_PULLMSG_MPC = "mpc";

	public static final String EB3_RECEIPTMSG_RECIEPT = "Receipt";
	public static final String EB3_RECEIPTMSG_REFMSGID = "RefToMessageId";

	public static final String EB3_SECURITY_USERNAME_TOKEN = "UsernameToken";
	public static final String EB3_SECURITY_USERNAME = "Username";
	public static final String EB3_SECURITY_PASSWORD = "Password";
	public static final String EB3_SECURITY_EBMS_ROLE = "ebms";
	public static final String EB3_SECURITY_SIGNATURE = "Signature";
	public static final String EB3_SECURITY_SIGNEDINFO = "SignedInfo";
	public static final String EB3_SECURITY_REFERENCE = "Reference";

	public final static String EB3_SECURITY_SIGN_HASH_FUNC_SHA256 = "http://www.w3.org/2001/04/xmlenc#sha256";
	public final static String EB3_SECURITY_SIGN_HASH_FUNC_SHA1 = "http://www.w3.org/2000/09/xmldsig#sha1";
	public final static String EB3_SECURITY_SIGN_HASH_FUNC_SHA384 = "http://www.w3.org/2001/04/xmldsig-more#sha384";
	public final static String EB3_SECURITY_SIGN_HASH_FUNC_SHA512 = "http://www.w3.org/2001/04/xmlenc#sha512";
	

	public final static String EB3_SECURITY_SIGN_ALGO_RSA_SHA256 = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";
	public final static String EB3_SECURITY_SIGN_ALGO_DSA_SHA1 = "http://www.w3.org/2000/09/xmldsig#dsa-sha1";
	public final static String EB3_SECURITY_SIGN_ALGO_RSA_SHA1 = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";

	public final static String EB3_SECURITY_ENCRYPT_ALGO_3DES = "http://www.w3.org/2001/04/xmlenc#tripledes-cbc";
	public final static String EB3_SECURITY_ENCRYPT_ALGO_AES128 = "http://www.w3.org/2001/04/xmlenc#aes128-cbc";
	public final static String EB3_SECURITY_ENCRYPT_ALGO_AES256 = "http://www.w3.org/2001/04/xmlenc#aes256-cbc";
	public final static String EB3_SECURITY_ENCRYPT_ALGO_AES128_GCM = "http://www.w3.org/2009/xmlenc11#aes128-gcm";
	
	
	public static final String EB3_RECEIPT_REPLY_PATTERN_RESPONSE = "response";
	public static final String EB3_RECEIPT_REPLY_PATTERN_CALLBACK = "callback";
	
	public static final String EB3_RECEIPT_EBBPSIG_NRI = "NonRepudiationInformation";
	public static final String EB3_RECEIPT_EBBPSIG_MSGPARTNRI = "MessagePartNRInformation";
	public static final String EB3_RECEIPT_EBBPSIG_MSGPARTID = "MessagePartIdentifier";
	
}
