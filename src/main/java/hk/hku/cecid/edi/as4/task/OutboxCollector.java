package hk.hku.cecid.edi.as4.task;

import java.util.ArrayList;
import java.util.List;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.edi.as4.dao.MessageDAO;
import hk.hku.cecid.edi.as4.dao.MessageDVO;
import hk.hku.cecid.edi.as4.dao.PmodeDVO;
import hk.hku.cecid.edi.as4.dao.RepositoryDAO;
import hk.hku.cecid.edi.as4.dao.RepositoryDVO;
import hk.hku.cecid.edi.as4.handler.PmodeHandler;
import hk.hku.cecid.piazza.commons.dao.DAOException;
import hk.hku.cecid.piazza.commons.module.ActiveTaskList;

/**
 * 
 * 
 */
public class OutboxCollector extends ActiveTaskList {

	private boolean init = false;
	private MessageDAO dao;
	private RepositoryDAO repoDao;

	private boolean setUpDao() {
		try {
			dao = (MessageDAO) AS4Processor.core.dao.createDAO(MessageDAO.class);
			repoDao = (RepositoryDAO) AS4Processor.core.dao.createDAO(RepositoryDAO.class);
			init = true;
		} catch (DAOException e) {
			AS4Processor.core.log.error("Error in collecting message from outbox", e);
			return false;
		}
		return true;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hk.hku.cecid.piazza.commons.module.ActiveTaskList#getTaskList()
	 */
	@Override
	public List getTaskList() {

		List<OutboxTask> messageList = new ArrayList<>(100);
		if (init == false && setUpDao() == false) {
			return messageList;
		}

		// get all the pending message
		try {
			for (MessageDVO message : dao.findPendingOutboxMessage()) {
				RepositoryDVO details = (RepositoryDVO) repoDao.createDVO();
				details.setMessageBox(message.getMessageBox());
				details.setMessageId(message.getMessageId());
				PmodeDVO pmode = PmodeHandler.getInstance().findByPmodeId(message.getPmodeId());
				if (pmode == null) {
					AS4Processor.core.log.error("Failed to retrieve pmode + (" + message.getPmodeId()
							+ ") for message (" + message.getMessageId() + ")");
					continue;
				}
				if (!repoDao.retrieve(details)) {
					AS4Processor.core.log
							.error("Failed to retrieve details for message (" + message.getMessageId() + ")");
					continue;
				}
				OutboxTask outboxTask = new OutboxTask(message, details, pmode);
				messageList.add(outboxTask);

			}
		} catch (DAOException e) {
			AS4Processor.core.log.warn("Database error in collecting pending messages for sending out", e);
		}
		return messageList;
	}
}