package hk.hku.cecid.edi.as4.pkg;

import static hk.hku.cecid.edi.as4.pkg.AS4MessageConstant.*;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import hk.hku.cecid.edi.as4.model.AS4Exception;

public class AS4PullMessage extends AbstractAS4Message implements AS4Message {
	private SOAPElement signalMsgNode;
	private SOAPElement pullRequest;


	public AS4PullMessage() throws SOAPException {
		signalMsgNode = msgNode.addChildElement(EB3_HEADER_SIGNALMSG, EB3_PREFIX);
		addMessageInfoNode(signalMsgNode);
		pullRequest = signalMsgNode.addChildElement(EB3_PULLMSG_REQ, EB3_PREFIX);
	}
	
	public AS4PullMessage(SOAPMessage message, SOAPElement signalMsgNode) throws AS4Exception {
		super(message);
		this.signalMsgNode = signalMsgNode;
		msgInfo = findRequiredNode(EB3_NS_URL, signalMsgNode, EB3_HEADER_MSGINFO);
		pullRequest = findRequiredNode(EB3_NS_URL, signalMsgNode, EB3_PULLMSG_REQ);
	}
	
	public SOAPElement getSignalNode() {
		return signalMsgNode;
	}

	
	public void setMpc(String mpc) {
		pullRequest.setAttribute("mpc", mpc);
	}

	public String getMpc() {
		return( pullRequest==null?null:pullRequest.getAttribute("mpc"));
	}
	


}
