package hk.hku.cecid.edi.as4.model;

import java.io.UnsupportedEncodingException;

import javax.mail.internet.ContentDisposition;
import javax.mail.internet.MimeUtility;
import javax.mail.internet.ParseException;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Attachment {
	private String filename;
	private byte[] content;
	private String contentType;

	public static String extractFileNameFromMime(String disposition) {
		if (disposition == null)
			return null;
		try {
			return new ContentDisposition(disposition).getParameter("filename");
		} catch (ParseException e) {
			return null;
		}
	}

	public String constructDispositionHeader() {
		String result = null;
		try {
			ContentDisposition cd = new ContentDisposition("attachment");
			cd.setParameter("filename", MimeUtility.encodeText(filename));
			result = cd.toString();
		} catch (ParseException | UnsupportedEncodingException e) {
		}
		return result;
	}	
}
