package hk.hku.cecid.edi.as4.dao;

import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import hk.hku.cecid.edi.as4.AS4Processor;
import hk.hku.cecid.piazza.commons.dao.DAOException;
import hk.hku.cecid.piazza.commons.dao.DVO;
import hk.hku.cecid.piazza.commons.dao.ds.DataSourceDAO;
import hk.hku.cecid.piazza.commons.dao.ds.DataSourceProcess;
import hk.hku.cecid.piazza.commons.dao.ds.DataSourceTransaction;

/**
 *
 * 
 */
public class MessageStoreDataSourceDAO extends DataSourceDAO implements MessageStoreDAO {

	@Override
	public void storeMessage(final MessageDVO messageDVO, final RepositoryDVO repositoryDVO) throws DAOException {
		storeMessage(new MessageDVO[] { messageDVO }, new RepositoryDVO[] { repositoryDVO });
	}

	@Override
	public void storeMessage(final MessageDVO[] messageDVO, final RepositoryDVO[] repositoryDVO) throws DAOException {

		DataSourceProcess process = new DataSourceProcess(this) {
			protected void doTransaction(DataSourceTransaction tx) throws DAOException {
				MessageDAO messageDAO = (MessageDAO) getFactory().createDAO(MessageDAO.class);
				RepositoryDAO repositoryDAO = (RepositoryDAO) getFactory().createDAO(RepositoryDAO.class);

				messageDAO.setTransaction(tx);
				repositoryDAO.setTransaction(tx);

				if (messageDVO != null) {
					for (int i = 0; i < messageDVO.length; i++) {
						if (messageDVO[i] != null) {
							messageDAO.create(messageDVO[i]);
						}
					}
				}
				if (repositoryDVO != null) {
					for (int i = 0; i < repositoryDVO.length; i++) {
						if (repositoryDVO[i] != null) {
							repositoryDAO.create(repositoryDVO[i]);
						}
					}
				}
			}
		};

		process.start();
	}

	@Override
	public void updateAndInsert(List<MessageDVO> updateMsgs, List<MessageDVO> insertMsgs, List<RepositoryDVO> insertDetails) throws DAOException, ConcurrentModificationException {
		DataSourceProcess process = new DataSourceProcess(this) {
			protected void doTransaction(DataSourceTransaction tx) throws DAOException {
				MessageDAO messageDAO = (MessageDAO) getFactory().createDAO(MessageDAO.class);
				RepositoryDAO repositoryDAO = (RepositoryDAO) getFactory().createDAO(RepositoryDAO.class);

				messageDAO.setTransaction(tx);
				repositoryDAO.setTransaction(tx);

				if( updateMsgs != null)
					for(MessageDVO m:updateMsgs) {
						Map<String, Object>condition= new HashMap<>();
						condition.put("modified_time_stamp", m.getModifiedTimeStamp());
						if( messageDAO.persistWithCondition(m, condition) != true)
							throw new ConcurrentModificationException("Update of message (" + m.getMessageId() + ") failed, concurrent change?");
					}
				
				if (insertMsgs != null) {
					for(MessageDVO m:insertMsgs)
						messageDAO.create(m);
				}
				if (insertDetails != null) {
					for(RepositoryDVO d:insertDetails)
						repositoryDAO.create(d);
				}				
			}
		};
		try {
			process.start();
		} catch ( DAOException e ) {
			if (e.getCause() instanceof ConcurrentModificationException) {
				throw (ConcurrentModificationException)e.getCause();
			} else {
				throw e;
			}
		}
	}

	public DVO createDVO() {
		return null;
	}

	public MessageDVO createMessageDVO() {
		return new MessageDataSourceDVO();
	}

	public RepositoryDVO createRepositoryDVO() {
		return new RepositoryDataSourceDVO();
	}

}